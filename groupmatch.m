%finds group
a=xlsread('AllIDdat.xls');
ID=a(:,7);
Q=a(:,6);
good=find((ID>10)&((ID<500)|(ID>599))&((ID<5500)|(ID>5599))&(Q>2));%no males, bad pics
ID=ID(good);
Q=Q(good);
daten=datenum('30-Dec-1899')+a(good,1);
datew=floor(daten);
jj=spones(sparse(ID,datew,1));%ID by date matrix
nq=sum(jj,1);%numbers identified on days
jq=jj'*jj;%numbers seen on different days
jnq=min(nq'*spones(nq),spones(nq')*nq);%minimum number seen on different days
S=jq.*spfun('invo',jnq);%index S
S=spdiags(zeros(max(datew),1),0,S);%removes diagonals
datej=find(nq);
for j=1:length(datej);disp(j);disp(datestr([datej(j)  find(S(datej(j),:)>0.25)]));disp('  '),end%displays day matches