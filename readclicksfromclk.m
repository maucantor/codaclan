function clicks = readclicksfromclk(filename)

% function readclicksfromclk(filename)
%   function to read codas from Rainbow Click file into matlab
%   takes as input [filename] for full path click file
%   returns [codas] struct with fieds:
%     .whale - whale number in file
%     .icis - vertical vector containing icis
%     .length - coda length in seconds ( sum of icis)
%     .nclicks - number of clicks
%     .time - time of first coda click since start of file
%     .clicknumbers - click number since start of fie
%     .manual_ipi - horizontal vector containing manualy measures ipis
%     .events - horizontal vector containing event numbers for each click
%     .filename - filename
%     .whalecoda - nth codas for .whale
%
%   adapted from Douglas Gillespie rclibexample.m 
%   Ricardo Antunes - July 2006


% verify if file exists
d = dir(filename);
if isempty(d)
    error('Could not find click file');
end


% first of all, open up the file. The file wil be analysed to see how many
% sections it contains..:P
rf = rainbowfile(filename,1);

% read in all the clicks, but don't read in the click waveforms
clicks = readdata(rf, 'section', 1,2);
% and get the control structures for that section (to find out sample
% rate, etc.

structs = get(rf, 'structure', 1);
sr = structs.soundcardsettings.nsamplespersec ;

if ~isempty(clicks) & isstruct(clicks)


    clicks = clicks(find([clicks.whale] > 0));
    for ci = 1:length(clicks)
        clicks(ci).sr = sr;
    end
    % convert the click times to seconds (they are stored in ADC sample
    % units)
    % times = [whaleclicks.starttime]/structs.soundcardsettings.nsamplespersec

    %disp(['whale:',num2str(iw)]); % uncomment for debugging



end

% total number of clicks
n_clicks = length(clicks);

disp ( ['...found ',num2str(n_clicks),' clicks from ',num2str(length(unique([clicks.whale]))),' whale(s).']);

% close the file
close(rf)