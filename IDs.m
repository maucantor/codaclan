%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mauricio's Galapagos Sperm Whale ID Script!
% Written by Wilfried Beslin
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cont = 1;

% Main loop
while cont > 0
	% Start by loading data
	disp('Input is .xls(x) file containing the following:')
    disp('Sheet1: Reference "Temporary ID" column and "Catalogue ID" column (including headers).')
	disp('Sheet2: Data with temporary ID in the 5th column (E). The script will add a 6th "Final ID" column.')
	filename = input('Enter filename (include extension): ', 's'); 
    
    % Load reference IDs
	[num, txt, refID] = xlsread(filename, 1);
    
    % Load temporary ID
    [num, txt, tmpID] = xlsread(filename, 2, 'E:E');
    
    % Duplicate temporary ID to make "Final ID". This way, any missing
    % catalogue IDs will be left as temporary.
    finID = tmpID;
    finID(1,1) = {'Final ID'};
    
    % Assign final IDs (where all the work gets done!)
    [nrref, ncref] = size(refID);
    [nrtmp, nctmp] = size(tmpID);
    for i = 2:nrref
        if ~isnan(refID{i,2}) % if there is a catalogue ID
            for j = 2:nrtmp
                if strcmp(tmpID{j,1}, refID{i,1})   % if temp ID = ref ID
                    finID(j,1) = refID(i,2);         % final ID is catalogue ID
                end
            end
        end
    end
    
    % write back to file
	xlswrite(filename, finID, 2, 'F:F');
	fprintf('Final ID column has been added to "%s"', filename);
	
	% continue?
	cont = input('Do another file? (1/0): ');
end