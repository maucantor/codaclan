function [data noise] = rmNoise(dataCodas)

%[data noise] = rmNoise(dataCodas)
%
%Identify all coda types deemed as 'noise' by OPTICSxi, ie all identified
%with #99 at the end (#: NCLICKS). The function returns the data set
%without the columns with 'noise coda types' and also the indices of the
%noise, if necessary.
%
%INPUT:
%'dataCodas': standard dataCodas with the 46th column giving the coda
%type by OPTICSxi. Necessary columns are: 
%1st(coda#), 2nd(date or rec#), 3rd(repertoire), 4th(Nclicks), 5th(total length),
%6-40th(ICIs), 46th(coda type).
%
%OUTPUT:
%'data': dataset without noise coda types
%'noise': list of indices for the noise coda types
%
% Mauricio Cantor, June 2015

noise = {};
for n=1:size(dataCodas, 1)
    aux = dataCodas(n, 46);
    aux2 = num2str(aux);
    aux3 = strfind(aux2, '99');
    if isempty(aux3)
        noise{n} = n;
    end
end
noise = cell2mat(noise');

% removing noise
data = dataCodas;
data = data(noise, :);

end