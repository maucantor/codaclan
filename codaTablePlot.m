function codaTablePlot(mat,bw,rcodes,ccodes,values,cbar,scale)
% codaTablePlot(mat,bw,rcodes,ccodes,values,cbar,scale)
%
% Plots heat map (and the values) of a contingency table
% INPUT:
%'mat': contingency table, with coda types in rows, repertoires in columns.
%Cells are counts or proportions
%'bw': in [0,1,2,3,4,5,6,7]. bw=0 plots default colormap. bw=1: greyscale;
%bw=2: yellow scale; bw=3: green scale; bw=4: cian scale; bw=5: blue scale;
%bw=6: magenta scale; bw=7: red scale;
%'rcodes': vector giving row labels (coda types)
%'ccodes': vector giving column labels (repertoires)
%'values': 1/0. values=1 plot matrix values on top of heat map.
%'cbar': =1 plots color bar, 0 otherwhise.
%'scale': integer. Gives the range for the colos scale in 'bw'. E.g. 5 will
%give 5 shades, 100 will give 100 shades.
%
% OBS: REQUIRE rotateXLabels.m
%
% Mauricio Cantor, June 2015 (adapted from code found online)

    % Create a colored plot of the matrix values
    figure
    imagesc(mat);
    
    if bw~=0
    % Change the colormap (so higher values are darker and lower values are lighter)
        switch bw
        case 1 %gray
                 colormap(flipud(gray));
        case 2 %yellow
                 G = fliplr(linspace(0,1,scale)) .';
                 myGmap = horzcat(G, G, zeros(size(G)));
                 myGmap(1,:)=[1 1 1];
                 colormap(myGmap);        
        case 3 %green
                 G = fliplr(linspace(0,1,scale)) .';
                 myGmap = horzcat(zeros(size(G)), G, zeros(size(G)));
                 myGmap(1,:)=[1 1 1];
                 colormap(myGmap);
        case 4 %cian
                 G = fliplr(linspace(0,1,scale)) .';
                 myGmap = horzcat(zeros(size(G)), G, G);
                 myGmap(1,:)=[1 1 1];
                 colormap(myGmap);        
        case 5 %blue
                 G = fliplr(linspace(0,1,scale)) .';
                 myGmap = horzcat(zeros(size(G)), zeros(size(G)), G);
                 myGmap(1,:)=[1 1 1];
                 colormap(myGmap); 
        case 6 %magenta
                 G = fliplr(linspace(0,1,scale)) .';
                 myGmap = horzcat(G, zeros(size(G)), G);
                 myGmap(1,:)=[1 1 1];
                 colormap(myGmap); 
        case 7 %red
                 G = fliplr(linspace(0,1,scale)) .';
                 myGmap = horzcat(G, zeros(size(G)), zeros(size(G)));
                 myGmap(1,:)=[1 1 1];
                 colormap(myGmap);
        end        
    end
            
            
    % Plots values as text
    if values==1
        % Set to black and white
        colormap(flipud(gray))
        % Create strings from the matrix values
        textStrings = num2str(mat(:),'%0.1f'); 
        textStrings = strtrim(cellstr(textStrings));
        % Create x and y coordinates for the strings
        [x,y] = meshgrid(1:size(mat,2), 1:size(mat,1));
        %# Plot the strings
        hStrings = text(x(:),y(:),textStrings(:), 'HorizontalAlignment','center');
        %# Get the middle value of the color range
        midValue = mean(get(gca,'CLim'));
        % Choose white or black for the text color of the strings so they can be easily seen over the background color
        textColors = repmat(mat(:) > midValue,1,3);  
        set(hStrings,{'Color'},num2cell(textColors,2));
    end
    
    % Add color bar
    if cbar
        colorbar('Ticks',[min(min(mat)):0.1:max(max(mat))])
    end
    
    % Change the axes tick marks and tick labels
    set(gca,'XTick',1:size(mat,2),'XTickLabel',ccodes,...  
            'YTick',1:size(mat,1),'YTickLabel',rcodes,'TickLength',[0 0]);
    % Rotate x tick labels (REQUIRE rotateXLabels.m)
    rotateXLabels(gca, 90)
    
    % Add axis labels
    xlabel('Group Repertoire')
    ylabel('Coda Types')
    
end