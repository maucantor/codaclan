disp('HOW TO CLASSIFY CODAS:')
disp('(1) FIRST, USE KCLUS FUNCTION...(for VRC method)')
disp('  ');

help kclus

disp('  ');

disp('This will result in *txt files for each coda length in the current directory, and v.mat of VRC results...');

disp('  ');

disp('(2) THEN USE VPLOT FUNCTION ON V [vplot(v)] TO VIEW VRC RESULTS...');

disp('  ');

disp('(3) THEN USE KASSIGN FUNCTION TO ASSIGN K FOR EACH CODA LENGTH...')

help kassign

disp('  ');

disp('(4) NOW YOU MUST ADD THE CODA ASSIGNMENTS AS THE LAST TWO COLUMNS OF THE ORIGINAL CODA MATRIX,')
disp('      THEN RUN THE NAMECODAS FUNCTION ON THE CODA MATRIX')

help namecodas

disp('NB Names can be read into matlab using:  [clusters,names]=textread(''names.txt'',''%d%s'',''delimiter'','','')')

disp('  ');

disp('NOW, if desired, use clusplot to plot out clusters...');

disp('EXAMPLE:');
disp('v = kclus(jgcodas,2,0,''euc'')');
disp('a = kassign(v)');
disp('n = namecodas(jgcodas)');