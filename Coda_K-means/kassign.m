function a = kassign(v)
%a = kassign(v)
%Function takes vrc output v and processes kclus output (*clk.txt files
%assumed to be present in current directory) prompting user input of k
%Returns list of assignments in a, and saves it in 'cluster_assignments.mat'
%Luke Rendell March 2002

a = [];

%%%% find *clk.txt files
clickfiles = dir('*clk.txt');
if size(clickfiles,1)<1
    disp('NO *CLK.TXT FILES PRESENT!')
    return
end
    


%%%%%%%%% get values for k and bring assignments in
for n = 1:size(clickfiles,1); %loop through all click files
    length = str2double(clickfiles(n).name(1:(findstr(clickfiles(n).name,'clk.txt')-1))); %find length
    vrc = v(find(v(:,1)==length),2:10); %get vrc values for that length
    ass = textread(clickfiles(n).name); %read in assigments file from kclus output
    disp(clickfiles(n).name);
    ncod = size(ass,1);
    cla;
    set(gca,'Xlim',[1 10]);
    hold on;
    plot((2:10),vrc,'ko-');%plot vrc
    title([num2str(length) ' click codas; ' num2str(ncod) ' codas; (Press space for k=1)']);
    [x y b] = ginput(1);%get decision on k
    if b == 1
        k = round(x);
        disp([num2str(length) ' click codas, ' num2str(k) ' coda types selected']);
    else
        k = 1;
        disp([num2str(length) ' click codas, 1 coda types selected']);
    end
    ass = [ass(:,1) ass(:,k)]; %get appropriate assignments
    if k == 1; ass(:,2)=1;end;
    ass(:,3) = ass(:,2)+(length*10); %make unique cluster column
    a = [a;ass];
end
a = sortrows(a,1);
cluster_assignments = a;
save cluster_assignments.mat cluster_assignments
dlmwrite('cluster_assignments.txt',cluster_assignments,',');