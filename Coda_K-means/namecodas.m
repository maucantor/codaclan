function names = namecodas(codas);
%function names = namecodas(codas);
%Allows user to name coda types from kmeans
%codas is standard coda matrix with last two columns cluster and clusterID


clicks = unique(codas(:,4));
names = {};
fid = fopen('names.txt','w');
for c = 1:size(clicks,1) %loop through click sizes
    data = codas(codas(:,4)==clicks(c),:);
    clusters = unique(data(:,47));
    nclus = size(clusters,1)
    clusmeans = zeros(nclus,clicks(c)-1);
    for n = 1:nclus %loop through clusters
        clus = data(data(:,47)==clusters(n),:);
        clus = clus(:,6:3+clicks(c))./repmat(clus(:,5),1,clicks(c)-2);
        clus = mean(clus,1);
        clusmeans(n,:) = [0 clus];
    end
    %now change to cumulative
    if size(clusmeans,2)>2
        for n = 3:size(clusmeans,2);
            clusmeans(:,n) = clusmeans(:,n-1)+clusmeans(:,n);
        end
    end
    clusmeans(:,size(clusmeans,2)+1) = ones(size(clusmeans,1),1)
    cla;
    set(gcf,'Color',[1 1 1]);
    set(gca,'Xcolor', [1 1 1], 'ycolor', [1 1 1]);
    set(gca,'Ylim',[1 nclus+1]);
    hold on;
    for n = 1:nclus
    text(clusmeans(n,:),(ones(1,size(clusmeans,2)).*n),num2str(n));
    end
    
    for j = 1:nclus
    name = input([num2str(clicks(c)) ' Clicks, cluster ' num2str(j) ' - Name for this cluster? \n'],'s');
    disp([num2str(clusters(j)) ' - ' name]);
    out = [num2str(clusters(j)) ',' name '\n'];
    fprintf(fid,'%1s',out);
    names{end+1} = name;
    end
end
s =fclose(fid);
    