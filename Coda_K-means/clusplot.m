function [varex] = clusplot(codas,dat,num,col)
%function [varex] = clusplot(codas,dat,num,col)
%Function to plot clustering of repertoires
%input variable contains : coda# rec 0 nclick length ici1 ici2..ici40 cluster clusterID
%dat = 1 for standardised icis, 2 for absolute
%num = Plot option? 1 = up to 10 click only 2 = all up to 26 clicks >2 = single plot of x click codas \n>');
%col = plots clusters in colour if = 1, otherwise b&w
%function plots histogram of 3click codas, ici1 vs ici2 for 4 click codas and then first 2 principal components for others
%Luke Rendell October 2001

cols = {'k','r','g','b','m','c','y','k'}; %cell array of color strings
syms = {'o','+','v','^','*','s','p','h'}; %cell array of symbol strings
symsize = [4 6 5 4 4 4 4 4]; %array of marker sizes
pic=figure; %make figure
set(gcf,'Position',[10,10,720,520]);
varex = [];
if num>2; varex = oneplot(num,codas, dat,col); return; end %call single plot function if only one plot required
%array of figure positions
figpos = [0.2 0.73 0.3 0.2; 0.5 0.73 0.3 0.2; 0.2 0.52 0.3 0.2; 0.5 0.52 0.3 0.2; 0.2 0.31 0.3 0.2; 0.5 0.31 0.3 0.2; 0.2 0.1 0.3 0.2; 0.5 0.1 0.3 0.2];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%3 click codas
s = codas(codas(:,4)==3,:); %get 3 click codas
axes('Position',[0.2 0.73 0.3 0.2]); %make axes
set(gcf,'color',[1 1 1]);

switch dat
    
case 1 %if standardised icis only, then bar plot
[b bins] = hist(s(:,6)./s(:,5), 20); % get bins for histograms
hold on;
for n = 1:max(s(:,46)); %loop through clusters
    clus = s(s(:,46)==n,6)./s(s(:,46)==n,5); %get standardised first icis
    b = hist(clus, bins); %make histograms of each cluster
    bmax(n) = max(b); %get max value
    h = bar('v6',bins,b,cols{n});
end
hold off;
set(gca,'PlotBoxAspectRatio', [2 1 1],'Box','on', 'xtick',[], 'ytick',[]);
yl = get(gca,'Ylim');
yl(2) = max(bmax)+5; %set upper y limit
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
xl(1) = bins(1)*0.75; %set lower x limit
set(gca,'Xlim',xl)
[tx,ty] = tcal(xl,yl);


case 2 %if absolute icis then xy first and second interval 
hold on;
for n = 1:max(s(:,46)); %loop through clusters
    x = s(s(:,46)==n,6); %get first ici
    y = s(s(:,46)==n,7); %get second ici
    switch col
        case 1
            sym = [cols{n} syms{n}];
        otherwise
            sym = ['k' syms{n}];
    end
    h = plot(x,y,sym); %plot cluster
    set(h, 'MarkerSize',symsize(n)); %set marker size    
end
hold off;
set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio', [2 1 1],'Box','on', 'xtick',[],'ytick',[]);
yl = get(gca, 'YLim');
yl(1) = yl(1)-(0.1*yl(1));%expand axes
yl(2) = yl(2)+(0.1*yl(2));
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
[tx,ty] = tcal(xl,yl);
end

text(tx, ty, ['3 click']); %label plot

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%4 click codas
axes('Position',[0.5 0.73 0.3 0.2]); %make axes
set(gcf,'color',[1 1 1]);
s = codas(codas(:,4)==4,:); %get 4 click codas
legendhandles = [];

switch dat
    
case 1 %if standardised icis only
hold on;
maxclus = max(s(:,46));
for n = 1:max(s(:,46)); %loop through clusters
    x = s(s(:,46)==n,6)./s(s(:,46)==n,5); %get standardised first icis
    y = s(s(:,46)==n,7)./s(s(:,46)==n,5); %get standardised second icis
    switch col
        case 1
            sym = [cols{n} syms{n}];
        otherwise
            sym = ['k' syms{n}];
    end
    h = plot(x,y,sym); %plot cluster
    legendhandles = [legendhandles; h];
    set(h, 'MarkerSize',symsize(n)); %set marker size
end
hold off;
str = ['4 click'];

case 2 %if absolute icis
s = [s(:,46), s(:,6:8)]; %get matrix of cluster membership and icis
[pe,pcdat,vs,tsq] = princomp(s(:,2:4));% principal components on data
s = [s(:,1) pcdat(:,1:2)]; %get first two principal components
vs = 100*vs/sum(vs)
varex(size(varex,1)+1,:) = [4 (vs(1) + vs(2))]; % put in percent variance explained by first two pcs
hold on;
for j = 1:max(s(:,1)); %loop through clusters
    x = s(s(:,1)==j,2); %get first pc
    y = s(s(:,1)==j,3); %get second pc
    switch col
        case 1
            sym = [cols{j} syms{j}];
        otherwise
            sym = ['k' syms{j}];
    end
    h = plot(x,y,sym); %plot cluster
    set(h, 'MarkerSize',symsize(j)); %set marker size
end
hold off;
str(1) = {'4 click'};
st = ['(' num2str(round(vs(1)+vs(2))) '%)'];
str(2) = {st};
vs = [];
st = [];
end
    
set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio', [2 1 1],'Box','on', 'xtick',[],'ytick',[]);
yl = get(gca, 'YLim');
yl(1) = yl(1)-(0.1*yl(1));%expand axes
yl(2) = yl(2)+(0.1*yl(2));
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
[tx,ty] = tcal(xl,yl);
text(tx, ty, str); %label plot
clear str

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5-10 click codas
for n = 5:10 
axes('Position',figpos(n-2,:)); %make axes
set(gcf,'color',[1 1 1]);
s = codas(codas(:,4)==n,:); %get n click codas
switch dat
    
case 1 %if standardised icis 
s = [s(:,46) s(:,6:n+4)./repmat(s(:,5),1,n-1)]; %get matrix of cluster membership and standardised icics
[pe,pcdat,vs,tsq] = princomp(s(:,2:n));% principal components on standardised icis

case 2 %if absolute icis
s = [s(:,46) s(:,6:n+4)]; %get matrix of cluster membership and absolute icics
[pe,pcdat,vs,tsq] = princomp(s(:,2:size(s,2)));% principal components on absolute icis

end
    
s = [s(:,1) pcdat(:,1:2)]; %get  first two principal components
vs = 100*vs/sum(vs);
varex(size(varex,1)+1,:) = [n (vs(1) + vs(2))]; % put in percent variance explained by first two pcs
hold on;
for j = 1:max(s(:,1)); %loop through clusters
    x = s(s(:,1)==j,2); %get first pc
    y = s(s(:,1)==j,3); %get second pc
        switch col
        case 1
            sym = [cols{j} syms{j}];
        otherwise
            sym = ['k' syms{j}];
    end
    h = plot(x,y,sym); %plot cluster
    set(h, 'MarkerSize',symsize(j)); %set marker size
end
hold off;
set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio', [2 1 1],'Box','on', 'xtick',[],'ytick',[]);
set(gca,'xtick',[],'ytick',[]);

yl = get(gca, 'YLim');
yl(1) = yl(1)-(0.2*abs(yl(1)));%expand axes
yl(2) = yl(2)+(0.1*yl(2));
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
[tx,ty] = tcal(xl,yl);
st = [num2str(n) ' click'];
str(1) = {st};
st = ['(' num2str(round(vs(1)+vs(2))) '%)'];
str(2) = {st};
st = [];
text(tx, ty, str);
vs = [];

end

if num==2 %if all to be plotted

pic=figure; %make new figure
set(gcf,'Position',[10,10,720,520]);

    
for n = 11:18 %11-18 click codas
axes('Position',figpos(n-10,:)); %make axes
set(gcf,'color',[1 1 1]);
s = codas(codas(:,4)==n,:); %get n click codas
if size(s,1)<2; nocod(n); continue ;end %bail if less than two codas
switch dat
    
case 1    
s = [s(:,46) s(:,6:n+3)./repmat(s(:,5),1,n-2)]; %get matrix of cluster membership and standardised icics
[pe,pcdat,vs,tsq] = princomp(s(:,2:n-1));% principal components on standardised icis

case 2 %if absolute icis
s = [s(:,46) s(:,6:n+4)]; %get matrix of cluster membership and absolute icics
[pe,pcdat,vs,tsq] = princomp(s(:,2:size(s,2)));% principal components on absolute icis

end
    
s = [s(:,1) pcdat(:,1:2)]; %get  first two principal components
vs = 100*vs/sum(vs);
varex(size(varex,1)+1,:) = [n (vs(1) + vs(2))]; % put in percent variance explained by first two pcs
vs = [];
hold on;
for j = 1:max(s(:,1)); %loop through clusters
    x = s(s(:,1)==j,2); %get first pc
    y = s(s(:,1)==j,3); %get second pc
    switch col
        case 1
            sym = [cols{j} syms{j}];
        otherwise
            sym = ['k' syms{j}];
    end
    h = plot(x,y,sym); %plot cluster
    set(h, 'MarkerSize',symsize(j)); %set marker size
end
hold off;
set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio', [2 1 1],'Box','on', 'xtick',[],'ytick',[]);
yl = get(gca, 'YLim');
yl(1) = yl(1)-(0.2*abs(yl(1)));%expand axes
yl(2) = yl(2)+(0.1*yl(2));
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
[tx,ty] = tcal(xl,yl);
str = [num2str(n) ' click'];
text(tx, ty, str);
end
    

pic=figure; %make new figure
set(gcf,'Position',[10,10,720,520]);

for n = 19:26 %19-26 click codas
axes('Position',figpos(n-18,:)); %make axes
set(gcf,'color',[1 1 1]);
s = codas(codas(:,4)==n,:); %get n click codas
if size(s,1)<2; nocod(n); continue ;end %bail if less than two codas
switch dat
    
case 1    
s = [s(:,46) s(:,6:n+3)./repmat(s(:,5),1,n-2)]; %get matrix of cluster membership and standardised icics
[pe,pcdat,vs,tsq] = princomp(s(:,2:n-1));% principal components on standardised icis

case 2 %if absolute icis
s = [s(:,46) s(:,6:n+4)]; %get matrix of cluster membership and absolute icics
[pe,pcdat,vs,tsq] = princomp(s(:,2:size(s,2)));% principal components on absolute icis

end
    
s = [s(:,1) pcdat(:,1:2)]; %get  first two principal components
vs = 100*vs/sum(vs);
varex(size(varex,1)+1,:) = [n (vs(1) + vs(2))]; % put in percent variance explained by first two pcs
vs = [];
hold on;
for j = 1:max(s(:,1)); %loop through clusters
    x = s(s(:,1)==j,2); %get first pc
    y = s(s(:,1)==j,3); %get second pc
    switch col
        case 1
            sym = [cols{j} syms{j}];
        otherwise
            sym = ['k' syms{j}];
    end
    h = plot(x,y,sym); %plot cluster
    set(h, 'MarkerSize',symsize(j)); %set marker size
end
hold off;
set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio', [2 1 1],'Box','on', 'xtick',[],'ytick',[]);
yl = get(gca, 'YLim');
yl(1) = yl(1)-(0.2*abs(yl(1)));%expand axes
yl(2) = yl(2)+(0.1*yl(2));
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
[tx,ty] = tcal(xl,yl);
str = [num2str(n) ' click'];
text(tx, ty, str);
end

end

legend(legendhandles,num2str([1:max(codas(:,46))]'));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [varex] = oneplot(nc,codas,dat,col) %if only one plot required of nclick codas
cols = {'k','r','g','b','m','c','y','k'}; %cell array of color strings
syms = {'o','+','v','^','*','s','p','h'}; %cell array of symbol strings
symsize = [4 6 5 4 4 4 4 4]; %array of marker sizes
varex = [];
switch nc
    
case 3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%3 click codas
s = codas(codas(:,4)==3,:); %get 3 click codas
axes; %make axes
set(gcf,'color',[1 1 1]);

switch dat
    
case 1 %if standardised icis only, then bar plot
[b bins] = hist(s(:,6)./s(:,5), 20); % get bins for histograms
hold on;
for n = 1:max(s(:,46)); %loop through clusters
    clus = s(s(:,46)==n,6)./s(s(:,46)==n,5); %get standardised first icis
    b = hist(clus, bins); %make histograms of each cluster
    bmax(n) = max(b); %get max value
    bar('v6',bins,b,cols{n})
end
hold off;
set(gca,'PlotBoxAspectRatio', [2 1 1],'Box','on');
yl = get(gca,'Ylim');
yl(2) = max(bmax)+5; %set upper y limit
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
xl(1) = bins(1)*0.75; %set lower x limit
set(gca,'Xlim',xl)
[tx,ty] = tcal(xl,yl);


case 2 %if absolute ici then xy first/second interval
hold on;
for n = 1:max(s(:,46)); %loop through clusters
    x = s(s(:,46)==n,6); %get first ici
    y = s(s(:,46)==n,7); %get second ici
    switch col
        case 1
            sym = [cols{n} syms{n}];
        otherwise
            sym = ['k' syms{n}];
    end
    h = plot(x,y,sym); %plot cluster
    set(h, 'MarkerSize',symsize(n)); %set marker size    
end
hold off;
set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio', [2 1 1],'Box','on');
yl = get(gca, 'YLim');
yl(1) = yl(1)-(0.1*yl(1));%expand axes
yl(2) = yl(2)+(0.1*yl(2));
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
[tx,ty] = tcal(xl,yl);
end

text(tx, ty, ['3 click']); %label plot

    
case 4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%4 click codas
axes; %make axes
set(gcf,'color',[1 1 1]);
s = codas(codas(:,4)==4,:); %get 4 click codas
lh = [];

switch dat
    
case 1 %if icis only
hold on;
for n = 1:max(s(:,46)); %loop through clusters
    x = s(s(:,46)==n,6)./s(s(:,46)==n,5); %get standardised first icis
    y = s(s(:,46)==n,7)./s(s(:,46)==n,5); %get standardised second icis
    switch col
        case 1
            sym = [cols{n} syms{n}];
        otherwise
            sym = ['k' syms{n}];
    end
    h = plot(x,y,sym); %plot cluster
    lh = [lh h];
    set(h, 'MarkerSize',symsize(n)); %set marker size
    xlabel('ICI 1')
    ylabel('ICI 2')
end
hold off;
str = ['4 click'];

case 2 %if absolute icis
s = [s(:,46), s(:,6:8)]; %get matrix of cluster membership and icis
[pe,pcdat,vs,tsq] = princomp(s(:,2:4));% principal components on data
s = [s(:,1) pcdat(:,1:2)]; %get first two principal components
vs = 100*vs/sum(vs);
varex(size(varex,1)+1,:) = [4 (vs(1) + vs(2))]; % put in percent variance explained by first two pcs
hold on;
for j = 1:max(s(:,1)); %loop through clusters
    x = s(s(:,1)==j,2); %get first pc
    y = s(s(:,1)==j,3); %get second pc
    switch col
        case 1
            sym = [cols{j} syms{j}];
        otherwise
            sym = ['k' syms{j}];
    end
    h = plot(x,y,sym); %plot cluster
    set(h, 'MarkerSize',symsize(j)); %set marker size
end
hold off;
str(1) = {'4 click'};
st = ['(' num2str(round(vs(1)+vs(2))) '%)'];
str(2) = {st};
vs = [];
st = [];
end

set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio', [2 1 1],'Box','on');%, 'xtick',[],'ytick',[]);
yl = get(gca, 'YLim');
yl(1) = yl(1)-(0.1*yl(1));%expand axes
yl(2) = yl(2)+(0.1*yl(2));
set(gca,'Ylim',yl);
xl = get(gca,'Xlim');
[tx,ty] = tcal(xl,yl);
text(tx, ty, str); %label plot
legend(lh,{'Cluster 1','Cluster 2'});    

otherwise %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%other coda lengths
    axes; %make axes
    set(gcf,'color',[1 1 1]);
    s = codas(codas(:,4)==nc,:); %get n click codas
    if size(s,1)<2; nocod(nc); return ;end %bail if less than two codas
    switch dat
    
    case 1 %if standardised icis only
    s = [s(:,46) s(:,6:nc+4)./repmat(s(:,5),1,nc-1)]; %get matrix of cluster membership and standardised icics
    [pe,pcdat,vs,tsq] = princomp(s(:,2:nc));% principal components on standardised icis

    case 2 %if absolute icis
    s = [s(:,46) s(:,6:nc+4)]; %get matrix of cluster membership, LENGTHS and standardised icics
    [pe,pcdat,vs,tsq] = princomp(s(:,2:size(s,2)));% principal components on standardised icis

    end

    s = [s(:,1) pcdat(:,1:2)]; %get  first two principal components
    vs = 100*vs/sum(vs);
    varex(size(varex,1)+1,:) = [nc (vs(1) + vs(2))]; % put in percent variance explained by first two pcs
    hold on;
    for j = 1:max(s(:,1)); %loop through clusters
        x = s(s(:,1)==j,2); %get first pc
        y = s(s(:,1)==j,3); %get second pc
        switch col
            case 1
                sym = [cols{j} syms{j}];
            otherwise
                sym = ['k' syms{j}];
        end
        h = plot(x,y,sym); %plot cluster
        set(h, 'MarkerSize',symsize(j)); %set marker size
    end
    hold off;
    %set(gca,'DataAspectRatio',[1 1 1]);
    yl = get(gca, 'YLim');
    %yl(1) = yl(1)-(0.2*abs(yl(1)));%expand axes
    %yl(2) = yl(2)+(0.1*yl(2));
    set(gca,'Ylim',[-0.25,0.25],'Xlim', [-0.2 0.3],'Box','on');
    xl = get(gca,'Xlim');
    [tx,ty] = tcal(xl,yl);
    st = [num2str(nc) ' click'];
    str(1) = {st};
    st = ['(' num2str(round(vs(1)+vs(2))) '%)'];
    str(2) = {st};
    st = [];
    text(tx, ty, str);
    vs = [];
    
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function nocod(n) %if there are no codas x long
set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio', [2 1 1],'Box','on', 'xtick',[],'ytick',[]);
yl = get(gca, 'YLim');
xl = get(gca,'Xlim');
[tx,ty] = tcal(xl,yl);
str = ['<2 ' num2str(n) ' click codas in repertoire'];
text(tx,ty,str);
