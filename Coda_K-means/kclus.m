function [v] = kclus(codas,iter,inp,method)
%function [v] = kclus(codas,iter,inp,method)
%routine to calculate VRC for k = 1:10 for each coda size
%
%works on workspace variable codas, matrix or cell array containing 
%coda# recording date nclicks length(s) 1 2 3 4 5 6 up to 40
%
%calculates Calinski & Harabasz Variance Ratio Criterion for each k
%outputs assignment matrix for each coda length and vrc matrix for all lengths combined
%
%iter = 2 for 20 kmeans runs
%inp = 1 for cell array input
%method = 'euc' for euclidean distance, 'inf' for infinity norm distance
%
%SAVE *CLK.TXT FILES FOR EACH LENGTH, AND V MATRIX AS V.MAT
%USES ALL CLICK INTERVALS
%USES SOM TOOLBOX 'BATCH' KMEANS ALGORITHM
%By Luke Rendell Sept 2001
%Modified Oct 2003

if inp == 1
    %loop into standard array
    for x = 1:size(codas,1)
        for y = 1:size(codas,2)
            if y==3
                c(x,y) = 0;
            else
                c(x,y) = codas{x,y};
            end
        end
    end  
    clear x y codas
    codas = c;
end
clear inp

%find coda sizes present
codlengths = unique(codas(:,4));
mcod = size(codlengths,1);

%loop through coda sizes
for clks = 1:mcod
    %get codas clks clicks long
    codlength = codlengths(clks);
    s = codas(codas(:,4)==codlength,:);
    %proceed only if codas exist!
    disp([num2str(codlength) ' clicks, ' num2str(size(s,1)) ' codas']);
    if size(s,1)>0
        ncod = size(s,1);
        nk = ncod;
        %if less than 10 codas cannot assign k = more than number of codas
        if nk>10
            nk = 10;
        end
        %if less than 2 codas won't even make that and must trim from codlengths
        if nk<2
            codlengths(clks)=0;
        end
        %enter coda numbers into output matrix
        a(:,1) = s(:,1);
        %get length and icis only
        s = s(:,5:codlength+4);
        %standardise
        lengths = s(:,1);%set up matrix to standardise icis
        lengths = lengths(:,ones(codlength-1,1));
        s(:,2:size(s,2)) = s(:,2:size(s,2))./lengths;%standardise icis
        clear lengths;
        s = s(:,2:size(s,2)); %icis only
        s1=s;
        %calculate whole SS (tss)
        xu=s-ones(length(s(:,1)),1)*mean(s);
        tss = sum(sum(xu.*xu));
        clear xu
        %get within ss (wss) and assignments for k = 2:10 (or ncod if less than 10)
        for n = 2:nk
            warning off;
            %initial kmeans run
            [clusters aa(:,1) wss] = kmeans2(method,s,n);
            %calculate VRC
            bss = tss-wss; %calculate between group sum of square
            vv(1) = (bss/(n-1))/(wss/(ncod-n)); 
            low = 1; %first run is initially lowest
            mss = wss; %minimum ss
            wss = 0; %reset wss
            %if required, loop through 10 trials (9 more)
            if iter == 2
                for t = 2:20
                    [clusters aa(:,t) wss] = kmeans2(method,s,n); %rerun kmeans
                    %calculate VRC
                    bss = tss-wss; %calculate between group sum of square
                    vv(t) = (bss/(n-1))/(wss/(ncod-n)); %calculate vrc
                    %disp(['TSS = ' num2str(tss) '   WSS = ' num2str(wss)])
                    if wss<mss %if within group ss less for this run, accept this trial
                        %disp(['CHANGE : On run ' sprintf('%1.0f',t) ' : Old SS - ' sprintf('%2.4f',ss) ', New SS - ' sprintf('%2.4f', ss1)]);
                        mss = wss;
                        wss = 0; %reset wss
                        low = t; %change flag
                    end
                end
            end
            v(clks, n) = vv(low); %assign VRC for lowest WGSS solution 
            a(:,n) = aa(:,low); %assignments from same
            clear vv aa ss mv
            warning backtrace;
            %display results
            disp(['k = ' sprintf('%1.0f',n) ',WGSS = ' num2str(mss) ',VRC = ' sprintf('%5.5f',v(clks,n))]);
            if iter == 2; disp(['    Lowest within group SS on Run #' sprintf('%1.0f',low)]);end
            
        end
        %if only a single coda assign it to cluster 1(!)
        if size(a,1)==1;a(1,2)=1;end;
        %save assignments
        if codlengths(clks)>0
            file = [num2str(codlengths(clks)) 'clk.txt'];
            dlmwrite(file,a,'\t');
        end
    end
    %clear variables ready for next coda length
    clear a s n ncod low t mv mss tss;
%next coda length
end
datIn = find(codlengths);
v = v(datIn,:);
codlengths = codlengths(datIn);
v(:,1) = codlengths;
save v.mat v;
