function vplot(v);
%plots VRC for various coda lengths; input variable = v from kclus.m
vv = find(v(:,2));
%plot vrcs for different coda lengths
pl=1;
for n = 1:size(vv,1)
    subplot(4,1,pl), h = plot([2:size(v,2)],v(vv(n),2:size(v,2)),'-ko');
    set (h,'MarkerFaceColor','black');
    pl=pl+1;
    yl = get(gca,'Ylim');
    yl(1) = 0;
    set(gca,'Ylim',yl);
    set(gca,'ytick',yl);
    text(0.5, yl(1)+(0.5*(yl(2)-yl(1))),[num2str(vv(n)+2) ' clicks']);
    xlabel('\itk');
    %ylabel('VRC');
    set(gca,'XLim', [0 10]);
    if rem(n,4) == 0; pl=1;h=figure;figure(h);end
end