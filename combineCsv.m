function [csvfile] = CombineCsv(output,outputpath)

%Reads csv files in a given folder and combine them into a single csv file
%and saves as .mat file as well
%'output' is the name for the final file
%'outputpath' is the desired folder path to save the file

[file,path] = uigetfile('*.csv','Find the File to Import','MultiSelect', 'on');

%empty object to receive the csv data
csvAll = [];

%read and concatenate files
for n=1:length(file)
  filename = fullfile ( path, file{n} );
   x = dlmread(filename,',');
   csvAll = [csvAll;x];
end

csvfile = csvAll;

%write single csv and .mat files
fullFileName = fullfile(outputpath, [output '.csv']);
dlmwrite(fullFileName, csvAll,'delimiter',',','precision','%.6f');
%dlmwrite([output '.csv'], csvAll,'delimiter',',','precision','%.6f')
%save([outputpath '/' output '.mat'], 'csvAll');

disp(' ');
disp('Final csv file has been written to working directory:');
disp(outputpath);