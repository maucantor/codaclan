function codaRhythmPlots(dataCodasType, maxCodaLength, ICI, codanames)

%function rhythmplot(dataCodasType, maxCodaLength, ICI)
%
%INPUT:
%'dataCodasType': standard dataCodas with the 46th column giving the coda
%type, either via kmeans or OPTICSxi, or something else. Necessary columns are: 
%1st(coda#), 2nd(date or rec#), 3rd(repertoire), 4th(Nclicks), 5th(total length),
%6-40th(ICIs), 46th(coda type).
%
%'maxCodaLength': a number giving the maximum number of clicks for the
%codas. Codas longer than this will be ignored
%
%'ICI': 'absolute' or 'standardized'
%
%'codanames': vector of strings giving coda labels (e.g. 3R, 1+2 etc). If
%codanames=0, a sequential nuber will be used (XYY, where X: number of
%clicks and YY sequential number)
%
% Coded by Shane Gero; modified by Mauricio Cantor, June 2015
%

if size(dataCodasType,2) < 46
    disp('There are no 46th column with coda types');
end


% ICI columns:
%getting all ICIs columns, up to the chosen number of maximum coda length
rawici = dataCodasType(:, 6:(6+(maxCodaLength-2)) );

%removing the samples (rows), with codas longer than the chosen maximum
codaLengths = unique(dataCodasType(:,4));
removeLengths = codaLengths(find(codaLengths > maxCodaLength));
removeRows = ismember(dataCodasType(:,4), removeLengths);
rawici(removeRows,:) = [];

% coda types: column, restricting to given maxCodaLength
cType = dataCodasType(:,46);
chosenType = find(ismember(dataCodasType(:,4), [3:maxCodaLength]));
cType = cType(chosenType);

%relabeling coda types to sequential numbers (for plotting)
types = unique(cType);
nType = zeros(size(cType));
for t = 1:numel(types)
    nType = nType+ismember(cType,types(t)).*t;
end
ybar = [1:length(types)]';
    
 switch ICI
    case 'absolute'

        %%%Rythm Plots
        clicks=cumsum(rawici,2);%%clicks=cumsum(rawici(1:end,:),2);%cummulative sum of the icis to get click timing
        zerostart=zeros(length(rawici),1);%column of zeros
        clicks=[zerostart clicks];%puts column of zeros in clicks matrix to standardize start click
        [xbar names meanci numbers] =  grpstats(clicks,cType,{'mean','gname','meanci','numel'});
        nclicks=size(clicks,2);%number of clicks
        high=meanci(:,:,2)-xbar;%value from mean to high value of 95%CI around mean
        low=xbar-meanci(:,:,1);%value from mean to low values of 95%CI around mean
        maxy=(max(xbar(:,nclicks)+(high(:,nclicks))))+0.1;%maximum value for msec


        %plots Absolute figure
        figure
        set(gcf,'color','w');
        %subplot(2,1,1)
        h = errorbar(repmat(ybar,1,nclicks)',xbar',low',high','k.');%makes plot
        set(h,'MarkerSize',20);
            if iscell(codanames)
                names = codanames;
            end
        set(gca,'Xtick',[1:length(types)],'Xticklabel',names)%gives type names on Y
        %view(-90,90)%flips data from flipped errorbar command
        %set(gca, 'ydir', 'reverse','ylim',[-0.1 maxy],'box','off','Fontsize',12);%sets properties of true X axis
        view(90,90)
        set(gca,'ylim',[0 maxy],'box','off','Fontsize',12);
        text(ybar,fliplr(ones((length(types)),1)*maxy),num2str(numbers(:,1)),'Fontsize',12);%gives sample sizes along right side
        xlabel('Coda Types','Fontsize',14)
        ylabel('Absolute Time (s)','Fontsize',14)

    case 'standardized'    
         
        stan = rawici./repmat(sum(rawici,2),1, size(rawici,2) );%standardized ICI for naming
        stanclicks=cumsum(stan(1:end,:),2);%cummulative sum of the icis to get click timing
        zerostart=zeros(length(stan),1);%column of zeros
        stanclicks=[zerostart stanclicks];%puts column of zeros in clicks matrix to standardize start click
        [stanxbar stannames stanmeanci stannumbers] =  grpstats(stanclicks,cType,{'mean','gname','meanci','numel'});
        stannclicks=size(stanclicks,2);%number of clicks
        stanhigh=(stanmeanci(:,:,2)-stanxbar)/5;%value from mean to high value of 95%CI around mean
        stanlow=(stanxbar-stanmeanci(:,:,1))/5;%value from mean to low values of 95%CI around mean
        stanmaxy=(max(stanxbar(:,stannclicks)+(stanhigh(:,stannclicks))))+0.1;%maximum value for msec

        %plots Standardized figure
        figure
        set(gcf,'color','w');
        %subplot(2,1,2)
        h = errorbar(repmat(ybar,1,stannclicks)',stanxbar',stanlow',stanhigh','k.');%makes plot
        set(gca,'Xtick',[1:length(types)],'Xticklabel',stannames)%gives type names on Y
        view(-90,90)%flips data from flipped errorbar command
        set(gca, 'ydir', 'reverse','ylim',[-0.1 1.1],'box','off','Fontsize',12);%sets properties of true X axis
        text(ybar,ones((length(types)),1)*stanmaxy,num2str(stannumbers(:,1)),'Fontsize',10);%gives sample sizes along right side
        xlabel('Coda Types','Fontsize',12)
        ylabel('Standardized Time (sec)','Fontsize',12)

     otherwise
       disp('ERROR: "ICI" must be "absolute" or "standardized"')
       
 end

end