%reads in iddata and processes it
%a=wk1read('C:\..\Data\iddat.wk1');
a=xlsread('AllIDdata.xls');
qq=find((a(:,6)>2.5)&((a(:,7)<500)|(a(:,7)>599))&(a(:,7)>10));
%no males, calves or poor quality 
nqq=length(qq);%number of records
id=a(qq,4);%ID
dayd=a(qq,1);%day
yq=datevec(dayd);year=yq(:,1);
hour=(a(qq,5)-a(qq,1))*24;
area=a(qq,8);
sameday=[1 (dayd(2:nqq)~=dayd(1:(nqq-1)))'];
dayn=cumsum(sameday);
stday=find(sameday);
enday=[stday(2:length(stday))-1 nqq];
nday=enday-stday;
poday=((1:nqq)-stday(dayn))./(0.00001+nday(dayn));%position of that sighting within the day's sightings
idc=zeros(1,1:max(id)); idc(id)=1;idl=find(idc);idc(idl)=1:length(idl);
sd=spones(sparse(dayn,idc(id),1));ss=full(sum(sd,2));%animals seen all day
disp('  ');
disp('Estimates of group size');
tit=['Morning/afternoon';'Split in half    '];
tit1=['Divided by seen both/total  ';'Divided by estimated cv     '];  
for t=1:2
   disp('   ');
   disp(tit(t,:));
   if t==1;meas=hour/24;else meas=poday;end
   sdm=spones(sparse(dayn,idc(id),meas<=0.5));sm=full(sum(sdm,2));%seen 1st half
   sda=spones(sparse(dayn,idc(id),meas>0.5));sa=full(sum(sda,2));%seen 2nd half
   nsb=sm+sa-ss;%number seen both
   gse=((sm+1).*(sa+1)./(nsb+1))-1;%group size estimate
   varn=(sm+1).*(sa+1).*(sm-nsb).*(sa-nsb)./(((nsb+1).^2).*(nsb+2));%variance
   cv=sqrt(varn)./gse;
   if (sa.*sm)==0;gse=0;cv=10000;end
   for t1=2:2
      disp('   ');
      disp(tit1(t1,:));
      disp('                Interval          Cumulative         Cumulative--typical')    
      disp('Range        n    mgs    sd     n   mgs    sd  c(sd)   mgs    sd  c(sd)');
      ratch=0.05;
      for rat=0:ratch:1;
         gg=find((cv>=rat)&(cv<(rat+ratch))&sa&sm);
         gh=find((cv<(rat+ratch))&sa&sm);
         typm=sum(gse(gh).^2)/sum(gse(gh));
         typs=sqrt((sum(gse(gh).^3)-typm^2*sum(gse(gh)))/(sum(gse(gh))-1));
         ctyps=sqrt(typs^2-sum(varn(gh).*gse(gh))/sum(gse(gh)));
         if length(gg)>1;disp(sprintf('%4.2f-%4.2f  %3.0f  %6.2f %5.2f  %3.0f  %5.2f %5.2f %5.2f  %5.2f %5.2f %5.2f',rat,(rat+ratch),length(gg),mean(gse(gg)),std(gse(gg)),length(gh),mean(gse(gh)),std(gse(gh)),sqrt(std(gse(gh))^2-mean(varn(gh))),typm,typs,ctyps));end
      end
      disp('                Cumulative         Cumulative--typical')    
      disp('Max(cv) Area   n   mgs    sd  c(sd)   mgs    sd  c(sd)');

      for cvmax=[0.25 0.4]
      for ar=1:4
         gh=find((cv<cvmax)&sa&sm&(ar==area(stday)));
         typm=sum(gse(gh).^2)/sum(gse(gh));
         typs=sqrt((sum(gse(gh).^3)-typm^2*sum(gse(gh)))/(sum(gse(gh))-1));
         ctyps=sqrt(typs^2-sum(varn(gh).*gse(gh))/sum(gse(gh)));
         if length(gh>1);disp(sprintf('%4.2f %4.0f  %3.0f  %6.2f %5.2f  %5.2f  %5.2f %5.2f %5.2f  %5.2f %5.2f %5.2f',cvmax,ar,length(gh),mean(gse(gh)),std(gse(gh)),sqrt(std(gse(gh))^2-mean(varn(gh))),typm,typs,ctyps));end
      end
      end
      for cvmax=[0.25 0.4]
      for yr=85:99
         gh=find((cv<cvmax)&sa&sm&(yr==year(stday))&(1==area(stday)));
         if length(gh>1)
            typm=sum(gse(gh).^2)/sum(gse(gh));
         typs=sqrt((sum(gse(gh).^3)-typm^2*sum(gse(gh)))/(sum(gse(gh))-1));
         ctyps=sqrt(typs^2-sum(varn(gh).*gse(gh))/sum(gse(gh)));
         disp(sprintf('%4.2f %4.0f  %3.0f  %6.2f %5.2f  %5.2f  %5.2f %5.2f %5.2f  %5.2f %5.2f %5.2f',cvmax,yr,length(gh),mean(gse(gh)),std(gse(gh)),sqrt(std(gse(gh))^2-mean(varn(gh))),typm,typs,ctyps));
      end
      end
      end
      subplot(2,1,t);
      %plot(cv,gse,'.');set(gca,'xlim',[0 1]);
      ql=find((cv<0.25)&sa&sm);
      for j=ql';
         disp([datestr(dayd(stday(j))) sprintf('  %1.0f  %3.0f %3.0f %3.0f %3.0f  %5.2f  %5.3f',area(stday(j)),ss(j),sm(j),sa(j),nsb(j),gse(j),cv(j))]);
      end   
   end
end
ql=find((cv<0.25)&sa&sm);
ql4=find((cv>=0.25)&(cv<0.4)&sa&sm);
ra=2.5:5:97.5;hr25=hist(gse(ql),ra);hr4=hist(gse(ql4),ra);bar(ra,[hr25;hr4]',1,'stack');colormap(gray);
xlabel('Estimated group size')
ylabel('Number of groups (days)')
legend('cv<0.25','0.25<cv<0.4')
set(gcf,'color',[1 1 1])
