function codaPCAclan(dataCodas, minLen, maxLen, clan, loadings, rotate)
% codaPCA(dataCodas, Nclicks)
%
% Plots PCA on interclick intervals for several coda types, coloured by
% clans, with plot shapes by coda types
%
% INPUT:
%'dataCodas': standard dataCodas format, with additional 46th column giving
%the coda types, either via kmeans or OPTICSxi, or something else. Strictly 
%necessary columns are: 6th-40th(ICIs), 46th(coda type). Others may contain:
%1st(coda#), 2nd(date or rec#), 3rd(repertoire), 4th(Nclicks), 5th(total length)
%
%'minLen': number. Minimum coda length, in number of clicks. Usually 3.
%
%'maxLen': number. Maximum coda length, in number of clicks. Usually 12.
%
%'clan': a numerical vector with the same size of nrows in dataCodas giving
%the clan labels
%0:GAL1314,1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
%black, green, blue, yellow, cian, red, magenta
%
%'loadings': if 1, plot a separate figure with the loadings;
%
%'rotate': if loadings=1, and rotate=1, the varimax rotation is used
%
% Mauricio Cantor, Aug 2015

dataCodas(:,47) = clan;


    %0:GAL1314,1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
    %black, green, blue, yellow, cian, red, magenta
  color = 'kgbycrm';
    %shape for different coda types
  shape = 'o^sdvh*x.';
  
  
    for i=minLen:maxLen
        
        %raw data per coda length, without noise
        auxdata = dataCodas(dataCodas(:,4)==i, :);
        %ICIs
        auxici = auxdata(:, 6:(6+(i-2)));
        %PCA
        [coeff score latent tsquared explained] = pca(auxici);

            %Plotting scores
            figure(1);
            subplot( round((maxLen-minLen+1)/2) ,2, (i-2))
            set(gcf,'color','w');

            %Color = clan, shape = coda type
            auxscore1 = [score auxdata(:,46) auxdata(:,47)];
                for j=0:6
                    auxscore2 = auxscore1(auxscore1(:,end)==j, :);
                    shape1 = num2str(unique(auxscore2(:,end-1)));
                    if ~isempty(shape1)
                        shape1 = str2num(shape1(:,end));
                        shape1 = shape(shape1);
                        gscatter(auxscore2(:,1), auxscore2(:,2), auxscore2(:,end-1), ...
                            color(j+1), shape1, 5, 'off')
                        hold on;
                    end
                end
            
            %title, axis scale and labels
            %axis([-1, 1, -1, 1]);
            axis([min(auxscore1(:,1))*1.2, max(auxscore1(:,1))*1.2, ...
                min(auxscore1(:,2))*1.2, max(auxscore1(:,2))*1.2]);
            title([num2str(i) '-click'])
            format bank
            aux1=num2str(round(explained(1),1)); aux2=num2str(round(explained(2),1));
            xlabel(['PC1 (' aux1 '%)'])
            ylabel(['PC2 (' aux2 '%)'])
            set(gca,'YTickLabel',[],'XTickLabel',[])
            hold off;

            
            %Plotting loadings
            if loadings==1
                figure(2)
                subplot( round((maxLen-minLen+1)/2) ,2, (i-2))  
                set(gcf,'color','w');

                %create variable names (ICIs)
                vlabs={};
                for j=1:size(coeff,2)
                    vlabs{j}= char(['ICI' num2str(j)]);
                end

                %Rotate axes? (varimax)
                    if rotate==1
                        [coeffv T]= rotatefactors(coeff(:,1:2), 'Method', 'varimax', 'Normalize', 'off'); 
                        %biplot(coeffv, 'varlabels',vlabs, 'Scores',  score(:,1:2)*T');
                        
                        %Color = clan, shape = coda type
                        figure(1);
                        auxscore1 = [score auxdata(:,46) auxdata(:,47)];
                            for j=0:6
                                auxscore2 = auxscore1(auxscore1(:,end)==j, :);
                                shape1 = num2str(unique(auxscore2(:,end-1)));
                                if ~isempty(shape1)
                                    shape1 = str2num(shape1(:,end));
                                    shape1 = shape(shape1);
                                    rotatedscore = auxscore2(:,1:2)*T.'';
                                    gscatter(rotatedscore(:,1), rotatedscore(:,2), auxscore2(:,end-1), ...
                                        color(j+1), shape1, 5, 'off')
                                    hold on;
                                end
                            end
                            rotscore = score(:,1:2)*T';
                axis([min(rotscore(:,1))*1.2, max(rotscore(:,1))*1.2, ...
                min(rotscore(:,2))*1.2, max(rotscore(:,2))*1.2]);
                            
                            figure(2);
                            biplot(coeffv, 'varlabels',vlabs, 'Scores',  score(:,1:2)*T');

                        
                        
                        
                    else
                        biplot(coeff(:,1:2), 'varlabels',vlabs);%, 'Scores', score(:,1:2));
                    end
                    
                grid off;
                %title, axis scale and labels
                axis([-1, 1, -1, 1]);
                title([num2str(i) '-click'])
                format bank
                aux1=num2str(round(explained(1),1)); aux2=num2str(round(explained(2),1));
                xlabel(['PC1 (' aux1 '%)'])
                ylabel(['PC2 (' aux2 '%)'])
                set(gca,'YTickLabel',[],'XTickLabel',[])
                hold off;
            end
        
    end
end

