function codaPCA(dataCodas, minLen, maxLen)
% codaPCA(dataCodas, Nclicks)
%
% Plots PCA on interclick intervals for several coda types
%
% INPUT:
%'dataCodas': standard dataCodas format, with additional 46th column giving
%the coda types, either via kmeans or OPTICSxi, or something else. Strictly 
%necessary columns are: 6th-40th(ICIs), 46th(coda type). Others may contain:
%1st(coda#), 2nd(date or rec#), 3rd(repertoire), 4th(Nclicks), 5th(total length)
%
%'minLen': number. Minimum coda length, in number of clicks. Usually 3.
%
%'maxLen': number. Maximum coda length, in number of clicks. Usually 11.
%
% Mauricio Cantor, June 2015

figure
    for i=minLen:maxLen
        %raw data per coda length, without noise
        auxdata = dataCodas(dataCodas(:,4)==i, :);
        %ICIs
        auxici = auxdata(:, 6:(6+(i-2)));
        %PCA
        [coeff score latent tsquared explained] = pca(auxici);
        %Plotting
        subplot(5,2, (i-2))
        set(gcf,'color','w');
        gscatter(score(:,1), score(:,2), num2str(auxdata(:,46)))
        axis([min(score(:,1))*1.2, max(score(:,1))*1.2, ...
            min(score(:,2))*1.2, max(score(:,2))*1.2]);
        
            title([num2str(i) '-click'])
            format bank
            aux1=num2str(round(explained(1),1)); aux2=num2str(round(explained(2),1));
            xlabel(['PC1 (' aux1 '%)'])
            ylabel(['PC2 (' aux2 '%)'])
            set(gca,'YTickLabel',[],'XTickLabel',[])
        hold off;    
        
    end

end

