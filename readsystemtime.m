function dn = readsystemtime(fid)
  wYear = fread(fid, 1, 'uint16');
  wMonth = fread(fid, 1, 'uint16');
  wDayOfWeek = fread(fid, 1, 'uint16');
  wDay = fread(fid, 1, 'uint16');
  wHour = fread(fid, 1, 'uint16');
  wMinute = fread(fid, 1, 'uint16');
  wSecond = fread(fid, 1, 'uint16');
  wMilliseconds = fread(fid, 1, 'uint16');
  dn = datenum(wYear, wMonth, wDay, wHour, wMinute, wSecond);
  dn = dn + wMilliseconds/24/3600/1000; % may as well use the milliseconds too !