function codas = readcodasfromclk(filename)

% function readcodasfromclk(filename)
%   function to read codas from Rainbow Click file into matlab
%   takes as input [filename] for full path click file
%   returns [codas] struct with fieds:
%     .whale - whale number in file
%     .icis - vertical vector containing icis
%     .length - coda length in seconds ( sum of icis)
%     .nclicks - number of clicks
%     .time - time of first coda click since start of file
%     .clicknumbers - click number since start of fie
%     .manual_ipi - horizontal vector containing manualy measures ipis
%     .events - horizontal vector containing event numbers for each click
%     .filename - filename
%     .whalecoda - nth codas for .whale
%
%   adapted from Douglas Gillespie rclibexample.m 
%   Ricardo Antunes - February 2006


% verify if file exists
d = dir(filename);
if isempty(d)
    error('Could not find click file');
end


% first of all, open up the file. The file wil be analysed to see how many
% sections it contains..:P
rf = rainbowfile(filename,1);

% read in all the clicks, and read in the click waveforms
clicks = readdata(rf, 'section', 1, 2);
codas = clicks;
% and get the control structures for that section (to find out sample
% rate, etc.
structs = get(rf, 'structure', 1);

% initialize codas
codas =  []; cur_coda = 1;
if ~isempty(clicks) & isstruct(clicks)
    for iw = 1:max([clicks.whale]) % loop over whale id's
        whaleclicks = clicks(find([clicks.whale] == iw));
        % convert the click times to seconds (they are stored in ADC sample
        % units)
        times = [whaleclicks.starttime]/structs.soundcardsettings.nsamplespersec;

        %disp(['whale:',num2str(iw)]); % uncomment for debugging

        % find the starts and ends of the labelled codas
        codastarts = find([whaleclicks.codastart]);
        codaends = find([whaleclicks.codaend]);


        for icoda = 1:length(codastarts)
            if numel(codaends) >= numel(codastarts)
                % get the clicks for an individual coda
                codaclicks = whaleclicks(codastarts(icoda):codaends(icoda));
                if numel(codaends) > numel(codastarts)
                    disp (['Warning: too many codaends for existing codastarts - skiping coda !']);
                else
                    % codaclicks = whaleclicks(codastarts(icoda):codaends(icoda));
                    if ~isempty(codaclicks)
                        % the times of the coda clicks ...
                        codatimes = times(codastarts(icoda):codaends(icoda));
                        % calculate the delay times between each click
                        ici = [diff(codatimes)];
                        codas(cur_coda).whale = iw;
                        s = size(ici);
                        codas(cur_coda).sr = structs.soundcardsettings.nsamplespersec;
                        codas(cur_coda).waves = {codaclicks(:).wavedata};
                        codas(cur_coda).icis = ici';
                        codas(cur_coda).length = sum(ici);
                        codas(cur_coda).nclicks = length(ici)+1;
                        codas(cur_coda).time = codatimes(1);
                        codas(cur_coda).endtime = codatimes(end);
                        codas(cur_coda).clicknumbers = [codaclicks(:).click_no];
                        codas(cur_coda).manual_ipi = [codaclicks(:).ipi];
                        codas(cur_coda).events = [codaclicks(:).eventnumber];
                        codas(cur_coda).filename = filename;
                        codas(cur_coda).whalecoda = icoda;
                        codas(cur_coda).timedifference = [codaclicks(:).timedifference];
                        cur_coda = cur_coda + 1;
                    end
                end
            else
                disp ('Problem: not enough codaends for codastarts - skiping coda!')
            end
        end % whale codas loop

    end % whale id's loop
end

% total number of codas
n_codas = length(codas);
disp ( ['...found ',num2str(n_codas),' codas.']);

% close the file
close(rf)