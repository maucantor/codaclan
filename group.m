function grps = group(ids,codas,threshold);
%function grps = group(ids,codas,threshold);
%function converts days into groups based on ids using the 
%n(ab) > 0.25.min(na,nb) in Whitehead et al. 1998 (but see threshold below)
%ids is from photo id database and contains date;id
%codas is a matrix with coda number, date
%threshold is the recapture criteria between days. Originally in Whitehead
%etal 1998 it was used 25%; for larger groups, as in the Galapagos 2013-14,
%10% seem to work better.
%grps is a matrix of day#; grp#

grps = unique(codas(:,2)); %get number of days
ndays = size(grps,1);
grps(:,2) = zeros(ndays,1); %make space for group id

for n = 1:ndays % loop through each day
    disp(grps(n,1));
    if grps(n,2) == 0 %only consider if not already in a group
        thisgroup = max(grps(:,2))+1;
        grps(n,2) = thisgroup;
        if sum(ids(:,1)==grps(n,1)); %only carry on if there are ids for this day
            idone = unique(ids(ids(:,1)==grps(n,1),2)); %ids of testday
            na = size(idone,1); %get na
            rest = grps(grps(:,1)~=grps(n,1) & grps(:,2)==0,:); %get rest of unassigned days
            nrest = size(rest,1);
            for dd = 1:nrest %loop through these days
                idtwo = unique(ids(ids(:,1)==rest(dd,1),2)); %ids of newday
                nb = size(idtwo,1); %get nb
                if nb %if there are any ids on this day
                    mab = sum(sum(repmat(idone',nb,1)==repmat(idtwo,1,na)));%get mab
%                   if mab>0.25*min([na nb])%original criterion by Whitehead et al. 2008
                    if mab>threshold*min([na nb]) %10% criterion that seems to work better for the larger groups of Gal2013-14 
                        grps(grps(:,1)==rest(dd,1),2) = thisgroup; %assign to same group
                    end
                end
            end
        end
    end
end