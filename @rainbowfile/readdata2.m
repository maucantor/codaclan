function [cks, rcf] = readdata2(rcf, What, varargin)
% function cks = readdata(rcf, What, opts ...)
%  reads  clicks or noise measurments from a file previously opened with the rainbowfile function
%  rcf - rainbowfile object
%  What - What to read: 1 - clicks without waveform data
%                       2 - clicks with waveform data
%                       3 - noise data
%                       4 - embedded gps data
% options come in pairs - an option identifier and the limits of that
% option. If no options are set, the whole file is read in.
% Valid options are:
% 'section'      - array of section numbers whos data should be read
% 'times'        - time limits (datenums) for data reading - 2 element array
% 'clicknumbers' - min and max numbers of clicks to be read in
% 'whales'       - whale numbers to be read in, array of any length, eg
%                  [1:12] will read all clicks which are marked, [2 3 6]
%                  will read whalse 2, 3 and 6, etc.
%
% see also RAINBOWFILE, DELAY2BEARING, GET
if nargin < 2
  What = 1;
end
if ~isa(rcf, 'rainbowfile')
  disp('First argument is not a valid porpoisefile object');
  %return;
end
if rcf.fid == -1
  disp('RainbowClick file object does not contain a valid file handle');
  return;
end
% work out what is in varargin
Sections = [];
Times = [];
ClickNumbers = [];
Whales = [];
i = 1;
while i < length(varargin)
  arg = lower(varargin{i});
  i = i + 1;
  arg = arg(1:min(length(arg),4));
  if strcmp(arg, 'sect') 
    Sections = varargin{i};
    i = i + 1;
  elseif strcmp(arg, 'time') 
    Times = varargin{i};
    i = i + 1;
  elseif strcmp(arg, 'clic') 
    ClickNumbers = varargin{i};
    i = i + 1;
  elseif strcmp(arg, 'whal') 
    Whales = varargin{i};
    i = i + 1;
  end
end
%if ~isempty(Sections)
  cks = readsectionclicks(rcf, What, Sections, Times, ClickNumbers, Whales);
  %elseif ~isempty(Times)
%  cks = readtimeclicks(rcf, What, Times, Whales);
%end

function cks = readsectionclicks(rcf, What, Sections, Times, ClickNumbers, Whales)
  %cks = porpoiseclick();
  totalclicks = 0;
  timeoffset = 0;
  if isempty(Sections)
    Sections = 1:rcf.nsections;
  end
  samplesperday = rcf.section(1).structs.soundcardsettings.nsamplespersec * 3600 * 24;
  starttime = rcf.section(Sections(1)).structs.sectiontime.starttime;
  for i = Sections
    fseek(rcf.fid, rcf.section(i).datfilepos, 'bof'); %jump to start of that section
    timeoffset = rcf.section(i).structs.sectiontime.starttime - rcf.section(Sections(1)).structs.sectiontime.starttime; % difference in days
    timeoffset = timeoffset * 24 * 3600; % difference in seconds
    timeoffset = timeoffset * rcf.section(1).structs.soundcardsettings.nsamplespersec;
    % if there are time limits set, then move through the pointers to get
    % to roughly the right place
    mappoint = 1;
    maplen = length(rcf.section(i).map);
    if isempty(Times)
      SectTimes(1) = rcf.section(i).structs.sectiontime.starttime ;
      SectTimes(2) = SectTimes(1) + rcf.section(i).structs.sectiontime.duration / samplesperday;
    else
      % use the map to get to roughly the right place.
      SectTimes = Times;
      for j = 1:maplen
        if rcf.section(i).map(j).clickdate > SectTimes(1), break, end;
        fseek(rcf.fid, rcf.section(i).map(j).fpos, 'bof');
        mappoint = j;
      end
    end
    if ~isempty(ClickNumbers)
      for j = mappoint:maplen
        if rcf.section(i).map(j).click_no > min(ClickNumbers), break, end;
        fseek(rcf.fid, rcf.section(i).map(j).fpos, 'bof');
        mappoint = j;
      end
    end
    
    while 1
      if feof(rcf.fid), break, end;
      fp = ftell(rcf.fid);
      ch = readclickheader(rcf);
      if isempty(ch), break, end;
      if ch.datatype == 1, break; end; % section header information so get out
      ch.start_time = ch.start_time + timeoffset; % units are samples
      ch.clickdate = ch.start_time/samplesperday+starttime;
      bit = getwhat(rcf, i, ch, What);
      if ~isempty(bit)
        if ch.clickdate < SectTimes(1), continue; end
        if ch.clickdate > SectTimes(2), break; end
        if ~isempty(ClickNumbers)
          if ch.click_no < min(ClickNumbers), continue, end;
          if ch.click_no > max(ClickNumbers), continue, end;
        end
        if ~isempty(Whales)
          if sum(Whales == ch.whale) == 0, continue, end;  
        end
        totalclicks = totalclicks + 1;
        cks(totalclicks) = bit;
      end
    end  
  end
  if totalclicks == 0
    cks = [];
  end
  
function bit = getwhat(rcf, i, ch, What)
  % start of a section common to all units !
  bit = [];
  fid = rcf.fid;
  if ch.datatype == 0  % It should be a normal porpoise click
    if What <= 2
      fpos = ftell(rcf.fid);     
      bit.date = ch.clickdate;
      bit.starttime = ch.start_time;
      bit.timedifference = ch.timediff(1);
      bit.duration = ch.duration;
      bit.click_no = ch.click_no;
      bit.amplitude = ch.amplitude;
      bit.whale = ch.whale;
      bit.echo = ch.echo;
      bit.ipi = ch.timediff(2);
      bit.codaclicknumber = bitand(hex2dec('3FFF'), ch.coda);
      bit.codastart = bitand(hex2dec('8000'), ch.coda) > 0;
      bit.codaend = bitand(hex2dec('4000'), ch.coda) > 0;
      bit.tracker = ch.tracker;
      bit.eventnumber = ch.eventnumber;
    end
    if What == 2
      bit.wavedata = fread(fid, [2, ch.duration], 'int16')';
    else
      fseek(rcf.fid, ch.duration * 4, 'cof');
    end
  elseif ch.datatype == 1 % section data - should never get here !
    return;
  elseif ch.datatype == 2 % GPS data
    if What == 4
      bit = readgpsinfo(fid);
    else
      readgpsinfo(fid);
    end
  elseif ch.datatype == 3  %Noise measurement
    if What == 3  % we want this so read it in
      bit.date = ch.clickdate;
      bit.starttime = ch.start_time;
      bit.noise = ch.NoiseLevel;
    end
  elseif ch.datatype == 4 %seismic event
  end% of section common to all read types
  