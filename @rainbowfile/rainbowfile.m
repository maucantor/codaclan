function rcf = rainbowfile(arg, verbose)
% function rcf = rainbowfile(arg, verbose)
%  opens a RainbowClick data file (.clk) and maps the contents of the
%  file to a 'rainbowfile' object. This can then be used in calls to
%  readdata to access click and noise data.
%  contents of the rcf object may be accessed using the  get() function
% if the second argument 'verbose' is true, it will print info to screen
if nargin < 2
    verbose = 0;
end
if nargin == 0
    rcf.filename = ' ';
    rcf.nsections = 0;
    rcf.fid = -1;
    rcf.header = struct([]);
    rcf.section = struct([]);
elseif isa(arg, 'rainbowfile')
    p = arg;
else
    % open the file (assuming it exists) and work out whats in it.
    rcf.filename = ' ';
    rcf.nsections = 0;
    rcf.fid = -1;
    rcf.header = struct([]);
    rcf.section = struct([]);
    rcf = class(rcf, 'rainbowfile');

    rcf.fid = fopen(arg, 'r');
    if rcf.fid == -1
        disp(sprintf('ERROR - file %s cannot be opened', arg));
        rcf = [];
        return;
    end
    rcf.filename = arg;
    if verbose > 1
        % prepare a progress bar since this next step can take some time
        f = figure;
        P = get(f, 'Position');
        set(f, 'Position', [P(1:2), 300, 50]);
        set(f, 'Menubar', 'none')
        set(f, 'Name', ['Mapping ', arg])
        set(f, 'Numbertitle', 'off')
        bar(1, 0.001);
        set(gca, 'XLim', [0 100]);
        axis off
        set(gca, 'XLim', [0 100]);
        hold on
        refresh(f)
    end
    secondsperday = 24 * 3600;
    totalclicks = 0;
    sectionclicks = 0;
    clicknumber = 0;
    rcf.nsections = 0;
    fseek(rcf.fid, 0, 'eof');
    totalbytes = ftell(rcf.fid);
    fseek(rcf.fid, 0, 'bof');
    rcf.header = readfileheader(rcf);
    if (rcf.header.format < 3)
        disp(sprintf('Old file format version %d - update to version 4 or above', rcf.header.format))
        disp('using a more rectent Rainbowclick downloaded from the web site')
        close(f)
        return;
    end
    mapthings = 0;
    while 1
        if feof(rcf.fid), break, end;
        fp = ftell(rcf.fid);
        ch = readclickheader(rcf);
        if isempty(ch), break, end;
        if ch.datatype == 0      % click information
            clicknumber = clicknumber + 1;
            ch.click_no = clicknumber;
            if mod(sectionclicks, 500)==0
                % map the position of every 500th click to speed up readclicks
                mapthings = mapthings + 1;
                rcf.section(rcf.nsections).map(mapthings).fpos = fp;
                clickdate = rcf.section(rcf.nsections).structs.sectiontime.starttime;% + ...
                clickdate = clickdate + ch.start_time / secondsperday / ...
                    rcf.section(rcf.nsections).structs.soundcardsettings.nsamplespersec;
                rcf.section(rcf.nsections).map(mapthings).clickdate = clickdate;
                rcf.section(rcf.nsections).map(mapthings).click_no = ch.click_no;
            end
            totalclicks = totalclicks+1;
            sectionclicks = sectionclicks + 1;
            if verbose > 1
                if mod(totalclicks, 1000) == 0
                    figure(f);
                    barh(1, fp*100/totalbytes);
                    pause(0.001)
                end
            end
            fseek(rcf.fid, ch.duration * 4, 'cof');
        elseif ch.datatype == 1  % section header information
            str = readstructures(rcf, ch);
            if isempty(str), break, end;
            if (rcf.nsections)
                rcf.section(rcf.nsections).nclicks = sectionclicks;
                sectionclicks = 0;
            end
            rcf.nsections = rcf.nsections + 1;
            rcf.section(rcf.nsections).structs = str;
            rcf.section(rcf.nsections).strfilepos = fp;
            mapthings = 0;
            fp = ftell(rcf.fid);
            rcf.section(rcf.nsections).datfilepos = fp;
            if verbose > 0
                disp(sprintf('section %d in file %s; Start: %s; Duration: %ds', rcf.nsections, arg, ...
                    datestr(str.sectiontime.starttime), ...
                    fix(str.sectiontime.duration/str.soundcardsettings.nsamplespersec)));
            end
        elseif ch.datatype == 2  % GPS data
            readgpsinfo(rcf.fid);
        elseif ch.datatype == 3  % Noise measurement
            % no action required
        elseif ch.datatype == 4  % Seismic measurement
            % no action required
        else
            disp(sprintf('Unknown click header type - %d', ch.datatype));
            break
        end
        if (rcf.nsections)
            rcf.section(rcf.nsections).nclicks = sectionclicks;
        end
    end

    if verbose > 0
        

        %fclose(rcf.fid);

        disp(sprintf('RainbowClick File contains %d kB of data with %d sections, %d clicks', ...
            fix(totalbytes/1024), rcf.nsections, totalclicks));
        if verbose >1
            close(f)
        end
    end
    % rcf = class(rcf, 'rainbowfile');
end
%% sub functions
function fh = readfileheader(rcf)
fid = rcf.fid;
[fh.nClicks, Count] = fread(fid, 1, 'int32');
[fh.nWhales, Count] = fread(fid, 1, 'int32');
[fh.flukeTime, Count] = fread(fid, 1, 'int32');
[fh.recordTime, Count] = fread(fid, 1, 'int32');
[fh.format, Count] = fread(fid, 1, 'int8');
[fh.spare, Count] = fread(fid, 1, 'int8');

