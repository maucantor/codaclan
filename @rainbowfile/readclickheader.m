function ch = readclickheader(rcf)
% function ch = readclickheader(cf)
%  reads a click header structure from a RainbowClick file
%  ch.type is the type of header:
%       = 128 (0x80) header marks the start of a sections sturctures (see readstructures)
%       = 64 (0x40) header is a noise measurement
%       otherwise   header is a porpoise click header
%  ch.datalength is the total length of data in bytes following this header structure
%  ch.samples is the number of samples in the click
%  ch.start_time is the start time of the click or noise measurement in ADC samples from the start of this section
%  ch.flags (for section headers only) bitmap of structures represented in following data blocks
%  ch.triggs is a bitmap of channels which triggered
%  ch.waveforms is a bitmap of channels whose waveforms have been written
%  
  ch = [];
  fid = rcf.fid;
  if feof(fid)
    ch = [];
    return;
  end
  %if isempty(ch.Type), return, end;
  [ch.click_no, Count] = fread(fid, 1, 'uint16');
  if (Count == 0)
    ch = [];
    return, 
  end;
  [ch.start_time, Count] = fread(fid, 1, 'uint32');
  [ch.duration, Count] = fread(fid, 1, 'uint16');
  [ch.datatype, Count] = fread(fid, 1, 'uint8');
  [ch.triggs, Count] = fread(fid, 1, 'uint8');
  fseek(fid, 2, 'cof');
  [ch.amplitude, Count] = fread(fid, 2, 'uint16');
  % the next 4 variables are in a union, so rewind between each read
  [ch.bearing, Count] = fread(fid, 1, 'float');
  fseek(fid, -4, 'cof');
  [ch.timediff, Count] = fread(fid, 2, 'int16');
  fseek(fid, -4, 'cof');
  [ch.flags, Count] = fread(fid, 1, 'uint32');
  fseek(fid, -4, 'cof');
  [ch.noiselevel, Count] = fread(fid, 1, 'float');
  [ch.whale, Count] = fread(fid, 1, 'int16');
  [ch.echo, Count] = fread(fid, 1, 'int16');
  [ch.coda, Count] = fread(fid, 1, 'uint16');
  [ch.tracker, Count] = fread(fid, 1, 'uint16');
  [ch.eventnumber, Count] = fread(fid, 1, 'int16');
  [spare1, Count] = fread(fid, 1, 'int16');
  [spare2, Count] = fread(fid, 1, 'int32');
  if (Count == 0)
    ch = [];
    return, 
  end;