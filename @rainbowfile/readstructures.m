function str = readstructures(rcf, ch)
% function str = readstructures(rcf, ch)
%  reads stuctures from the porpoise file of a RainbowClick object
%  each click file is divided into a number of section
%  a set of structures exist at the start of every file section containing
%  information on the analysis parameters and contents of that section
%  Many of the structures are required only during data collection and are not 
%  relevant to analysis using Matlab
%  The function returns a structure contiaining a number of other structures
  fid = rcf.fid;
  str = [];
%   flags = dec2hex(ch.flags)
  % stick to the form of seeing which nes are actually there
  % but probably don't programme them all up at the moment !
  if bitand(ch.flags, 1), str.soundcardsettings = readsoundcardsettings(fid); end;
  if bitand(ch.flags, 2), str.analparms = readanalparms(fid);  end;
  if bitand(ch.flags, 4), str.filtparms = readfiltparms(fid);  end;
  if bitand(ch.flags, hex2dec('8')), str.filtparms = readfiltparms(fid);  end;
  if bitand(ch.flags, hex2dec('10')), str.datheader = readdatheader(fid);  end;
  if bitand(ch.flags, hex2dec('20')), str.analtype = readanaltype(fid);  end;
  if bitand(ch.flags, hex2dec('40')), str.sectiontime = readsectiontime(fid);  end;
  if bitand(ch.flags, hex2dec('80')), str.runpardata = readrunpardata(fid);  end;
  if bitand(ch.flags, hex2dec('100')), fseek(fid, 124, 'cof');  end;
  if bitand(ch.flags, hex2dec('200')), str.options = readoptions(fid);  end;
  if bitand(ch.flags, hex2dec('400')), fseek(fid, 127, 'cof');  end;
  if bitand(ch.flags, hex2dec('800')), fseek(fid, 45, 'cof');  end;
  if bitand(ch.flags, hex2dec('1000')), fseek(fid, 283, 'cof');  end;
  if bitand(ch.flags, hex2dec('2000')), fseek(fid, 288, 'cof');  end;
  if bitand(ch.flags, hex2dec('4000')), str.wavfileinfo = readwavfileinfo(fid); end; 
  if bitand(ch.flags, hex2dec('8000')), str.digitalfilter = readdigitalfilter(fid);  end;
  if bitand(ch.flags, hex2dec('10000')), fseek(fid, 31, 'cof');  end;
  if bitand(ch.flags, hex2dec('20000')), str.autoadjust = readautoadjustinfo(fid);  end;
  if bitand(ch.flags, hex2dec('40000')), fseek(fid, 119, 'cof');  end;
  if bitand(ch.flags, hex2dec('80000')), str.narrowreject = readnarrowreject(fid);  end;
  if bitand(ch.flags, hex2dec('200000')), str.predigitalfilter = readdigitalfilter(fid); end; 
%   knownflags = (hex2dec('28c2ff'))
%   unknownflags = dec2hex(bitand(knownflags, ch.flags))
  
%   else
%     disp (sprintf ('Unknown structure header id %s', dec2hex(ch.flags)));
%   end
  
function ss = readsoundcardsettings(fid)
  ss.wformattag = fread(fid, 1, 'int16');
  ss.nchannels = fread(fid, 1, 'int16');
  ss.nsamplespersec = fread(fid, 1, 'int32');
  ss.navgbytespersec = fread(fid, 1, 'int32');
  ss.nblockalign = fread(fid, 1, 'int16');
  ss.wbitspersample = fread(fid, 1, 'int16');
  %ss.cbsize = fread(fid, 1, 'int16');
  
function ap = readanalparms(fid)
  ap.alphalong = fread(fid, 1, 'float');
  ap.alphashort = fread(fid, 1, 'float');
  ap.snapint = fread(fid, 1, 'int16');
  ap.snapdur = fread(fid, 1, 'float');
  ap.snapstart = fread(fid, 1, 'uint32');
  ap.snapend = fread(fid, 1, 'uint32');
  ap.threshoff = fread(fid, 1, 'int16');
  ap.threshon = fread(fid, 1, 'int16');
  ap.presample = fread(fid, 1, 'int16');
  ap.postsample = fread(fid, 1, 'int16');
  ap.datalength = fread(fid, 1, 'int16');
  ap.snapgain = fread(fid, 1, 'int16');
  ap.t2active = fread(fid, 1, 'int32');
  ap.minenergy = fread(fid, 1, 'int16');
  ap.minsep = fread(fid, 1, 'int16');
  ap.fvremove = fread(fid, 1, 'int32');
  ap.fvrecalc = fread(fid, 1, 'int32');
  ap.fveto = fread(fid, 1, 'int32');
  ap.bigveto = fread(fid, 1, 'int8');
  ap.bigthreshold = fread(fid, 1, 'uint16');
  ap.bigseconds = fread(fid, 1, 'float');
  ap.measurenoise = fread(fid, 1, 'int8');
  ap.noiseinterval = fread(fid, 1, 'float');
  fseek(fid, 12, 'cof');

function fp = readfiltparms(fid)
  fp.hpfilter = fread(fid, 1, 'int16');
  fp.nfilter_1 = fread(fid, 1, 'int16');
  fp.nfilter_2 = fread(fid, 1, 'int16');
  fp.lpfilter = fread(fid, 1, 'int16');
  fp.hseptime = fread(fid, 1, 'float');
  fp.fftlen = fread(fid, 1, 'int16');
  fp.logfftlen = fread(fid, 1, 'int16');
  fp.windowfunc = fread(fid, 1, 'int16');
  fseek(fid, 20, 'cof');

function dh = readdatheader(fid)
  [dh.nClicks, Count] = fread(fid, 1, 'int32');
  [dh.nWhales, Count] = fread(fid, 1, 'int32');
  [dh.flukeTime, Count] = fread(fid, 1, 'int32');
  [dh.recordTime, Count] = fread(fid, 1, 'int32');
  [dh.format, Count] = fread(fid, 1, 'int8');
  [spare, Count] = fread(fid, 1, 'int8');
  
function at = readanaltype(fid)
  [at.inputdevice, Count] = fread(fid, 1, 'int32');
  [at.triggermethod, Count] = fread(fid, 1, 'int32');
  [at.playback, Count] = fread(fid, 1, 'int32');
  [at.identification, Count] = fread(fid, 1, 'int32');
  [at.bearingcalc, Count] = fread(fid, 1, 'int32');
  [at.bufferlength, Count] = fread(fid, 1, 'int32');
  [at.boardnum, Count] = fread(fid, 1, 'int32');
  [at.bits, Count] = fread(fid, 1, 'int16');
  [at.wavstarttime, Count] = fread(fid, 1, 'float');
  [at.cbgain, Count] = fread(fid, 1, 'int16');
  [at.loopwavfile, Count] = fread(fid, 1, 'int8');
  fseek(fid, 15, 'cof');
  
  
function st = readsectiontime(fid)
  st.starttime = readsystemtime(fid);
  st.duration = fread(fid, 1, 'uint32');
  st.whystopped = fread(fid, 1, 'int16');
  fseek(fid, 18, 'cof');
  
function rp = readrunpardata(fid)
  rp.decay = fread(fid, 1, 'float');
  rp.beardiff = fread(fid, 1, 'float');
  rp.specbear = fread(fid, 1, 'float');
  rp.tophat = fread(fid, 1, 'int32');
  rp.constant = fread(fid, 1, 'int32');
  rp.okgood = fread(fid, 1, 'float');
  rp.timewin = fread(fid, 1, 'int16');
  rp.speccorr = fread(fid, 1, 'float');
  rp.amplitude = fread(fid, 1, 'int16');
  fseek(fid, 24, 'cof');
  
  
function op = readoptions(fid)
  op.playbackdevice = fread(fid, 1, 'int16');
  op.playbackboard = fread(fid, 1, 'int16');
  op.playbackrate = fread(fid, 1, 'int32');
  op.playbackfiltered = fread(fid, 1, 'int8');
  fseek(fid, 15, 'cof');
  

function wi = readwavfileinfo(fid)
  wi.filename = fread(fid, 260, 'char');
  fseek(fid, 128, 'cof');
  
function df = readdigitalfilter(fid)
  df.filtertype = fread(fid, 1, 'uint16');
  df.filterbands = fread(fid, 1, 'uint16');
  df.highpassfreq = fread(fid, 1, 'uint32');
  df.lowpassfreq = fread(fid, 1, 'int32');
  df.pbripple = fread(fid, 1, 'float');
  df.order = fread(fid, 1, 'int32');
  fseek(fid, 24, 'cof');
  
function aa = readautoadjustinfo(fid)
  aa.AutoAdjust = fread(fid, 1, 'uint16');
  aa.ThresholdSigmas = fread(fid, 1, 'float');
  aa.ThresholdRatio = fread(fid, 1, 'float');
  aa.BigVetoSigmas = fread(fid, 1, 'float');
  fseek(fid, 40, 'cof');

function nr = readnarrowreject(fid)
  nr.on = fread(fid, 1, 'int8');
  nr.limitfrequencies = fread(fid, 1, 'int8');
  nr.startfreq = fread(fid, 1, 'int32');
  nr.endfreq = fread(fid, 1, 'int32');
  nr.threshold = fread(fid, 1, 'float');
  fseek(fid, 30, 'cof');
  