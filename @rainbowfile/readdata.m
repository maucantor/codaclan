function [cks, rcf] = readdata(rcf, Ctrl, Limits, What, What2)
% function cks = readdata(rcf, Ctrl, Limits, Waves)
%  reads  clicks or noise measurments from a file previously opened with the rainbowfile function
%  rcf - rainbowfile object
%  Ctrl - type of read - either 'section', 'Time' , 'ClickList' or 'Database'
%  Limits - if Ctrl is 'section' Limits is either a scaler or a vector of sections to read data from
%           if Ctrl is 'Time' then Limits is a 2  element array giving the start and end times of the
%             read either as datestr or datenum datatypes.
%           if Ctrl is 'clicklist', limits are a section number AND a list
%             of click numbers (5 parameters in all)
%  What - What to read: 1 - clicks without waveform data
%                       2 - clicks with waveform data
%                       3 - noise data
%                       4 - embedded gps data
% 
% modified by Ricardo Antunes (Feb 2006) to allow reading of longest
% section
if nargin == 3
  What = 1;
end
if nargin < 2
  disp('Function readdata requires a least three arguments');
  return;
end
if ~isa(rcf, 'rainbowfile')
  disp('First argument is not a valid porpoisefile object');
  %return;
end
if rcf.fid == -1
  disp('RainbowClick file object does not contain a valid file handle');
  return;
end
switch lower(Ctrl)
  case 'section'
    cks = readsectionclicks(rcf, Limits, What);
  case 'time'
    cks = readtimeclicks(rcf, Limits, What);
  case 'clicklist'
    cks = readlistedclicks(rcf, Limits, What, What2); 
end

%--------------------------------------------------------------------------
function cks = readsectionclicks(rcf, Limits, What)
%cks = porpoiseclick();
totalclicks = 0;
timeoffset = 0;
samplesperday = rcf.section(1).structs.soundcardsettings.nsamplespersec * 3600 * 24;
starttime = rcf.section(Limits(1)).structs.sectiontime.starttime;
% 
if ischar(Limits)
    switch lower(Limits)
        case 'max'
            
    end
end
for i = Limits
  fseek(rcf.fid, rcf.section(i).datfilepos, 'bof'); %jump to start of that section
  timeoffset = rcf.section(i).structs.sectiontime.starttime - rcf.section(Limits(1)).structs.sectiontime.starttime; % difference in days
  timeoffset = timeoffset * 24 * 3600; % difference in seconds
  timeoffset = timeoffset * rcf.section(1).structs.soundcardsettings.nsamplespersec;
  while 1
    if feof(rcf.fid), 
      break;
    end;
    fp = ftell(rcf.fid);
    ch = readclickheader(rcf);
    if isempty(ch), 
      break, 
    end;
    if ch.datatype == 1, 
      break; 
    end; % section header information so get out
    ch.start_time = ch.start_time + timeoffset; % units are samples
    ch.clickdate = ch.start_time/samplesperday+starttime;
    bit = getwhat(rcf, i, ch, What);
    if ~isempty(bit) 
      totalclicks = totalclicks + 1;
      cks(totalclicks) = bit;
    end
  end  
end
if totalclicks == 0
  cks = [];
end
%--------------------------------------------------------------------------

function cks = readtimeclicks(rcf, Limits, What)
totalclicks = 0;
secsperday = 3600 * 24;
% Limits may be either two datenum (double) or two datestr types. If datestr, convert to datenum, otherwise, escape
% assume for now that they are two datenum !
if length(Limits) ~= 2
  disp('The second argument must be a pair of datenum arguments');
  cks = [];
  return;
end
% need to work out which section the first timelimit falls in
% rcf.section(rcf.nsections).structs.sectiontime
endtime = rcf.section(rcf.nsections).structs.sectiontime.starttime + ...
  rcf.section(rcf.nsections).structs.sectiontime.duration / rcf.section(rcf.nsections).structs.soundcardsettings.nsamplespersec / secsperday;
if Limits(1) > endtime
  disp(sprintf('ERROR - start time for data read is beyond the end of data file %s', rcf.filename));
  cks = [];
  return;
end
if Limits(2) < rcf.section(1).structs.sectiontime.starttime
  disp(sprintf('ERROR - end time %s for data read is before the start %s of data file %s', ...
    datestr(Limits(2)), datestr(rcf.section(rcf.nsections).structs.sectiontime.starttime), rcf.filename));
  cks = [];
  return;
end
startsect = 0;
for isect = rcf.nsections:-1:1
  %     endtime = rcf.section(rcf.nsections).structs.sectiontime.starttime + ...
  %       rcf.section(rcf.nsections).structs.sectiontime.duration / rcf.section(rcf.nsections).structs.soundcardsettings.nsamplespersec / secsperday;
  if Limits(1) >= rcf.section(isect).structs.sectiontime.starttime
    Limits(1) - rcf.section(isect).structs.sectiontime.starttime
    startsect = isect;
    break,
  end
end
if Limits(1) < rcf.section(1).structs.sectiontime.starttime
  disp(sprintf('WARNING - start time for data read is before the start of data file %s', rcf.filename));
  startsect = 1;
end
if Limits(2) > endtime
  disp(sprintf('WARNING - end time for data read is beyond the end of data file %s', rcf.filename));
end
samplesperday = rcf.section(1).structs.soundcardsettings.nsamplespersec * 3600 * 24;
starttime = rcf.section(startsect).structs.sectiontime.starttime;
for i = startsect:rcf.nsections
  %jump to start of that section
  fseek(rcf.fid, rcf.section(i).datfilepos, 'bof'); 
  if i == startsect 
    % use the map made in porpoisefile to get in roughly the right place
    maplen = length(rcf.section(i).map);
    for j = 1:maplen
      if rcf.section(i).map(j).clickdate > Limits(1), break, end;
      fseek(rcf.fid, rcf.section(i).map(j).fpos, 'bof');
    end
  end
  timeoffset = rcf.section(i).structs.sectiontime.starttime - starttime; % difference in days
  timeoffset = timeoffset * 24 * 3600; % difference in seconds
  timeoffset = timeoffset * rcf.section(1).structs.soundcardsettings.nsamplespersec;
  while(1)
    if feof(rcf.fid), break, end;
    fp = ftell(rcf.fid);
    ch = readclickheader(rcf);
    if isempty(ch), break, end;
    if ch.datatype == 1, break; end; % section header information so get out
    ch.start_time = ch.start_time + timeoffset; % units are samples
    ch.clickdate = ch.start_time/samplesperday+starttime;
    if ch.clickdate < Limits(1)
      fseek(rcf.fid, ch.duration * 4, 'cof');
      continue;
    elseif ch.clickdate > Limits(2)
      disp(sprintf('%s    %s', datestr(ch.clickdate), datestr(Limits(2))));
      return;
    end;      
    bit = getwhat(rcf, i, ch, What);
    if ~isempty(bit)
      totalclicks = totalclicks + 1;
      cks(totalclicks) = bit;
    end
    
  end
end  

if totalclicks == 0
  cks = [];
end

function  cks = readlistedclicks(rcf, Section, List, What)
totalclicks = 0;
timeoffset = 0;
samplesperday = rcf.section(Section).structs.soundcardsettings.nsamplespersec * 3600 * 24;
starttime = rcf.section(Section).structs.sectiontime.starttime;
fseek(rcf.fid, rcf.section(Section).datfilepos, 'bof'); %jump to start of that section
% all the clicks will be in order, in this file, in this section
imapthing = 1;
maplen = length(rcf.section(Section).map);
maxcount = 65536;
countoffset = 0;
for i = 1:length(List)
  % seeif th emap can tell us to jump ahead
  for j = imapthing:maplen
    if rcf.section(Section).map(j).click_no > List(i)
      break;
    end;
    fseek(rcf.fid, rcf.section(Section).map(j).fpos, 'bof');
    imapthing = j+1;
%     if j > 1
      countoffset = fix(rcf.section(Section).map(j).click_no/maxcount) * maxcount;
%     end
    % need to work out what the offset is in the click number
  end
  % move forward and if the click is the next in the list, read it out
  % and loop onto the next click
  while 1
    if feof(rcf.fid), break, end;
    fp = ftell(rcf.fid);
    ch = readclickheader(rcf);
    if isempty(ch), break, end;
    if ch.datatype == 1, break; end; % section header information so get out
    if (ch.datatype == 0 & ch.click_no == 0)
      countoffset = countoffset + maxcount;
    end
    ch.click_no = ch.click_no + countoffset;
    if ch.click_no == List(i)
      ch.start_time = ch.start_time + timeoffset; % units are samples
      ch.clickdate = ch.start_time/samplesperday+starttime;
      bit = getwhat(rcf, i, ch, What);
      if ~isempty(bit) 
        totalclicks = totalclicks + 1;
        cks(totalclicks) = bit;
      end
      break;
    else
      % skip rest of click data
      fseek(rcf.fid, ch.duration * 4, 'cof');      
    end
  end  
end


if totalclicks == 0
  cks = [];
end

function bit = getwhat(rcf, i, ch, What)
% start of a section common to all units !
bit = [];
fid = rcf.fid;
if ch.datatype == 0  % It should be a normal porpoise click
  if What <= 2
    fpos = ftell(rcf.fid);     
    bit.date = ch.clickdate;
    bit.starttime = ch.start_time;
    bit.timedifference = ch.timediff(1);
    bit.duration = ch.duration;
    bit.click_no = ch.click_no;
    bit.amplitude = ch.amplitude;
    bit.whale = ch.whale;
    bit.echo = ch.echo;
    bit.ipi = ch.timediff(2);
    bit.codaclicknumber = bitand(hex2dec('3FFF'), ch.coda);
    bit.codastart = bitand(hex2dec('8000'), ch.coda) > 0;
    bit.codaend = bitand(hex2dec('4000'), ch.coda) > 0;
    bit.tracker = ch.tracker;
    bit.eventnumber = ch.eventnumber;
  end
  if What == 2
    bit.wavedata = fread(fid, [2, ch.duration], 'int16')';
  else
    fseek(rcf.fid, ch.duration * 4, 'cof');
  end
elseif ch.datatype == 1 % section data - should never get here !
  return;
elseif ch.datatype == 2 % GPS data
  if What == 4
    bit = readgpsinfo(fid);
  else
    readgpsinfo(fid);
  end
elseif ch.datatype == 3  %Noise measurement
  if What == 3  % we want this so read it in
    bit.date = ch.clickdate;
    bit.starttime = ch.start_time;
    bit.noise = ch.NoiseLevel;
  end
elseif ch.datatype == 4 %seismic event
end% of section common to all read types
