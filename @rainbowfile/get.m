function ans = get(rcf, arg1, arg2)
% function ans = get(rcf, arg1, arg2)  
%  accesses the contents of a rainbowfile object
%  rcf   - rainbowfile object created with rainbowfile(filename)
%  arg1 - data to retreive. 
%         'structure' - retreives the structure for section arg2
%         'fid'       - file id (from fopen)
%         'nsections' - the number of sectins 
%         'fileheader' - contents o file header structure
%         'filename'      - name of hpd file
  if nargin == 1
    % tell everything
    disp (sprintf('File     : %s', rcf.filename));
    disp (sprintf('Sections : %d', rcf.nsections));
    for i = 1:rcf.nsections
      disp(sprintf('section %d: Start: %s; Duration: %d s; clicks: %d', i, datestr(rcf.section(i).structs.sectiontime.starttime), ...
          fix(rcf.section(i).structs.sectiontime.duration/rcf.section(i).structs.soundcardsettings.nsamplespersec), ...
          rcf.section(i).nclicks));  
    end
    return;
  end
  
  if nargin < 3
    arg2 = 1;
  end
  switch lower(arg1)
    case 'structure'
      ans = rcf.section(arg2).structs;
    case 'fid'
      ans = rcf.fid;
    case 'nsections'
      ans = rcf.nsections;
    case 'fileheader'
      ans = rcf.header;
    case 'filename'
      ans = rcf.filename;
    otherwise
      ans = [];
  end