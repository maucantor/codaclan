# Cluster: Cluster_883_939
# Cluster name: Cluster_883_939
# Cluster noise flag: false
# Cluster size: 57
# Parents: Cluster_0_1067
ID=222725 0.089 0.115 0.132 0.156 0.212 0.358 codaN=9893 recN=787 group=71 nClicks=7 kMeans=75 predecessor=221903 reachability=0.11414902540100812
ID=222790 0.091 0.059 0.173 0.16 0.248 0.411 codaN=11918 recN=493 group=40 nClicks=7 kMeans=75 predecessor=222725 reachability=0.1110630451590447
ID=222789 0.092 0.053 0.172 0.17 0.249 0.425 codaN=11915 recN=493 group=40 nClicks=7 kMeans=75 predecessor=222725 reachability=0.1110630451590447
ID=222788 0.064 0.074 0.145 0.158 0.247 0.399 codaN=11887 recN=493 group=40 nClicks=7 kMeans=75 predecessor=222725 reachability=0.1110630451590447
ID=222781 0.101 0.051 0.167 0.141 0.267 0.436 codaN=11790 recN=488 group=38 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222780 0.088 0.05 0.134 0.19 0.2 0.399 codaN=11777 recN=488 group=38 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222779 0.086 0.063 0.15 0.17 0.231 0.402 codaN=11762 recN=488 group=38 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222778 0.075 0.096 0.14 0.157 0.191 0.345 codaN=11746 recN=487 group=38 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222777 0.096 0.048 0.164 0.157 0.25 0.42 codaN=11745 recN=487 group=38 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222776 0.098 0.063 0.159 0.153 0.28 0.443 codaN=11744 recN=487 group=38 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222743 0.1 0.053 0.163 0.174 0.235 0.42 codaN=10409 recN=798 group=71 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222741 0.108 0.05 0.18 0.161 0.256 0.424 codaN=10364 recN=798 group=71 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222738 0.121 0.121 0.14 0.178 0.246 0.419 codaN=10245 recN=798 group=71 nClicks=7 kMeans=75 predecessor=222788 reachability=0.11078808600206073
ID=222737 0.098 0.05 0.17 0.173 0.248 0.424 codaN=10233 recN=798 group=71 nClicks=7 kMeans=75 predecessor=222738 reachability=0.09303225247192501
ID=222736 0.102 0.054 0.182 0.174 0.264 0.452 codaN=10203 recN=798 group=71 nClicks=7 kMeans=75 predecessor=222738 reachability=0.09303225247192501
ID=222733 0.071 0.099 0.124 0.178 0.291 0.447 codaN=10093 recN=794 group=71 nClicks=7 kMeans=75 predecessor=222738 reachability=0.09303225247192501
ID=222732 0.099 0.098 0.145 0.191 0.248 0.4 codaN=10005 recN=790 group=71 nClicks=7 kMeans=75 predecessor=222738 reachability=0.09303225247192501
ID=222731 0.078 0.076 0.162 0.175 0.265 0.429 codaN=10003 recN=790 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222730 0.101 0.06 0.159 0.185 0.24 0.414 codaN=10001 recN=790 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222729 0.104 0.058 0.159 0.175 0.259 0.433 codaN=9997 recN=790 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222728 0.103 0.053 0.146 0.178 0.261 0.415 codaN=9994 recN=790 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222727 0.084 0.05 0.175 0.166 0.25 0.415 codaN=9936 recN=789 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222726 0.09 0.094 0.142 0.168 0.258 0.379 codaN=9913 recN=788 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222724 0.066 0.066 0.159 0.168 0.235 0.416 codaN=9873 recN=787 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222723 0.069 0.064 0.163 0.174 0.245 0.433 codaN=9870 recN=787 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222722 0.071 0.053 0.182 0.155 0.268 0.409 codaN=9840 recN=787 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222721 0.093 0.046 0.175 0.149 0.238 0.422 codaN=9821 recN=786 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222720 0.097 0.1 0.117 0.149 0.197 0.405 codaN=9817 recN=786 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222718 0.087 0.054 0.156 0.182 0.227 0.408 codaN=9749 recN=785 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222716 0.094 0.046 0.143 0.17 0.268 0.422 codaN=9715 recN=784 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222715 0.097 0.102 0.126 0.157 0.195 0.38 codaN=9713 recN=784 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222714 0.089 0.062 0.167 0.163 0.257 0.411 codaN=9711 recN=784 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222713 0.081 0.046 0.163 0.154 0.24 0.402 codaN=9703 recN=784 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222712 0.096 0.054 0.157 0.159 0.254 0.422 codaN=9700 recN=784 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222711 0.111 0.096 0.117 0.15 0.2 0.382 codaN=9699 recN=784 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222710 0.091 0.055 0.165 0.168 0.219 0.396 codaN=9681 recN=783 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222709 0.071 0.053 0.16 0.173 0.255 0.403 codaN=9645 recN=781 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222708 0.078 0.067 0.153 0.163 0.243 0.398 codaN=9642 recN=781 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222707 0.072 0.055 0.164 0.184 0.23 0.426 codaN=9640 recN=781 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222706 0.077 0.063 0.175 0.167 0.246 0.412 codaN=9626 recN=781 group=71 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222705 0.065 0.082 0.188 0.174 0.272 0.407 codaN=9602 recN=779 group=70 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222704 0.061 0.081 0.145 0.184 0.253 0.418 codaN=9598 recN=779 group=70 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222703 0.067 0.089 0.157 0.179 0.208 0.391 codaN=9595 recN=779 group=70 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222702 0.077 0.109 0.15 0.161 0.232 0.366 codaN=9593 recN=779 group=70 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222546 0.093 0.066 0.183 0.188 0.271 0.441 codaN=5776 recN=691 group=63 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222545 0.108 0.12 0.156 0.178 0.228 0.373 codaN=5773 recN=691 group=63 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222544 0.102 0.065 0.186 0.174 0.279 0.46 codaN=5772 recN=691 group=63 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222543 0.123 0.107 0.133 0.169 0.266 0.453 codaN=5770 recN=691 group=63 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222476 0.097 0.13 0.145 0.129 0.284 0.366 codaN=5422 recN=610 group=58 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222460 0.068 0.103 0.143 0.179 0.297 0.33 codaN=5334 recN=683 group=64 nClicks=7 kMeans=78 predecessor=222732 reachability=0.09184225606985055
ID=222207 0.076 0.129 0.14 0.201 0.255 0.428 codaN=3312 recN=318 group=20 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222205 0.063 0.102 0.148 0.2 0.202 0.414 codaN=3270 recN=318 group=20 nClicks=7 kMeans=75 predecessor=222732 reachability=0.09184225606985055
ID=222527 0.164 0.157 0.156 0.187 0.264 0.519 codaN=5645 recN=612 group=58 nClicks=7 kMeans=75 predecessor=222543 reachability=0.09692264957170743
ID=222459 0.076 0.127 0.178 0.236 0.3 0.453 codaN=5299 recN=683 group=64 nClicks=7 kMeans=75 predecessor=222732 reachability=0.0999849988748312
ID=222696 0.077 0.072 0.088 0.131 0.172 0.333 codaN=9133 recN=762 group=69 nClicks=7 kMeans=75 predecessor=222702 reachability=0.1052093151769367
ID=222175 0.105 0.114 0.145 0.253 0.34 0.441 codaN=3024 recN=295 group=25 nClicks=7 kMeans=78 predecessor=222207 reachability=0.10685504199615474
ID=222180 0.355 0.354 0.35 0.343 0.354 0.479 codaN=3089 recN=296 group=25 nClicks=7 kMeans=71 predecessor=222220 reachability=0.1149565135170687
