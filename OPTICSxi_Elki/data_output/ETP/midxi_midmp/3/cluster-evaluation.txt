# Pair counting measures
Jaccard 0.24616006944444443
F1-Measure 0.3950697434145616
Precision 1.0
Recall 0.24616006944444443
Rand 0.24616006944444443
ARI 0.0
FowlkesMallows 0.49614521003879947
# Entropy based measures
NMI Joint 0.0
NMI Sqrt 0.0
# BCubed-based measures
F1-Measure 0.3950697434145616
Recall 1.0
Precision 0.24616006944444446
# Set-Matching-based measures
F1-Measure 0.5339034819792303
Purity 0.3641666666666667
Inverse Purity 1.0
# Editing-distance measures
F1-Measure 0.5329800918836142
Precision 0.36375
Recall 0.9966666666666667
# Gini measures
Mean +-0.3770 0.6230800347222222
