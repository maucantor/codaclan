# Cluster: Cluster_1090_1159
# Cluster name: Cluster_1090_1159
# Cluster noise flag: false
# Cluster size: 70
# Parents: Cluster_0_1228
ID=209795 0.126 0.093 0.113 0.33 0.554 codaN=11095 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209672 reachability=0.11891173196955795
ID=209794 0.124 0.098 0.107 0.384 0.545 codaN=11094 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209793 0.149 0.124 0.14 0.398 0.554 codaN=11093 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209792 0.166 0.127 0.134 0.404 0.611 codaN=11092 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209791 0.15 0.127 0.129 0.408 0.578 codaN=11091 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209790 0.153 0.109 0.121 0.278 0.591 codaN=11088 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209789 0.121 0.105 0.116 0.33 0.609 codaN=11083 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209788 0.114 0.095 0.104 0.369 0.626 codaN=11082 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209787 0.124 0.098 0.114 0.282 0.609 codaN=11081 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209786 0.118 0.084 0.103 0.285 0.637 codaN=11080 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209782 0.106 0.087 0.095 0.328 0.616 codaN=11076 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209781 0.151 0.13 0.142 0.402 0.598 codaN=11075 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209780 0.097 0.091 0.104 0.282 0.615 codaN=11074 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209779 0.135 0.11 0.122 0.335 0.585 codaN=11073 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209778 0.108 0.093 0.095 0.379 0.612 codaN=11072 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209777 0.124 0.09 0.106 0.305 0.609 codaN=11071 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209776 0.123 0.103 0.111 0.405 0.579 codaN=11070 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209775 0.131 0.093 0.107 0.355 0.596 codaN=11069 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209774 0.122 0.1 0.102 0.312 0.574 codaN=11068 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209773 0.116 0.093 0.117 0.329 0.604 codaN=10408 recN=798 group=71 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209766 0.181 0.15 0.142 0.4 0.553 codaN=10208 recN=798 group=71 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209711 0.103 0.102 0.126 0.297 0.583 codaN=8603 recN=739 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209701 0.106 0.082 0.085 0.312 0.544 codaN=8586 recN=739 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209699 0.085 0.09 0.115 0.343 0.551 codaN=8580 recN=739 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209698 0.077 0.088 0.106 0.31 0.576 codaN=8578 recN=739 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209697 0.077 0.091 0.101 0.27 0.575 codaN=8576 recN=739 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209696 0.121 0.086 0.101 0.369 0.571 codaN=8575 recN=739 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209691 0.123 0.106 0.114 0.364 0.536 codaN=8514 recN=738 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209690 0.114 0.082 0.108 0.354 0.554 codaN=8513 recN=738 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209689 0.123 0.107 0.11 0.352 0.566 codaN=8512 recN=738 group=67 nClicks=6 kMeans=64 predecessor=209795 reachability=0.11213384859176109
ID=209688 0.083 0.081 0.122 0.274 0.597 codaN=8509 recN=738 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209687 0.083 0.069 0.085 0.307 0.576 codaN=8506 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209686 0.143 0.119 0.138 0.299 0.592 codaN=8502 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209685 0.128 0.08 0.112 0.357 0.562 codaN=8500 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209684 0.119 0.092 0.109 0.366 0.564 codaN=8499 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209683 0.145 0.094 0.148 0.32 0.576 codaN=8497 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209682 0.098 0.076 0.092 0.341 0.504 codaN=8495 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209681 0.112 0.085 0.1 0.326 0.537 codaN=8494 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209680 0.133 0.093 0.107 0.343 0.554 codaN=8493 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209679 0.131 0.092 0.117 0.32 0.582 codaN=8491 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209678 0.141 0.088 0.117 0.335 0.584 codaN=8489 recN=737 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209675 0.126 0.103 0.098 0.388 0.48 codaN=8484 recN=735 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209674 0.148 0.106 0.112 0.383 0.526 codaN=8483 recN=735 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209673 0.135 0.088 0.101 0.363 0.558 codaN=8482 recN=735 group=67 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209667 0.127 0.087 0.101 0.381 0.513 codaN=8473 recN=732 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209666 0.123 0.102 0.105 0.325 0.546 codaN=8472 recN=732 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209664 0.135 0.107 0.104 0.375 0.529 codaN=8470 recN=732 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209663 0.116 0.086 0.09 0.437 0.537 codaN=8469 recN=732 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209655 0.153 0.142 0.172 0.408 0.526 codaN=8452 recN=731 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209653 0.117 0.094 0.117 0.363 0.571 codaN=8450 recN=731 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209652 0.125 0.105 0.127 0.356 0.531 codaN=8449 recN=731 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209651 0.106 0.107 0.139 0.346 0.592 codaN=8448 recN=731 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209650 0.103 0.095 0.11 0.331 0.583 codaN=8447 recN=731 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209649 0.092 0.086 0.109 0.331 0.583 codaN=8446 recN=731 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209645 0.117 0.079 0.098 0.354 0.552 codaN=8439 recN=730 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209644 0.098 0.077 0.101 0.365 0.544 codaN=8438 recN=730 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209643 0.101 0.078 0.099 0.329 0.556 codaN=8437 recN=730 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209642 0.097 0.082 0.108 0.312 0.552 codaN=8436 recN=730 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209638 0.115 0.098 0.108 0.32 0.602 codaN=8427 recN=729 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209637 0.108 0.088 0.089 0.306 0.596 codaN=8425 recN=729 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209636 0.104 0.08 0.09 0.285 0.597 codaN=8423 recN=729 group=66 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209421 0.124 0.101 0.12 0.298 0.553 codaN=4899 recN=59 group=9 nClicks=6 kMeans=64 predecessor=209689 reachability=0.10346496991735894
ID=209676 0.178 0.112 0.123 0.233 0.554 codaN=8485 recN=736 group=67 nClicks=6 kMeans=64 predecessor=209683 reachability=0.10675673280875543
ID=209671 0.208 0.166 0.143 0.347 0.594 codaN=8480 recN=733 group=67 nClicks=6 kMeans=64 predecessor=209683 reachability=0.10675673280875543
ID=209501 0.212 0.15 0.178 0.317 0.603 codaN=5652 recN=613 group=58 nClicks=6 kMeans=64 predecessor=209683 reachability=0.10675673280875543
ID=209785 0.131 0.1 0.108 0.233 0.656 codaN=11079 recN=822 group=73 nClicks=6 kMeans=64 predecessor=209686 reachability=0.11615076409563564
ID=209654 0.093 0.108 0.155 0.304 0.691 codaN=8451 recN=731 group=66 nClicks=6 kMeans=64 predecessor=209686 reachability=0.11615076409563564
ID=209713 0.219 0.182 0.173 0.404 0.577 codaN=8612 recN=739 group=67 nClicks=6 kMeans=64 predecessor=209672 reachability=0.11891173196955795
ID=209692 0.192 0.199 0.154 0.41 0.551 codaN=8565 recN=740 group=67 nClicks=6 kMeans=64 predecessor=209672 reachability=0.11891173196955795
ID=209700 0.166 0.162 0.151 0.437 0.593 codaN=8582 recN=739 group=67 nClicks=6 kMeans=64 predecessor=209653 reachability=0.11891593669479295
