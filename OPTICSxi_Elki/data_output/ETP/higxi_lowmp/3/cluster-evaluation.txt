# Pair counting measures
Jaccard 0.5107288194444445
F1-Measure 0.6761356675942144
Precision 1.0
Recall 0.5107288194444445
Rand 0.5107288194444445
ARI 0.0
FowlkesMallows 0.7146529363575332
# Entropy based measures
NMI Joint 0.0
NMI Sqrt 0.0
# BCubed-based measures
F1-Measure 0.6761356675942144
Recall 1.0
Precision 0.5107288194444445
# Set-Matching-based measures
F1-Measure 0.8255444091020308
Purity 0.7029166666666666
Inverse Purity 1.0
# Editing-distance measures
F1-Measure 0.8242620740377543
Precision 0.7025
Recall 0.9970833333333333
# Gini measures
Mean +-0.2447 0.7553644097222223
