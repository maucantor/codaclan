# Pair counting measures
Jaccard 0.8290316845104844
F1-Measure 0.9065252302967769
Precision 1.0
Recall 0.8290316845104844
Rand 0.8290316845104844
ARI 0.0
FowlkesMallows 0.9105117706600417
# Entropy based measures
NMI Joint 0.0
NMI Sqrt 0.0
# BCubed-based measures
F1-Measure 0.9065252302967768
Recall 1.0
Precision 0.8290316845104843
# Set-Matching-based measures
F1-Measure 0.9523088569265707
Purity 0.9089595375722543
Inverse Purity 1.0
# Editing-distance measures
F1-Measure 0.9518210317276656
Precision 0.9087530966143683
Recall 0.9991742361684558
# Gini measures
Mean +-0.0855 0.9145158422552422
