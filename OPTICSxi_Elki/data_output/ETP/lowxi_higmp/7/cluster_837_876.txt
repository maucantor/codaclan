# Cluster: Cluster_837_876
# Cluster name: Cluster_837_876
# Cluster noise flag: false
# Cluster size: 40
# Parents: Cluster_0_1208
ID=223963 0.037 0.039 0.107 0.161 0.157 0.144 codaN=11358 recN=831 group=64 nClicks=7 kMeans=77 predecessor=223137 reachability=0.08813625814612283
ID=223962 0.037 0.048 0.12 0.181 0.187 0.161 codaN=11356 recN=831 group=64 nClicks=7 kMeans=77 predecessor=223963 reachability=0.0685200700525036
ID=223961 0.03 0.041 0.118 0.179 0.152 0.168 codaN=11351 recN=830 group=64 nClicks=7 kMeans=77 predecessor=223962 reachability=0.04758150901348128
ID=223960 0.029 0.032 0.12 0.177 0.162 0.17 codaN=11349 recN=830 group=64 nClicks=7 kMeans=77 predecessor=223962 reachability=0.04758150901348128
ID=223959 0.029 0.034 0.142 0.196 0.183 0.177 codaN=11348 recN=830 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223958 0.033 0.03 0.138 0.191 0.178 0.172 codaN=11345 recN=830 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223957 0.027 0.03 0.093 0.198 0.176 0.178 codaN=11277 recN=829 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223956 0.029 0.034 0.123 0.195 0.178 0.176 codaN=11253 recN=828 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223866 0.033 0.037 0.134 0.206 0.19 0.181 codaN=7860 recN=713 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223865 0.035 0.04 0.116 0.198 0.185 0.178 codaN=7853 recN=712 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223864 0.036 0.039 0.095 0.177 0.196 0.161 codaN=7850 recN=712 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223863 0.028 0.037 0.132 0.194 0.19 0.181 codaN=7816 recN=712 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223862 0.034 0.041 0.134 0.198 0.196 0.184 codaN=7801 recN=712 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223858 0.022 0.031 0.109 0.173 0.156 0.154 codaN=7448 recN=708 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223855 0.03 0.033 0.113 0.156 0.159 0.152 codaN=7283 recN=707 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223854 0.026 0.033 0.116 0.158 0.15 0.159 codaN=7139 recN=705 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223852 0.028 0.034 0.136 0.195 0.193 0.18 codaN=6920 recN=701 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223851 0.026 0.031 0.132 0.186 0.189 0.175 codaN=6919 recN=701 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223850 0.024 0.032 0.113 0.181 0.187 0.173 codaN=6917 recN=701 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223849 0.027 0.037 0.123 0.203 0.193 0.185 codaN=6888 recN=701 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223848 0.03 0.029 0.116 0.16 0.135 0.176 codaN=6866 recN=702 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223772 0.037 0.035 0.123 0.18 0.163 0.17 codaN=6268 recN=698 group=64 nClicks=7 kMeans=77 predecessor=223960 reachability=0.04577116996538324
ID=223771 0.036 0.039 0.125 0.184 0.193 0.178 codaN=6217 recN=697 group=64 nClicks=7 kMeans=77 predecessor=223772 reachability=0.04449719092257398
ID=223770 0.029 0.034 0.119 0.2 0.192 0.18 codaN=6216 recN=697 group=64 nClicks=7 kMeans=77 predecessor=223772 reachability=0.04449719092257398
ID=223769 0.029 0.034 0.138 0.2 0.183 0.175 codaN=6215 recN=697 group=64 nClicks=7 kMeans=77 predecessor=223772 reachability=0.04449719092257398
ID=223768 0.035 0.04 0.1 0.196 0.191 0.176 codaN=6103 recN=696 group=64 nClicks=7 kMeans=77 predecessor=223772 reachability=0.04449719092257398
ID=223763 0.028 0.036 0.092 0.176 0.177 0.165 codaN=5959 recN=693 group=64 nClicks=7 kMeans=77 predecessor=223772 reachability=0.04449719092257398
ID=223868 0.027 0.036 0.132 0.218 0.219 0.201 codaN=8034 recN=717 group=64 nClicks=7 kMeans=77 predecessor=223770 reachability=0.04642197755374065
ID=223859 0.026 0.034 0.137 0.213 0.192 0.201 codaN=7453 recN=708 group=64 nClicks=7 kMeans=77 predecessor=223770 reachability=0.04642197755374065
ID=223857 0.033 0.031 0.133 0.206 0.212 0.182 codaN=7389 recN=708 group=64 nClicks=7 kMeans=77 predecessor=223770 reachability=0.04642197755374065
ID=223767 0.033 0.039 0.138 0.21 0.206 0.189 codaN=6101 recN=696 group=64 nClicks=7 kMeans=77 predecessor=223770 reachability=0.04642197755374065
ID=223766 0.036 0.039 0.126 0.208 0.226 0.192 codaN=6100 recN=696 group=64 nClicks=7 kMeans=77 predecessor=223770 reachability=0.04642197755374065
ID=223762 0.032 0.036 0.15 0.205 0.222 0.196 codaN=5944 recN=693 group=64 nClicks=7 kMeans=77 predecessor=223770 reachability=0.04642197755374065
ID=223853 0.025 0.043 0.06 0.21 0.178 0.167 codaN=6921 recN=701 group=64 nClicks=7 kMeans=77 predecessor=223768 reachability=0.049000000000000016
ID=223761 0.033 0.038 0.157 0.175 0.165 0.211 codaN=5935 recN=693 group=64 nClicks=7 kMeans=77 predecessor=223863 reachability=0.050368641037852116
ID=223904 0.04 0.037 0.084 0.153 0.147 0.152 codaN=9079 recN=761 group=69 nClicks=7 kMeans=77 predecessor=223960 reachability=0.050665570163573606
ID=223773 0.029 0.035 0.103 0.137 0.135 0.144 codaN=6272 recN=698 group=64 nClicks=7 kMeans=77 predecessor=223961 reachability=0.05376802023508025
ID=223764 0.042 0.046 0.129 0.254 0.23 0.207 codaN=5970 recN=694 group=64 nClicks=7 kMeans=77 predecessor=223859 reachability=0.06020797289396148
ID=223847 0.032 0.029 0.111 0.252 0.24 0.205 codaN=6849 recN=702 group=64 nClicks=7 kMeans=77 predecessor=223857 reachability=0.06259392941811531
ID=223088 0.091 0.126 0.16 0.187 0.223 0.237 codaN=437 recN=622 group=62 nClicks=7 kMeans=74 predecessor=223718 reachability=0.08822698000045112
