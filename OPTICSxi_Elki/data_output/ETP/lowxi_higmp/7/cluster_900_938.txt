# Cluster: Cluster_900_938
# Cluster name: Cluster_900_938
# Cluster noise flag: false
# Cluster size: 39
# Parents: Cluster_0_1208
ID=223746 0.034 0.174 0.228 0.192 0.196 0.204 codaN=5676 recN=613 group=58 nClicks=7 kMeans=74 predecessor=223098 reachability=0.09291931984253868
ID=223744 0.039 0.168 0.23 0.192 0.181 0.225 codaN=5674 recN=613 group=58 nClicks=7 kMeans=74 predecessor=223746 reachability=0.07636098480245004
ID=223743 0.05 0.172 0.196 0.202 0.164 0.183 codaN=5673 recN=613 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223742 0.034 0.16 0.242 0.202 0.178 0.24 codaN=5672 recN=613 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223741 0.033 0.164 0.208 0.184 0.176 0.167 codaN=5671 recN=613 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223740 0.033 0.167 0.2 0.192 0.169 0.2 codaN=5670 recN=613 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223739 0.04 0.164 0.24 0.188 0.187 0.247 codaN=5665 recN=613 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223727 0.046 0.18 0.236 0.192 0.181 0.265 codaN=5620 recN=615 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223726 0.047 0.171 0.225 0.178 0.185 0.241 codaN=5619 recN=615 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223724 0.041 0.163 0.241 0.172 0.183 0.246 codaN=5614 recN=615 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223722 0.047 0.19 0.223 0.191 0.18 0.258 codaN=5611 recN=615 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223720 0.041 0.177 0.218 0.172 0.171 0.241 codaN=5601 recN=615 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223719 0.04 0.186 0.215 0.175 0.183 0.251 codaN=5600 recN=615 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223706 0.031 0.157 0.262 0.189 0.17 0.243 codaN=5522 recN=686 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223705 0.034 0.19 0.239 0.177 0.19 0.252 codaN=5521 recN=686 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223699 0.044 0.187 0.225 0.177 0.174 0.228 codaN=5492 recN=685 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223698 0.039 0.183 0.226 0.178 0.174 0.275 codaN=5485 recN=685 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223696 0.046 0.207 0.206 0.174 0.177 0.26 codaN=5469 recN=685 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223692 0.035 0.114 0.256 0.183 0.179 0.222 codaN=5457 recN=685 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223688 0.048 0.181 0.208 0.176 0.174 0.209 codaN=5443 recN=610 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223687 0.032 0.145 0.213 0.205 0.17 0.188 codaN=5442 recN=610 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223686 0.032 0.163 0.207 0.187 0.179 0.181 codaN=5441 recN=610 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223684 0.036 0.186 0.201 0.176 0.183 0.233 codaN=5420 recN=610 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223683 0.043 0.185 0.23 0.182 0.177 0.263 codaN=5419 recN=610 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223682 0.046 0.198 0.225 0.18 0.174 0.249 codaN=5418 recN=610 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223681 0.04 0.197 0.21 0.178 0.181 0.229 codaN=5417 recN=610 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223680 0.035 0.128 0.25 0.175 0.183 0.225 codaN=5416 recN=610 group=58 nClicks=7 kMeans=74 predecessor=223744 reachability=0.06315853069855251
ID=223725 0.059 0.203 0.22 0.185 0.176 0.291 codaN=5618 recN=615 group=58 nClicks=7 kMeans=74 predecessor=223699 reachability=0.0686512927773396
ID=223407 0.056 0.143 0.247 0.206 0.222 0.273 codaN=3217 recN=675 group=20 nClicks=7 kMeans=74 predecessor=223726 reachability=0.06933974329343887
ID=223105 0.073 0.126 0.184 0.196 0.207 0.259 codaN=1011 recN=634 group=62 nClicks=7 kMeans=74 predecessor=223726 reachability=0.0742563128629479
ID=223092 0.079 0.141 0.196 0.21 0.23 0.263 codaN=678 recN=629 group=62 nClicks=7 kMeans=74 predecessor=223726 reachability=0.07935993951610601
ID=223101 0.054 0.139 0.172 0.213 0.229 0.242 codaN=793 recN=629 group=62 nClicks=7 kMeans=74 predecessor=223684 reachability=0.08330666239863413
ID=223111 0.104 0.148 0.201 0.234 0.264 0.278 codaN=1251 recN=642 group=62 nClicks=7 kMeans=74 predecessor=223407 reachability=0.08496469855181034
ID=223100 0.077 0.143 0.172 0.212 0.227 0.252 codaN=784 recN=629 group=62 nClicks=7 kMeans=74 predecessor=223407 reachability=0.08496469855181034
ID=223097 0.072 0.148 0.187 0.225 0.241 0.254 codaN=728 recN=629 group=62 nClicks=7 kMeans=74 predecessor=223407 reachability=0.08496469855181034
ID=223091 0.069 0.13 0.173 0.224 0.252 0.268 codaN=660 recN=629 group=62 nClicks=7 kMeans=74 predecessor=223407 reachability=0.08496469855181034
ID=223110 0.09 0.13 0.189 0.235 0.277 0.286 codaN=1249 recN=642 group=62 nClicks=7 kMeans=74 predecessor=223105 reachability=0.08648699324175863
ID=223107 0.106 0.14 0.144 0.212 0.248 0.264 codaN=1051 recN=635 group=62 nClicks=7 kMeans=74 predecessor=223105 reachability=0.08648699324175863
ID=223383 0.272 0.257 0.282 0.277 0.292 0.458 codaN=3020 recN=295 group=25 nClicks=7 kMeans=72 predecessor=223423 reachability=0.09392550239418472
