# Pair counting measures
Jaccard 0.2938495032510828
F1-Measure 0.45422516685707415
Precision 1.0
Recall 0.2938495032510828
Rand 0.2938495032510828
ARI 0.0
FowlkesMallows 0.5420788717991901
# Entropy based measures
NMI Joint 0.0
NMI Sqrt 0.0
# BCubed-based measures
F1-Measure 0.4542251668570741
Recall 1.0
Precision 0.2938495032510828
# Set-Matching-based measures
F1-Measure 0.5872274143302181
Purity 0.41565600882028664
Inverse Purity 1.0
# Editing-distance measures
F1-Measure 0.5853663582612174
Precision 0.41455347298787215
Recall 0.9955898566703418
# Gini measures
Mean +-0.3532 0.6469247516255413
