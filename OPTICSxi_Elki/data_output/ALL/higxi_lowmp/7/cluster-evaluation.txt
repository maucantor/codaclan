# Pair counting measures
Jaccard 0.9037322988506088
F1-Measure 0.9494321227792828
Precision 1.0
Recall 0.9037322988506088
Rand 0.9037322988506088
ARI 0.0
FowlkesMallows 0.9506483570966758
# Entropy based measures
NMI Joint 0.0
NMI Sqrt 0.0
# BCubed-based measures
F1-Measure 0.9494321227792828
Recall 1.0
Precision 0.9037322988506086
# Set-Matching-based measures
F1-Measure 0.9743160245672808
Purity 0.949918345127926
Inverse Purity 1.0
# Editing-distance measures
F1-Measure 0.9732542638692329
Precision 0.9493739793140991
Recall 0.9983669025585193
# Gini measures
Mean +-0.0481 0.9518661494253043
