# Pair counting measures
Jaccard 0.5286932855388496
F1-Measure 0.6916930826349386
Precision 1.0
Recall 0.5286932855388496
Rand 0.5286932855388496
ARI 0.0
FowlkesMallows 0.7271129798998569
# Entropy based measures
NMI Joint 0.0
NMI Sqrt 0.0
# BCubed-based measures
F1-Measure 0.6916930826349384
Recall 1.0
Precision 0.5286932855388495
# Set-Matching-based measures
F1-Measure 0.8352115670848393
Purity 0.7170500182548375
Inverse Purity 1.0
# Editing-distance measures
F1-Measure 0.8340716530562176
Precision 0.7166849215041986
Recall 0.9974443227455275
# Gini measures
Mean +-0.2357 0.7643466427694248
