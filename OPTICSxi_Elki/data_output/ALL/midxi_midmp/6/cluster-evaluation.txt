# Pair counting measures
Jaccard 0.24996295185809356
F1-Measure 0.3999525769728117
Precision 1.0
Recall 0.24996295185809356
Rand 0.24996295185809356
ARI 0.0
FowlkesMallows 0.49996295048542705
# Entropy based measures
NMI Joint 0.0
NMI Sqrt 0.0
# BCubed-based measures
F1-Measure 0.3999525769728117
Recall 1.0
Precision 0.24996295185809356
# Set-Matching-based measures
F1-Measure 0.6294528094013955
Purity 0.4592711682743837
Inverse Purity 1.0
# Editing-distance measures
F1-Measure 0.6278854331667124
Precision 0.4587352625937835
Recall 0.9946409431939979
# Gini measures
Mean +-0.3751 0.6249814759290467
