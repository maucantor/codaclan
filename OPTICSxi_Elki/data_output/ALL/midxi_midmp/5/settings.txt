# Settings:
# de.lmu.ifi.dbs.elki.workflow.InputStep
# -db StaticArrayDatabase
# 
# de.lmu.ifi.dbs.elki.database.StaticArrayDatabase
# -dbc FileBasedDatabaseConnection
# 
# de.lmu.ifi.dbs.elki.datasource.FileBasedDatabaseConnection
# -dbc.in /Users/maucantor/Documents/Copy/Doctor/Cantor_etal_codaclans/codaclanrepo/OPTICSxi_Elki/data_input/ALL/dataOptics_ALLG_5clickcoda.csv
# -dbc.parser NumberVectorLabelParser
# 
# de.lmu.ifi.dbs.elki.datasource.parser.NumberVectorLabelParser
# -parser.colsep \s*[,;\s]\s*
# -parser.quote "'
# -string.comment ^\s*(#|//|;).*$
# -parser.labelIndices [unset]
# -parser.vector-type DoubleVector
# 
# de.lmu.ifi.dbs.elki.datasource.FileBasedDatabaseConnection
# -dbc.filter [unset]
# 
# de.lmu.ifi.dbs.elki.database.StaticArrayDatabase
# -db.index [unset]
# 
# de.lmu.ifi.dbs.elki.workflow.AlgorithmStep
# -time false
# -algorithm clustering.optics.OPTICSXi
# 
# de.lmu.ifi.dbs.elki.algorithm.clustering.optics.OPTICSXi
# -opticsxi.xi 0.025
# -opticsxi.algorithm OPTICS
# 
# de.lmu.ifi.dbs.elki.algorithm.clustering.optics.OPTICS
# -algorithm.distancefunction minkowski.EuclideanDistanceFunction
# -optics.epsilon 250.0
# -optics.minpts 100
# 
# de.lmu.ifi.dbs.elki.workflow.EvaluationStep
# -evaluator AutomaticEvaluation
