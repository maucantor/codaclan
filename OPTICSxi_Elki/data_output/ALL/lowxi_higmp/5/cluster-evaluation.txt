# Pair counting measures
Jaccard 0.2639292481576024
F1-Measure 0.4176329466895799
Precision 1.0
Recall 0.2639292481576024
Rand 0.2639292481576024
ARI 0.0
FowlkesMallows 0.5137404482397725
# Entropy based measures
NMI Joint 0.0
NMI Sqrt 0.0
# BCubed-based measures
F1-Measure 0.41763294668957995
Recall 1.0
Precision 0.26392924815760244
# Set-Matching-based measures
F1-Measure 0.5009074410163339
Purity 0.3341404358353511
Inverse Purity 0.9999999999999999
# Editing-distance measures
F1-Measure 0.5005345410845891
Precision 0.33395418141180855
Recall 0.9986962190352021
# Gini measures
Mean +-0.3681 0.6319646240788012
