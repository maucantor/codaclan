function codaDiscovery(dataCodas, repertoire, individual)
%function codaDiscovery(dataCodas, repertoire, individual)
%
% Discovery curve plots of coda types by repertoires, and overall cumulative
%curve with SE intervals
%
% INPUT
%'dataCodas':standard dataCodas with the 46th column giving the coda
%type, either via kmeans or OPTICSxi, or something else. Necessary columns are: 
%1st(coda#), 2nd(date or rec#), 3rd(repertoire), 4th(Nclicks), 5th(total length),
%6-40th(ICIs), 46th(coda type).
%
%'repertoire': number specifying the column where the repertoire labels
%are. Usually, this is the 3rd col, so 'repertoire=3'
%
%'individual': Do you have coda repertoires per individuals? individual=1
%plots discovery curves per individuals, given in the 60th column of the
%dataset. Otherwise, set 'individual=0'.
%
% Shane Gero, modified by Mauricio Cantor June 2015


if individual==1 % if there are individual repertoires
    inds = unique(n(~isnan(n(:,60)),60));
    inds = inds(inds>0);
    nInd = numel(inds);
    individualData = cell(nInd,3); % data for individual are id, list of types, and count of n types for each n coda recorded
    individualData(:,1) = num2cell(inds); %add in individual ids
    individualData(:,3) = num2cell(zeros(size(inds))); %everyone starts at zero
end

units = unique(dataCodas(~isnan(dataCodas(:,repertoire)),repertoire));
nUnits = numel(units);
unitData = cell(nUnits, 3);
unitData(:,1) = num2cell(units);
unitData(:,3) = num2cell(zeros(size(units)));

maxI = 0;
maxIu = 0;


for c = 1:size(dataCodas,1) % loop through all codas
    
    if individual==1 % if there are individual repertoires
        if ~isnan(dataCodas(c,60)) && dataCodas(c,60)>0 
            i = dataCodas(c,60); % this is the individual
            iInd = find(inds==i); % this is the index
            thisType = dataCodas(c,46); % get the coda type
            disp(['Individual ' num2str(i) '; coda type ' num2str(thisType)])
            disp(individualData{iInd,1})
            disp(individualData{iInd,2})
            disp(individualData{iInd,3})
            known = individualData{iInd,2}; % recover known codas
            disc = individualData{iInd,3}; % recover discovery history
            if isempty(known) % if no codas known then this must be new
                known = thisType; % add to known
                disc = [disc disc(end)+1]; %add discovery entry with increment
                disp('FIRST::::::::::::::')
            elseif any(known==thisType) % if not a new coda
                disc = [disc disc(end)]; %add discovery entry no increment
                disp('KNOWN::::::::::::::')
            else %if a new coda
                known = [known; thisType]; %add to known
                disc = [disc disc(end)+1]; %add dicovery entry increment
                disp('NEW::::::::::::::')
            end
             individualData{iInd,2} = known; %update individual known repertoire
            individualData{iInd,3} = disc; % update individual discovery record
            if numel(disc)>maxI
                maxI = numel(disc);
            end
            disp(individualData{iInd,1})
            disp(individualData{iInd,2})
            disp(individualData{iInd,3})    
        end
    end
    
    % Per repertoire
    if ~isnan(dataCodas(c,repertoire)) && dataCodas(c,repertoire)>0 
        u = dataCodas(c,repertoire); % this is the unit
        uInd = find(units==u); % this is the index
        thisType = dataCodas(c,46); % get the coda type
        disp(['Unit ' num2str(u) '; coda type ' num2str(thisType)])
        %disp(unitData{uInd,1})
        %disp(unitData{uInd,2})
        %disp(unitData{uInd,3})
        known = unitData{uInd,2}; % recover known codas
        disc = unitData{uInd,3}; % recover discovery history
        if isempty(known) % if no codas known then this must be new
            known = thisType; % add to known
            disc = [disc disc(end)+1]; %add discovery entry with increment
            disp('FIRST CODA TYPE::::::::::::')
        elseif any(known==thisType) % if not a new coda
            disc = [disc disc(end)]; %add discovery entry no increment
            disp('KNOWN CODA TYPE::::::::::::')
        else %if a new coda
            known = [known; thisType]; %add to known
            disc = [disc disc(end)+1]; %add dicovery entry increment
            disp('NEW CODA TYPE::::::::::::::')
        end
        unitData{uInd,2} = known; %update individual known repertoire
        unitData{uInd,3} = disc; % update individual discovery record
        if numel(disc)>maxIu
            maxIu = numel(disc);
        end
        %disp(unitData{uInd,1})
        %disp(unitData{uInd,2})
        %disp(unitData{uInd,3})    
    end
end

% Plotting
 if individual==1 % if there are individual repertoires

    combinedI = zeros(nInd,maxI);
    for i=1:nInd
        combinedI(i,1:numel(individualData{i,3})) = individualData{i,3};
    end
    countsI = sum(logical(combinedI));

    meansI = [0;0];
    semI = [0];
    for c = 1:maxI
       data = combinedI(:,c);
       data = data(data>0);
       if numel(data)>1
           meansI = [meansI [c;mean(data)]];
           semI = [semI  std(data)/sqrt(countsI(c))];
       end
    end

    figure
    set(gcf,'color','w');

    for i=1:nInd
       h = plot(individualData{i,3},'k-');
       hold on
    end
    hm = plot(meansI(1,:),meansI(2,:),'r-');
    set(hm,'LineWidth',2)
    hs = patch([meansI(1,:) fliplr(meansI(1,:))],[meansI(2,:)+semI fliplr(meansI(2,:)-semI)],[0.8 0.8 0.8]);
    set(hs,'EdgeColor','none','FaceColor','r')
    set(hs,'FaceAlpha',0.25)
    set(gca,'Fontsize',12)
    xlabel('Number of codas recorded','Fontsize',14)
    ylabel('Number of coda types identified','Fontsize',14)
    title('By Individual','Fontsize',16)
    legend([h hm],'Individual data','Mean +/- 1 standard error','Location','SouthEast')
 end

 % Per repertoire
    combinedU = zeros(nUnits,maxIu);
    for i=1:nUnits
        combinedU(i,1:numel(unitData{i,3})) = unitData{i,3};
    end
    countsU = sum(logical(combinedU));

    meansU = [0;0];
    semU = [0];
    for c = 1:maxIu
       data = combinedU(:,c);
       data = data(data>0);
       if numel(data)>1
           meansU = [meansU [c;mean(data)]];
           semU = [semU  std(data)/sqrt(countsU(c))];
       end
    end

    figure
    for i=1:nUnits
       h = plot(unitData{i,3},'k-');
       hold on
    end
    hm = plot(meansU(1,:),meansU(2,:),'r-');
    set(hm,'LineWidth',2)
    hs = patch([meansU(1,:) fliplr(meansU(1,:))],[meansU(2,:)+semU fliplr(meansU(2,:)-semU)],[0.8 0.8 0.8]);
    set(hs,'EdgeColor','none','FaceColor','r')
    set(hs,'FaceAlpha',0.25)
    set(gca,'Fontsize',12,'Ylim',[0 22])
    xlabel('Number of codas recorded','Fontsize',14)
    ylabel('Number of coda types identified','Fontsize',14)
    %title('By Unit','Fontsize',16)
    legend([h hm],'Single repertoires','Mean +/- 1 standard error','Location','SouthEast')
    
end