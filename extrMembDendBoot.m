function [dataClusplot,membership_nodes,branchReplicateCutoff] = extrMembDendBoot(codas,outboot,level)

% Extracts the members of branches of dendrogram replicated by bootstrap
%
% function [dataClusplot,membership_nodes,branchReplicateCutoff] = 
%           extrMembDendBoot(codas,outboot,level)
% 
% function to define a categorical variable to group repertoires of codas
% based on bootstrapped dendrograms.
%
% It prepares the basic coda data matrix to 'clusplot.m', i.e. adds the 46th
% column from which clustplot evaluates each coda lenght. This 46th column
% will contain the branches of a bootstrapped dendrogram
%
% outputs: 
%'dataClustplot': the coda matrix data with a 46th column and the membership matrix
% showing the nodes of the dendrogram (correspondent to the 3rd colum of
% coda data matrix - the repertoire label)
% 'membership_nodes': matrix of cluster/branch membership (col1), the corresponding
% node label in the original dendrogram (col2) and the original coda
% repertoire label (col3).
%'branchReplicateCutoff': the number of time the branch used to define the
% cluster was replicated in the bootstrap procedure
% 
%'codas': the standard coda data matrix, i.e the output of 'makeCodaFile.m'
% and input of 'daysim_boot.m' and 'daysim_boot-mc.m'. It contains
% coda# (col1), time (c2), GROUPING VARIABLE (c3), nclicks (c4), coda length(s) (c4),
% ICIs from 1 2 3 4 5 6 up to 40. The new membership from the dendrogram
% will be added at the column 46, which will be used by the function
% 'clusplot.m'. 
%
%'outboot': the output matrix of the modified functions 'bootcompz_mc.m' and
%'daysim_boot_mc.m' which contains branch (column 1), times the branch was reproduced in the
% bootstrap (col2) and the members of the branch (col3-end).
%
%'level': a value between 0 and the maximum number of braches evaluated in
%the 'day_sim_boot_mc.m', i.e. the 'nbranchdown' parameter. If level=0 it
%returns the branch level with the highest support (i.e. the maximum number
%of times the branch was replicated in the bootstrap
%
% Mauricio Cantor, jan 2015

if level==0
    %gets the level with the highest number of replicates. If more than
    %one, takes the first
    cutLevel = find(outboot(:,2) == max(outboot(:,2)), 1);
    breplic = max(outboot(:,2));
else
    cutLevel = level;
    breplic = outboot(cutLevel,2);
end

%takes the total number of members (i.e. total nodes in the dendrogram)
allMembers = outboot(:,3:end);
totMembers = max(allMembers(:));

%Splitting in 2 clusters
%take all members defined by the 'level' (and remove zeros, ie no membership)
members1 = outboot(cutLevel, 3:end);
members1(members1 == 0) = [];
%assign these nodes to the branch1 (cluster 1)
branch1 = repmat(1, 1, size(members1, 2));
%all the rest is assigned to branch2 (cluster 2)
members2 = setdiff(1:totMembers, members1);
branch2 = repmat(2, 1, size(members2, 2));

%prepare the output matrix with the clusters (1st column) and nodes(2nd col)
branchMembership = [branch1 branch2; members1 members2]';

%Assigning the cluster memberships to the overall coda data matrix ('codas')
%1.includes the original repertoire label (3rd column in the dataCoda matrix: 
%i.e. encounter, day or group)into the membership output
branchMembership = [sortrows(branchMembership, 2) unique(codas(:,3))];

%2.Creates 2 empty vectors: fills one up with respective branch label and other with branch membership
dendroLabelVector = zeros(size(codas,1),1);
memberVector = zeros(size(codas,1),1);
for m = 1:size(branchMembership,1)
    dendroLabelVector(codas(:,3)==branchMembership(m,3)) = branchMembership(m,2);
    memberVector(dendroLabelVector==branchMembership(m,2)) = branchMembership(m,1);
end
%3. Adds the cluster memberships to the data codas at the 46th column for the clusplot.m
codas(:,46) = memberVector;

%outputs
dataClusplot = codas;
membership_nodes = branchMembership
branchReplicateCutoff = breplic