function x = bootcompz(zin,zboot,nbranchdown,print)

%function x =bootcompz(zin,zboot,nbranchdown)
%This function checks how many times branches in the linkage data zin
%are repeated in the 3d array zboot of bootstrap linkage data
%Analysis proceeds up to the nbranchdown'th branch in the original tree
%
%'print' if=1 returns a matrix with branch (c1), times this branch was
%reproduced (c2) and the members of this branch (c3 on)
%
%Modified by Mauricio Cantor, jan 2015

[d,dd,nboots] = size(zboot); %get number of bootstraps

[zbranches] = branches(zin,nbranchdown); %get branches matrix
x = zeros(1, nbranchdown);

for nb = 1:nboots
    [tbranches] = branches(zboot(:,:,nb),size(zin,1)-1); %get test branches matrix
    %here make sure they are exactly the same size
    if size(zbranches,2)>size(tbranches,2);tbranches = [tbranches zeros(size(tbranches,1),size(zbranches,2)-size(tbranches,2))];end
    if size(zbranches,2)<size(tbranches,2);zbranches = [zbranches zeros(size(zbranches,1),size(tbranches,2)-size(zbranches,2))];end
    if ~size(tbranches,2)==size(zbranches,2); disp('Branches matrix size mismatch');end
    branchsize = size(zbranches,2);
    for nd = 1:nbranchdown
        zz = zbranches(nd,:); %get nbth branch
        if find(sum(tbranches==repmat(zz,size(zin,1)-1,1),2)==branchsize) %if any branches match exactly
           x(nd)=x(nd)+1; %add one to total of reproduced branches
        end
    end %next branch
end %next bootstrap
        
for d = 1:nbranchdown
    disp(['Branch ' num2str(d) ' reproduced ' num2str(x(d)) ' times'])
    disp(['Branch contains nodes ' num2str(zbranches(d,find(zbranches(d,:))))]);
end

if print
    matOutput = zeros(nbranchdown, size(zin,1)+2); %output matrix: c1:Branch#, c2:times reproduced, c3 on: members of each branch (max total members)
    %headerStr = ['Branch,Times' sprintf('Node%d,',1:size(zin,1))];
    for d = 1:nbranchdown
        matOutput(d,1) = d;
        matOutput(d,2) = x(d);
        memb = zbranches(d,find(zbranches(d,:)));
        matOutput(d,3:(2+(size(memb,2)))) = memb;
    end
    x = matOutput;
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [obranches] = branches(Z,nbd)   

n = size(Z,1)+1; %number of data nodes
m = size(Z,1);
obranches = [];

for x = 1:m-1
    T = zeros(n,1);
    T = clusternum(Z,T,1,x);
    nodeso = find(T);
    obranches(x,1:size(nodeso,1)) = nodeso';
%    disp(['Branch ' num2str(x) ' contains nodes ' num2str(nodeso')]);
end
obranches = flipud(obranches);
obranches = obranches(1:nbd,:);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
function T = clusternum(X, T, c, k)
% assign leaves under cluster c to c.
m = size(X,1)+1;
i = X(k,1); % left tree
if i <= m % original node, no leafs
   T(i) = c;
else % created before cutoff, search down the tree
   T = clusternum(X, T, c, i-m);
end
i = X(k,2); % right tree
if i <= m % original node, no leafs
   T(i) = c;
else % created before cutoff, search down the tree
   T = clusternum(X, T, c, i-m);
end
