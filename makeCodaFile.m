function makeCodaFile(filename,metadataFile,pathToClickFiles)
%function to generate csv file of coda data from given click file
%filename is name of clk file, metadataFile is name of Excel file with meta
%data - code expects filename to be 7th column and encounter ID to be 5th
%column in this excel sheet

%change this to correct path for click files
%pathToClickFiles = 'C:\Users\cantor\Documents\Doctor\_Cachalote_coredata\Galapagos_2013_codas_RainbowClick\';

%change this to correct path for meta data file
%pathToMeta = 'C:\Users\cantor\Desktop\';

[d1 d2 metaData] = xlsread(metadataFile);

%get encounter data and add the year to it (e.g. 13001 is the first 
%encounter of the year 2013)
encounter = metaData{strmatch(filename(1:end-4),metaData(:,7)),5}+(1000*str2num(filename(7:8)));

%get click data from clk file
c = readcodasfromclk([pathToClickFiles filename]);

Fs = c(1).sr; %get sample rate - assumes does not change during file!

fileStartTime = datenum(filename(5:end-4),'yyyymmdd_HHMMSS'); %get start time of file from file name

iciData = zeros(numel(c),45);

for n=1:numel(c)%loop through each coda start
    disp(n)
    startTimeSec = c(n).time; % number of seconds into file
    startTimeSerial = datenum(0,0,0,0,0,startTimeSec); %as a date serial
    startTime = fileStartTime+startTimeSerial; %get coda start time absolute
    datestr(startTime)
    nClick = c(n).nclicks;
    icis = c(n).icis'; %get inter click intervals in seconds
    disp([nClick icis])
    codaLength = c(n).length;
    codavec = [n startTime encounter nClick codaLength icis];
    iciData(n,1:length(codavec)) = codavec;
end

iciData = sortrows(iciData,2);
iciData(:,1) = [1:n]';
disp('******************** WRITING FILE')
dlmwrite([pathToClickFiles filename(1:end-4) '.csv'],iciData,'delimiter',',','precision','%.6f')
disp('******************** DONE')