function processAllClickFiles(metadataFile, pathToClickFiles)
% processAllClickFiles(metadataFile, pathToClickFiles)
% Reads all clk files in a given folder
% Select the useful files (with marked codas), given by the column 'worthy
% to analyse?' (y/n)
% Run 'makeCodaFiles.m' to create individual csv files
%
% INPUT: 
%'metadatafile': excel file with information on recording files. Columns:
%Date,area,yyyymmdd,hhmmss,encounter,predominant_sex,record_file,Rainbowclick_version,
%Notes_wav,Scan Complete,Codas,Slow Clicks,Creaks,Notes_clk,Marked_By,Scanned_MakeCoda,
%Make_codas,Worthy_analyzing?
%'pathToClickFiles': path to folder with rainbowclik .clk files
%
%OUTPUT:
%csv files, one per clk file, with coda data. Columns:
%Coda#,date,repertoire,Nclicks,codalength,ICI(1-40)
%
% Mauricio Cantor & Luke Rendell, jan 2015

%%read metadata into Matlab
[d1 metaData d2] = xlsread(metadataFile);

%%select column with clk file names (7), sets the selection file criterion
%(if it's worthy to analyse, column 18), select the files worthy to analyze
%('filesToDo')
fileNames = metaData(2:end,7);
worthy = metaData(2:end,18);
i = strmatch('Y',worthy);
filesToDo = fileNames(i);
filesNotFound = {};

%loop to run function 'makeCodaFile.mat' to all clk files
for f = 1:length(filesToDo)
   disp(filesToDo{f})
   filename = [filesToDo{f} '.clk'];
   fd = dir([pathToClickFiles filename]);
   if length(fd)>0
       disp('PROCESSING')
       makeCodaFile(filename,metadataFile,pathToClickFiles) 
   else
       filesNotFound{end+1} = filename;
   end
end

%display eventual files not found in folder
if length(filesNotFound)>0
    disp('Files not found:')
    disp(filesNotFound')
else disp('All useful clk files in the folder were processed.')
disp('.csv files were saved in the same folder as the original clk files')
end