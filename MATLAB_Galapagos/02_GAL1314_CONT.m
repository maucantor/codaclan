%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014 Acoustic Analysis
% Mauricio Cantor, St Andrews University, Scotland, Jan 2015
%
% REPERTOIRE COMPARISONS:
% Summary:
% 1. Loadings
% 2. Repertoire comparison (for encounters, days, groups) with continuous metrics: Infinity-norm 
% 3. Repertoire comparison (for encounters, days, groups) with continuous metrics: Euclidean distance
% 4. Clusplots for GAL data based on Boostrap branches
% 5. Mantel tests for group repertoires
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1. Loadings 

% Set input paths
% Please set your path to the working directory manually
addpath('Coda_K-means')
addpath('Coda_multivariate')
addpath('GroupSizesPetersen')
addpath('data')
addpath('MATLAB_Galapagos')
addpath('_Cachalote_coredata')

% Set output paths
outputpath = [pwd, 'data'];

% Input similarity data
dataCodasE = load('dataCodasE.csv');
dataCodasD = load('dataCodasD.csv');
dataCodasG = load('dataCodasG.csv');





%% 2. Repertoire Similarity with Continuous metrics: Infinity-norm

% Summary:
% Calculate repertoire similarity with continuous metrics 
% Bootstrap repertoires to replicate branches
% Plot dendrograms + CCC
% Extract branch membership from dendrograms and add to data set
% Clustplot to explore the patterns of the clusters extracted from the dendrogram with bootstrap
    % Using 3 repertoire labels: encounters, days, groups (prefer group)
    % Using 2 continuous metrics: Euclidean distance and Infinity-norm (prefer the 1st?)
    % Using 1 b-value for infinity-norm, b = 0.001(Rendell&Whitehead2003)
    % Using 1 type of raw data: absolute ICI (for standardized ICI choose last standardise=1 in 'daysim')



%%%%Rejected days/encounters/groups when using minimum 25 codas:
rejected_day = [735640 735686 735732] 
datestr(rejected_day)% miminum 10 codas
rejected_group = [735640]
datestr(rejected_group) %minimum 16 codas: problem with equalization, there are codas, but impossible to mark!!
rejected_enc = [735640]
datestr(rejected_enc) %minimum 16 codas: problem with equalization, there are codas, but impossible to mark!!
%%%%


% 2.1. Absolute ICI, Infinity norm similarity (b=0.001)

% 2.1.1. Encounters
% Infinity-norm similarity
simAbsInfEnc = daysim(dataCodasE,2,16,0);
% Bootstrapping repertoires within encounters to replicate dendrogram branches
[simsoutIE, z0001bootIE, z001bootIE, z01bootIE, z1bootIE,out0001IE,out001IE,out01IE,out1IE] = daysim_boot_mc(dataCodasE,100,2,9,5,0,1);
% empirical dendrogram
zIE = linkage_sim(simAbsInfEnc(:,3)'); 
dendrogram_sim(zIE,0);
% CCC
cccIE = cophenet(zIE, simAbsInfEnc(:,3)')
% Editing the dendrogram
hold on;
title(cccIE)%CCC
ylabel('Infinity-norm similarity, b=0.001');
xlabel('Encounters')
% To add the number of replicates to the text it is easier to pick up the data from the output matrices (second column: out0001IE(:,2)
% and add using the interative menu Insert>TextBox. Or try to add using text(x,y, out0001IE(1,2)), text(x,y, out0001IE(2,2)) etc
hold off;

% 2.1.2 Days
% Infinity-norm similarity (b=0.001)
simAbsInfDay = daysim(dataCodasD,2,10,0);
% Bootstrapping repertoires within days to replicate dendrogram branches
[simsoutID, z0001bootID, z001bootID, z01bootID, z1bootID,out0001ID,out001ID,out01ID,out1ID] = daysim_boot_mc(dataCodasD,100,2,7,5,0,1);
% empirical dendrogram
zID = linkage_sim(simAbsInfDay(:,3)');
dendrogram_sim(zID,0);
% CCC
cccID = cophenet(zID, simAbsInfDay(:,3)')
% Editing the dendrogram
hold on;
title(cccID)%CCC
ylabel('Infinity-norm similarity, b=0.001');
xlabel('Days')
% To add the number of replicates to the text it is easier to pick up the data from the output matrices (second column: out0001ID(:,2)
% and add using the interative menu Insert>TextBox. Or try to add using text(x,y, out0001IE(1,2)), text(x,y, out0001IE(2,2)) etc
hold off;

% 2.1.3.Groups
% Infinity-norm similarity (b=0.001)
simAbsInfGro = daysim(dataCodasG,2,25,0);
% Bootstrapping repertoires within groups to replicate dendrogram branches
[simsoutIG, z0001bootIG, z001bootIG, z01bootIG, z1bootIG,out0001IG,out001IG,out01IG,out1IG] = ...
    daysim_boot_mc(dataCodasG,100,2,25,10,0,1);
% empirical dendrogram
zIG = linkage_sim(simAbsInfGro(:,3)');
dendrogram_sim(zIG,0);
% CCC
cccIG = cophenet(zIG, simAbsInfGro(:,3)')
% Editing the dendrogram
hold on;
title(cccIG)%CCC
ylabel('Infinity-norm similarity, b=0.001');
xlabel('Groups')
% To add the number of replicates to the text it is easier to pick up the data from the output matrices (second column: out0001IG(:,2)
% and add using the interative menu Insert>TextBox. Or try to add using text(x,y, out0001IG(1,2)), text(x,y, out0001IG(2,2)) etc

hold off;

%relabeling dendrogram
aux = dataCodasG;
auxYear = datestr(aux(:,2), 'yy');
auxLab = [str2num(auxYear)*100+ aux(:,3)];
aux(:,3) = auxLab;
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(auxLab);
rejlab = 9;
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)







%% 3. Repertoire Similarity with Continuous metrics: Euclidean Distances


% 3.1. Encounters
simAbsEucEnc = daysim(dataCodasE,1,16,0);
[simsoutEE, z0001bootEE, z001bootEE, z01bootEE, z1bootEE,out0001EE,out001EE,out01EE,out1EE] = daysim_boot_mc(dataCodasE,100,1,9,5,0,1);
zEE = linkage_sim(simAbsEucEnc(:,3)'); 
dendrogram_sim(zEE,0);
cccEE = cophenet(zEE, simAbsEucEnc(:,3)')
title(cccEE)
ylabel('Euclidean distance');
xlabel('Encounters')

% 3.2. Days
simAbsEucDay = daysim(dataCodasD,1,11,0);
[simsoutED, z0001bootED, z001bootED, z01bootED, z1bootED,out0001ED,out001ED,out01ED,out1ED] = daysim_boot_mc(dataCodasD,100,1,7,5,0,1);
zED = linkage_sim(simAbsEucDay(:,3)'); 
dendrogram_sim(zED,0);
cccED = cophenet(zED, simAbsEucDay(:,3)')
title(cccED)
ylabel('Euclidean distance');
xlabel('Days')

% 3.3. Groups
simAbsEucGro = daysim(dataCodasG,1,25,0);
[simsoutEG, z0001bootEG, z001bootEG, z01bootEG, z1bootEG,out0001EG,out001EG,out01EG,out1EG] = ...
    daysim_boot_mc(dataCodasG,100,1,25,10,0,1);
zEG = linkage_sim(simAbsEucGro(:,3)'); 
dendrogram_sim(zEG,0);
cccEG = cophenet(zEG, simAbsEucGro(:,3)')
title(cccEG)
ylabel('Euclidean distance');
xlabel('Groups')

%relabeling dendrogram
aux = dataCodasG;
auxYear = datestr(aux(:,2), 'yy');
auxLab = [str2num(auxYear)*100+ aux(:,3)];
aux(:,3) = auxLab;
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(auxLab);
rejlab = 9;
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)








%% 4. Clusplots based on Bootstrap branches

% 4.1. Infinity-norm, Encounter, cluster=branch with max replicates
% extracting the dendrogram branch membership from bootstrapped results
% Picking up the mostly supported branch level (level=0)
[dataClusplotIE membIE breplicIE] = extrMembDendBoot(dataCodasE,out0001IE,0);
% Exporting data
dlmwrite([outputpath '\dataClusplotIE.csv'], dataClusplotIE,'delimiter',',','precision','%.6f')
% Clusplots with absolute ICIc up to 10-click codas
clusplot(dataClusplotIE,2,1,1)
% All codas
clusplot(dataClusplotIE,2,2,1)

% 4.2. Infinity-norm, Day, cluster=branch with max replicates
[dataClusplotID membID breplicID] = extrMembDendBoot(dataCodasD,out0001ID,0);
dlmwrite([outputpath '\dataClusplotID.csv'], dataClusplotID,'delimiter',',','precision','%.6f')
clusplot(dataClusplotID,2,1,1)
clusplot(dataClusplotID,2,2,1)

% 4.3. Infinity-norm, Group, cluster=branch with max replicates
[dataClusplotIG membIG breplicIG] = extrMembDendBoot(dataCodasG,out0001IG,0);
dlmwrite([outputpath '\dataClusplotIG.csv'], dataClusplotIG,'delimiter',',','precision','%.6f')
clusplot(dataClusplotIG,2,1,1)
clusplot(dataClusplotIG,2,2,1)

% 4.4. Euclidean, Encounter, cluster=branch with max replicates
[dataClusplotEE membEE breplicEE] = extrMembDendBoot(dataCodasE,out0001EE,0);
dlmwrite([outputpath '\dataClusplotEE.csv'], dataClusplotEE,'delimiter',',','precision','%.6f')
clusplot(dataClusplotEE,2,1,1)
clusplot(dataClusplotEE,2,2,1)

% 4.5. Euclidean, Day, cluster=branch with max replicates
[dataClusplotED membED breplicED] = extrMembDendBoot(dataCodasD,out0001ED,0);
dlmwrite([outputpath '\dataClusplotED.csv'], dataClusplotED,'delimiter',',','precision','%.6f')
clusplot(dataClusplotED,2,1,1)
clusplot(dataClusplotED,2,2,1)

% 4.6. Euclidean, Group, cluster=branch with max replicates
[dataClusplotEG membEG breplicEG] = extrMembDendBoot(dataCodasG,out0001EG,0);
dlmwrite([outputpath '\dataClusplotEG.csv'], dataClusplotEG,'delimiter',',','precision','%.6f')
clusplot(dataClusplotEG,2,1,1)
clusplot(dataClusplotEG,2,2,1)








%% 5. Mantel tests for group repertoires


%To test if recordings from different days of the same group represent different repertoires
%A Mantel permutation test will compare all the measures of similarity among
%daily repertoires of a given group and among repertoires of different
%groups to get a global measure of similarity within groups and between groups: 
%because there could be variations due to sampling. 
%Each day repertoire (pooling all recordings of that day) is considered an independent sample
%to avoid potential autocorrelation of coda production in that day.
%For each group, we need a replicate (more than one day)
%The Mantel test will account for heterogeneity in sampling within/between groups, as 
%it gives a sense of self similarity

%We tested whether pair-wise similarities were higher between two days? recordings of the same group 
%(same group, different day SGDD) compared with two days? recordings of different groups (different 
%groups, different days DGDD). Each day?s recordings were treated as independent in an attempt to 
%account for any autocorrelation in coda production within a recording day. To do so, we tested the 
%matrix of pair-wise similarities of each day?s recordings against a 0/1 matrix with 1 coding for SGDD
%and 0 coding for DGDD. If groups produced distinct repertoires, then the expectation is a significantly
%positive correlation between these matrices.

% Create the binary matrix SGDD=1, DGDD=0  
   
% First, sort the repertoires by group then day, then the repertoire numbers are cumulative
% should look like:
%Group   Day       Daily repertoire (sequential)
%GrpA     1            1
%GrpA     2            2
%GrpB     1            3
%GrpB     2            4
%GrpB     3            5
%Grp C    1            6
datalabel = [dataGroups(:,2) dataGroups(:,1)];
datalabel = sortrows(datalabel);
% Removing excluded groups from the other analysis: group 9
datalabel(find(datalabel(:,1)==9), :) = [];
datalabel = [datalabel (1:size(datalabel,1))'];


% Getting the similarity results by day (for b=0.001) and transforming it into a matrix
% But first, remove excluded groups from the other analysis: group 9
%Get the day of group 9
rmday = floor(dataGroups(find(dataGroups(:,2)==9), 1));
%Get group 9 similarities and remove them
matsimAbsEucDay = simAbsEucDay;
matsimAbsEucDay(find(matsimAbsEucDay(:,1)==rmday),:) = [];
matsimAbsEucDay(find(matsimAbsEucDay(:,2)==rmday),:) = [];
%Making the similarity matrix from the simils output
matsimAbsEucDay = simmatrix(matsimAbsEucDay);


% The Binary matrix will look like a bunch of large boxes of 1s around the diagonal 
%To do that, create a big nxn matrix of zeros (n is total number of daily repertoires)
%then fill it with multiple small mxm matrices of 1s by using the m number of repertoires (days) for 
% each group landmarked in the larger nxn matrix by the repertoire number between groups.
%putting small matrices inside of the big one:

n = size(unique(datalabel(:,2)), 1);
big = zeros(n);
%get sizes
[rowsBig, columnsBig] = size(big);

for i=1:n
    %Reps = values of daily repertoires for each group
    Reps = datalabel(find(datalabel(:,1)==i), 3);
    %Rlength= Get length of reps for each group
    Rlength = size(Reps, 1);
    %Build matrix of 1s of size Rlength
    small = ones(Rlength);
    %get sizes
    [rowsSmall, columnsSmall] = size(small);
    % Specify upper left row, column of where we'd like to paste the small matrix.
    row1 = min(Reps); column1 = min(Reps);
    % Determine lower right location.
    row2 = row1 + rowsSmall - 1; column2 = column1 + columnsSmall - 1;
    %paste it.
	big(row1:row2, column1:column2) = small;
end




% mantel
[c pval] = bramila_mantel(matsimAbsEucDay, big, 1000, 'pearson')
mantel1(matsimAbsEucDay, big, 1000)
