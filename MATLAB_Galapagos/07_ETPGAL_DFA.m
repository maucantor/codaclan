%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014 Acoustic Analysis
% Mauricio Cantor, Dalhousie University, June 2015
%
% CATEGORICAL ANALYSES.
% Discriminant Function Analysis: Using ETP data to classify GAL1314 codas
% NOTE: IT ASSUMES THAT GAL WILL HAVE NO NOISE...
%
% Summary:
% 1. Data input
% 2. Data preparation for DFA
% 3. Linear Discriminant Analysis
% 4. Visualizing LDA on 2D (using PCA on ICI)
% 5. Quadratic Discriminant Analysis
% 6. Visualizing QDA on 2D (using PCA on ICI)
% 7. 2D Plot Overlaps of original clans and new GAL1314 data
%
%%%




%% 1. Loadings and Data Organization

% Set input paths:
%Please set your path to the working directory manually
%%% NOTE: If working in Windows do CTRL+F to replace all "/" with "\"! %%%
addpath('Coda_K-means')
addpath('Coda_multivariate')
addpath('GroupSizesPetersen')
addpath('data')
addpath('MATLAB_Galapagos')
addpath('_Cachalote_coredata')
addpath('data/ETP_dataCodas')

% Set output paths
outputpath = [pwd, '/data'];
outputOptics2 = [pwd, '/OPTICSxi_Elki/data_output'];


% Data input:

%%%% 1.1. ETP DATASET %%%%

% ETP Data with repertoires labeled by Group, defined by photoID
%(codas recorded in the same day = group; codas recorded on different days were in
%the same group if more than 25% of the individuals were present in both)
%and coda types defined by OPTICS, under three parameters schemes:

%low xi, high minpts
load('dataCodaOpticsETPG_lxhm.csv');
%mid xi, mid minpts
load('dataCodaOpticsETPG_mxmm.csv');
%high xi, low minpts
load('dataCodaOpticsETPG_hxlm.csv');

%removing noise coda types (#99)
dataCodaOpticsETPG_lxhmN = rmNoise(dataCodaOpticsETPG_lxhm);
dataCodaOpticsETPG_mxmmN = rmNoise(dataCodaOpticsETPG_mxmm);
dataCodaOpticsETPG_hxlmN = rmNoise(dataCodaOpticsETPG_hxlm);




%%%% 1.2. GAL1314 DATASET %%%%

% GAL1314 DataCodas with repertoires organized by photoID groups
dataCodasG = load('dataCodasG.csv');







%% 2. Preparing data for DFA


%%2.2. OPTICS mid xi, mid minpts%%

% 1. Preparing data per coda length (3- to 12-click codas): ETP N-click ICIs and types
%ETP
etpici = {};
etptype = {};
for c=3:12
    %raw data per coda length, without noise
    auxdata = dataCodaOpticsETPG_mxmmN(dataCodaOpticsETPG_mxmmN(:,4)==c, :);
    %ICIs
    etpici{c-2} = auxdata(:, 6:(6+(c-2)));
    etptype{c-2} = auxdata(:, 46);
end

%GAL1314
galici = {};
for c=3:12
    %raw data per coda length
    auxdata = dataCodasG(dataCodasG(:,4)==c, :);
    %ICIs
    galici{c-2} = auxdata(:, 6:(6+(c-2)));
end





%%2.1. OPTICS low xi, high minpts%%
%%2.3. OPTICS high xi, low minpts%%






%% 3. Linear Discriminant Analysis Classifier


%%3.2. OPTICS mid xi, mid minpts%%


% 3.2.1. Create linear discriminant analysis to classify GAL samples into 
%coda types of ETP
ldaclass = {};
for c=3:12
    % Create a default LDA classifier
    ldaclass{c-2} = fitcdiscr(etpici{c-2}, etptype{c-2});
end


% 3.2.2. GAL->ETP: Classify the GAL1314 ICIs according to the ETP coda types given
% by OPTICS, using a LINEAR Discriminant Function
galtypelda = {};
dataCodasGlda = [];
for c=3:12
    % Predicting the coda type
    galtypelda{c-2} = predict(ldaclass{c-2}, galici{c-2});
    
    % Sticking it into the original GAL dataCoda data set
    auxdata = dataCodasG(dataCodasG(:,4)==c, :);
    auxdata = [auxdata galtypelda{c-2}(:,1)];
    
    dataCodasGlda = [dataCodasGlda; auxdata];
end


% 3.2.3. Adding the long codas, with >=12-click: assign a single coda type for each
% coda size, e.g. 131, 141, 151 (13-click coda type 1, 14-click coda type 1...) 
longCodas = dataCodasG(dataCodasG(:,4)>=13, :);
nCodalength = unique(dataCodasG(:,4));
longCodasSizes = nCodalength(nCodalength>=13)';
% assigning coda types
for c=longCodasSizes
    longCodas(longCodas(:,4)==c,46)=str2num(strcat(num2str(c),num2str(1)));
end
%Append longCodas to dataCodasGlda and sort by Date:
dataCodasGlda = [dataCodasGlda; longCodas];
dataCodasGlda = sortrows(dataCodasGlda,2);


% 3.2.4. Appending the new GAL1314 with codatypes to the ETP data, sorting by
% coda number
dataCodasOpticsAllLda_mxmmN = [dataCodaOpticsETPG_mxmmN; dataCodasGlda];
dataCodasOpticsAllLda_mxmmN = sortrows(dataCodasOpticsAllLda_mxmmN,1);


% 3.2.5. FINAL files with categorical coda types by OPTICs: save and export
dlmwrite([outputpath '/dataCodasOpticsAllLda_mxmmN.csv'], dataCodasOpticsAllLda_mxmmN,'delimiter',',','precision','%.6f');
dlmwrite([outputOptics2 '/dataCodasOpticsAllLda_mxmmN.csv'], dataCodasOpticsAllLda_mxmmN,'delimiter',',','precision','%.6f');








%%3.1. OPTICS low xi, high minpts%%
%%3.3. OPTICS high xi, low minpts%%








%% 4. Visualize the classification boundaries of a 2-D linear classification of the data

% Use the first 2 PCs for display only! The real classification is based on
% the raw ICIs!!


%%4.2. OPTICS mid xi, mid minpts%%

% 4.2.1. Getting the PCA scores on ICIs for ETP and GAL independently
etppca = {};
galpca = {};
for c=3:12
    [aux etppca{c-2}] = pca(etpici{c-2});
    [aux galpca{c-2}] = pca(galici{c-2});    
end

% Selecting the first 2 PCs
etppcs = {};
galpcs = {};
for c=3:12
    etppcs{c-2} = etppca{c-2}(:, 1:2);
    galpcs{c-2} = galpca{c-2}(:, 1:2);
end

% 4.2.2. Create linear discriminant analysis on PCs and classify coda type with PCs!
ldapcs = {};
for c=3:12
    % Create a default LDA classifier
    ldapcs{c-2} = fitcdiscr(etppcs{c-2}, etptype{c-2});
end

% 4.2.3. Plotting
for c=3:12
    plotDFA(etppcs{c-2}, etptype{c-2}, ldapcs{c-2}, 'linear', galpcs{c-2});
end






%%4.1. OPTICS low xi, high minpts%%
%%4.3. OPTICS high xi, low minpts%%





%% 5. Quadratic Discriminant Function Analysis


%%5.2. OPTICS mid xi, mid minpts%%

% 5.2.1. Create quadratic discriminant analysis to classify GAL samples into 
%coda types of ETP
qdaclass = {};
for c=3:12
    % Create a default LDA classifier
    qdaclass{c-2} = fitcdiscr(etpici{c-2}, etptype{c-2}, 'discrimType', 'quadratic');
end


% 5.2.2. GAL->ETP: Classify the GAL1314 ICIs according to the ETP coda types given
% by OPTICS, using a QUADRATIC Discriminant Function
galtypeqda = {};
for c=3:12
    galtypeqda{c-2} = predict(qdaclass{c-2}, galici{c-2});
end

% 5.2.3. Sticking it into the original GAL dataCoda data set
dataCodasGqda = [];
for c=3:12
    auxdata = dataCodasG(dataCodasG(:,4)==c, :);
    auxdata = [auxdata galtypeqda{c-2}(:,1)];    
    dataCodasGqda = [dataCodasGqda; auxdata];
end


% 5.2.4. Adding the long codas, with >=12-click: assign a single coda type for each
% coda size, e.g. 131, 141, 151 (13-click coda type 1, 14-click coda type 1...) 
%Append longCodas to dataCodasGlda and sort by Date:
dataCodasGqda = [dataCodasGqda; longCodas];
dataCodasGqda = sortrows(dataCodasGqda,2);


% 5.2.5. Appending the new GAL1314 with codatypes to the ETP data, sorting by
% coda number
dataCodasOpticsAllQda_mxmmN = [dataCodaOpticsETPG_mxmmN; dataCodasGqda];
dataCodasOpticsAllQda_mxmmN = sortrows(dataCodasOpticsAllQda_mxmmN,1);


% 5.2.6. FINAL files with categorical coda types by OPTICs: save and export
dlmwrite([outputpath '/dataCodasOpticsAllQda_mxmmN.csv'], dataCodasOpticsAllQda_mxmmN,'delimiter',',','precision','%.6f');
dlmwrite([outputOptics2 '/dataCodasOpticsAllQda_mxmmN.csv'], dataCodasOpticsAllQda_mxmmN,'delimiter',',','precision','%.6f');





%%5.1. OPTICS low xi, high minpts%%
%%5.3. OPTICS high xi, low minpts%%





%% 6. Visualize the classification boundaries of a 2-D quadratic classification of the data

% Use the first 2 PCs for display only! The real classification is based on
% the raw ICIs!!

%%6.1. OPTICS low xi, high minpts%%

% 6.1.1. Create Quadratic discriminant analysis on PCs and classify coda type with PCs!
qdapcs = {};
for c=3:12
    % Create a default LDA classifier
    qdapcs{c-2} = fitcdiscr(etppcs{c-2}, etptype{c-2}, 'discrimType', 'quadratic');
end

% 6.1.2. Plotting
for c=3:12
    plotDFA(etppcs{c-2}, etptype{c-2}, qdapcs{c-2}, 'quadratic', galpcs{c-2});
end




%%6.1. OPTICS low xi, high minpts%%
%%6.3. OPTICS high xi, low minpts%%











%% 7. Overlapping 2D plots: original clans and PCs on new Data


% Retrieve the original data: ETP ad groups
dataCodasETP = load([pwd, '/data/ETP_dataCodas/allcodas_with_types.mat']);
dataGroupETP = load([pwd, '/data/ETP_dataCodas/group_assignments.mat']);

dataCodasETPG = dataCodasETP.codas;
dataCodasETPG(:,3) = dataGroupETP.group_assignments(:,3);

dataCodasETPGclan = dataCodasETPG;

% clan data
groupclan = [group_data(:,1) group_data(:,8)];
% relating groups with clans
newclan = zeros(size(dataCodasETPG(:,46),1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==dataCodasETPG(:,3)) = groupclan(g,2);
end
% adding the original clan classification in the dataCodas data
dataCodasETPGclan(:,47) = newclan;

% Splitting  the data into coda lengths and getting their clan classification
etpici = {};
etpclan = {};
for c=3:12
    %raw data per coda length, without noise
    auxdata = dataCodasETPGclan(dataCodasETPGclan(:,4)==c, :);
    %ICIs
    etpici{c-2} = auxdata(:, 6:(6+(c-2)));
    etpclan{c-2} = auxdata(:, 47);
end


% Getting the PCA scores on ICIs for ETP and GAL independently
etppca = {};
galpca = {};
for c=3:12
    [aux etppca{c-2}] = pca(etpici{c-2});
    [aux galpca{c-2}] = pca(galici{c-2});    
end

% Selecting the first 2 PCs
etppcs = {};
galpcs = {};
for c=3:12
    etppcs{c-2} = etppca{c-2}(:, 1:2);
    galpcs{c-2} = galpca{c-2}(:, 1:2);
end


% Plotting 2 PCs for ETP with original clan classification and overlapping
% the 2 PCs of the new GAL1314 data
for c=3:12
    figure
    gscatter(etppcs{c-2}(:,1), etppcs{c-2}(:,2), etpclan{c-2}, '', '', 10);
    hold on;
    scatter(galpcs{c-2}(:,1), galpcs{c-2}(:,2));
    legend('Location', 'Best');
    hold off;
end

