%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014 Acoustic Analysis
% Mauricio Cantor, St Andrews University, Scotland, Jan 2015
%
% INITIAL SETUP:
% Summary:
% 1. Loadings
% 2. Extracting inter-click intervals from clk files and save as csv files
% 3. Organize individual csv files and export final data
% 4. Define repertoires labels (encounter, day, group)
% 5. Cleaning up data set: remove codas with >10 and <3 clicks
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1. Loadings 

% Set input paths
% Please set your path to the working directory manually
addpath('Coda_K-means')
addpath('Coda_multivariate')
addpath('GroupSizesPetersen')
addpath('data')
addpath('MATLAB_Galapagos')
addpath('_Cachalote_coredata')

% Input metadata (a single-sheet 2003 xls file with raw data on click files)
% if using Windows, use xls file:
%metadataFile2013 = [pwd,'/_Cachalote_coredata/Galapagos_2013_codas_RainbowClick/Galapagos_2013_clickfiles_rawdata.xls'];
%metadataFile2014 = [pwd,'/_Cachalote_coredata/Galapagos_2014_RainbowClick/Galapagos_2014_clickfiles_rawdata.xls'];
% if using Mac, it's better to choose xlsx file:
metadataFile2013 = [pwd, '/_Cachalote_coredata/Galapagos_2013_codas_RainbowClick/Galapagos_2013_clickfiles_rawdata.xlsx'];
metadataFile2014 = [pwd,'/_Cachalote_coredata/Galapagos_2014_RainbowClick/Galapagos_2014_clickfiles_rawdata.xlsx'];

% Input path to the raw rainbowclick .clk files
% NOTE: Currently, this will be retrieved from the external Hard Drive. 
% Adjust path accordingly.
pathToClickFiles2013 = '/Volumes/MAUCANTOR1/Cachalote/Galapagos_2013_codas_RainbowClick/';
pathToClickFiles2014 = '/Volumes/MAUCANTOR1/Cachalote/Galapagos_2014_RainbowClick/';
%pathToClickFiles2013 = [pwd,'/data'];
%pathToClickFiles2014 = [pwd,'/data'];

% Input photoID data
dataPhotoID = load('dataPhotoidAll.csv');
sexPhotoID = load('dataPhotoidSex.csv'); %male=0, female,juvenile=1

% Set output paths
outputpath = [pwd, '/data'];


%% 2. Extracting the inter-click intervals (ICI)

% Summary:
% a. Read all clk files in a folder
% b. Use 'makeCodaFile.m' function to extract ICIs from clk files
% c. save individual csv files with coda data

% Function 'processAllClickFiles' does all of that
% 2013 clk files
processAllClickFiles(metadataFile2013, pathToClickFiles2013)

% 2014 clk files
processAllClickFiles(metadataFile2014, pathToClickFiles2014)



%% 3. Combining ICI data and exporting repertoires

% Summary: 
% a. Select all csv files from same clk file folder (produced in step 2)
% b. Combine all csv files
% c. export data into csv and mat format:
    % The final output file contains the following data:
    %column 1: sequential coda number within file
    %column 2: date and time in numerical format
    %column 3: Repertoire label, the unit of analysis in the similarity. See '4' below 
    %column 4: number of clicks in the coda
    %remaining columns: interclick intervals (ICIs) in seconds
% d. adjust col1: coda sequential number
    
% Selecting all 2013 individual csv files in
%~\_Cachalote_coredata\Galapagos_2013_codas_RainbowClick
combineCsv('dataCodas2013', outputpath);

% Selecting all 2014 individual csv files in
%~\_Cachalote_coredata\Galapagos_2014_RainbowClick
combineCsv('dataCodas2014', outputpath);

% Making the final file:
%Selecting these two csv files for 2013 and 2014 in the working directory:
%~\data
dataCodasAll = combineCsv('dataCodasAll', outputpath);
% adjusting the chronological order and coda sequential number, from 1 to max number of codas
dataCodasAll = sortrows(dataCodasAll,2);
dataCodasAll(:,1) = 1:size(dataCodasAll,1);
% exporting again:
dlmwrite([outputpath '/dataCodasAll.csv'], dataCodasAll,'delimiter',',','precision','%.6f');

% all files were save to the data folder
outputpath

    
%% 4. Defining Repertoires

% Summary:
% Repertoire label is in the 3rd column of the coda data file.
% This is the unit of analysis in the similarity and will be defined as of the following:
% a. Encounter: compare repertoires of encounters with multiple days. OR
% b. Day: repertoire composed by all recordings on a given days
% c. Group: group based on photoid data (splits encounters based on
    %recapture rate between days)

% Repertoires by encounters:
% Input final csv file with all codas and repertoires organized by encounter
dataCodasE = dataCodasAll;


% Repertoires by day:
% Change 3rd column to receive the date in mmddyy format
dataCodasD = dataCodasE;
dataCodasD(:,3) = floor(dataCodasE(:, 2));


% Repertoires by group based on photoid data:
%Input all data
dataCodasG = dataCodasE;

%Defining groups based on photoid data, loaded from /data/ folder in step 1.
%1.round numeric date to the day
dataPhotoID(:,1) = floor(dataPhotoID(:,1));
%round numeric date to the day to match
dataCodasG(:,3)= floor(dataCodasE(:, 2)); 

%2.selecting only females and juveniles (1), removing males (0)
%creates a sexvector and adds to photoID data
sexVector = zeros(size(dataPhotoID,1),1);
for s = 1:size(sexPhotoID,1)               
    sexVector(dataPhotoID(:,2)==sexPhotoID(s,1)) = sexPhotoID(s,2);
end
dataPhotoID(:,3)=sexVector;
%removes males
dataPhotoID = dataPhotoID(dataPhotoID(:,3)==1, 1:2);

%defining the groups using the 25% rule in Whitehead et al. 2008
dataGroups = group(dataPhotoID, dataCodasG(:,[1 3]), 0.25)
%checking dates and group data
grouplabels = [datestr(dataGroups(:,1)) repmat(' -> ',size(dataGroups,1),1) num2str(dataGroups(:,2))]
%adding group data to the dataGroupsG third column: creates empty vector and fills it up with respective groups
groupVector = zeros(size(dataCodasG,1),1);
for g = 1:size(dataGroups,1)               
    groupVector(dataCodasG(:,3)==dataGroups(g,1)) = dataGroups(g,2);
end
dataCodasG(:,3) = groupVector;



%% 5. Cleaning up and Exporting the data

% Summary
% Removing possible codas with less than 3 clicks and codas that could be
% creacks (here, defined as those with higher than 15 clicks).
% Export csv files


%Setting the thresholds to remove codas: lower than 3, higher than 15 clicks
lowIndex = dataCodasE(:,4) <3;
highIndex = dataCodasE(:,4) >15;

% Encounter
dataCodasE(lowIndex,:)=[]; dataCodasE(highIndex,:)=[];
dlmwrite([outputpath '/dataCodasE.csv'], dataCodasE,'delimiter',',','precision','%.6f')

% Day
dataCodasD(lowIndex,:)=[]; dataCodasD(highIndex,:)=[];
dlmwrite([outputpath '/dataCodasD.csv'], dataCodasD,'delimiter',',','precision','%.6f')

% Group
dataCodasG(lowIndex,:)=[]; dataCodasG(highIndex,:)=[];
dlmwrite([outputpath '/dataCodasG.csv'], dataCodasG,'delimiter',',','precision','%.6f')