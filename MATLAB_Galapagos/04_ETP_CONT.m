%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014 Acoustic Analysis
% Mauricio Cantor, Dalhousie University, May 2015
%
% Eastern Tropical Pacific (ETP) and new Galapagos (GAL1314) data:
% Summary:
% 1. Loadings
% 2. Repertoire comparison (for groups) with continuous metrics: Infinity-norm 
% 3. Repertoire comparison (for groups) with continuous metrics: Euclidean distance
% 4. Mantel tests for group repertoires
% 5. Assigning groups to clans, dates to clans
%
%%% TO DO %%%
% Clusplots ETPGAL data
%%%

%% 1. Loadings 

% Set input paths:
%Please set your path to the working directory manually
%%% NOTE: If working in Windows do CTRL+F to replace all "/" with "\"! %%%
addpath('Coda_K-means')
addpath('Coda_multivariate')
addpath('GroupSizesPetersen')
addpath('data')
addpath('MATLAB_Galapagos')
addpath('_Cachalote_coredata')
addpath('data/ETP_dataCodas')

% Set output paths
outputpath = [pwd, '/data'];

% Data input:

%%%% ETP DATASET %%%%

%%a. dataCodasETP.codas: coda standard data; dataCodasETP.codas_cols: column name
%for coda data; dataCodasETP.clusternames: coda type labels from k-means.
%dataCodasETP.clanNames: clan names in relation to their numerical labels cols#)
dataCodasETP = load([pwd, '/data/ETP_dataCodas/allcodas_with_types.mat']);

%%b. dataGroupETP.group_assignments: Group assignments = coda#, recording date, group number
%after using groups.m on ids and group_assignments(:,1:2); made groups from
%all q>=3 ids using nab>0.25(min(na,nb)); dataGroupETP.group_date_assignments: 
%date (numerical format) for each photoid group; dataGroupETP.ids: date
%(numerical format) and IDs of members of each photoID group
dataGroupETP = load([pwd, '/data/ETP_dataCodas/group_assignments.mat']);

%%c. group_data: group id, number of codas, %<9 clicks, first day recorded, lat,
%long, # stars for fig, clan number for seven clans (see dataCodasETP.clanNames)
%1/0 genetic data available.
%To find data for a group enter group_data with row#=dendrogram node#
load([pwd, '/data/ETP_dataCodas/groupData.mat']);

%%d. unit_assignments: ETP ID and unit label
load([pwd, '/data/ETP_dataCodas/unit_assignments.mat']);

%%e. ***NOT SURE: but it includes most of all objects loaded in a-d steps
%plus other variables. ***CHECK WITH LUKE
load([pwd, '/data/ETP_dataCodas/groups.mat']);


% Data organization:
% ETP Data with repertoires labeled by Day
dataCodasETPD = dataCodasETP.codas;

% ETP Data with repertoires labeled by Group, defined by photoID
%(codas recorded in the same day = group; codas recorded on different days were in
%the same group if more than 25% of the individuals were present in both)
dataCodasETPG = dataCodasETP.codas;
dataCodasETPG(:,3) = dataGroupETP.group_assignments(:,3);

% Removing undersampled LONG codas (>= 13 clicks)
dataCodasETPGs = dataCodasETPG;
longcodas = dataCodasETPGs(:,4)>=13;
dataCodasETPGs(longcodas,:) = [];



%%%% GAL1314 DATASET %%%%

% Input GAL1314 DataCodas with repertoires organized by photoID groups
dataCodasG = load('dataCodasG.csv');

% Removing undersampled LONG codas (>= 13 clicks)
dataCodasGs = dataCodasG;
longcodas = dataCodasGs(:,4)>=13;
dataCodasGs(longcodas,:) = [];



%%%% ALL DATASET %%%%

% ALL Dataset: ETP + GAL1314, labeled by photoID groups
%Relabelling GAL1314 photoID groups to be different than the ETP. These
%will be 'yy##' (yy=year (13,14))
aux = dataCodasG;
auxYear = datestr(aux(:,2), 'yy');
auxLab = [str2num(auxYear)*100+ aux(:,3)];
aux(:,3) = auxLab;
% combining the datasets (note that we removed the ETP 46th col with kmeans
dataCodasAllG = [dataCodasETPG(:,1:45); aux];




%%%%% Removing undersampled LONG codas (>= 13 clicks)
dataCodasAllGs = dataCodasAllG;
longcodas = dataCodasAllGs(:,4)>=13;
dataCodasAllGs(longcodas,:) = [];







%% 2. Repertoire Similarity with Continuous metrics: Infinity-norm

% Summary:
% Replicate Fig2 (Rendell & Whitehead 2003), add new Galapagos data to it:
    % ETP data, Dendrogram with Infinity, photoID groups, b=0.001 bootstrap
    % Run the same for GAL2013-14 data with photoID groups
    % Combine ETP+GAL1314 and see in which branch new data falls into
    
% 1. Calculate repertoire similarity with continuous metric (Infinity-norm)
% 2. Bootstrap repertoires to replicate branches
% 3. Plot dendrograms + CCC
% 4. Adjust dendrogram branch names to original photoid group names
% 5. Link photoid group name in ETP data to their clan names
% 6. Figure out to which clan the new GAL1314 data belongs to


%%% 2.1 ETP data, Infinity-norm similarity, photoID groups, b=0.001
simAbsInfETPG = daysim(dataCodasETPG,2,25,0);
[simsoutIETPG, z0001bootIETPG, z001bootIETPG, z01bootIETPG, z1bootIETPG,out0001IETPG,out001IETPG,out01IETPG,out1IETPG] = daysim_boot_mc(dataCodasETPG,100,2,25,5,0,1);
zIETPG = linkage_sim(simAbsInfETPG(:,6)'); 
dendrogram_sim(zIETPG,0);
cophenet(zIETPG, simAbsInfETPG(:,3)')

% Relabeling branches
% New branch labels in the dendrogram (64 out of 73 groups, i.e. 9 rejected < 25 codas)
%newlab = [35 36 34 49 52 57  9 30 55 62 32 61 28 29 54 31 38 40 42 43 47 48  1 10 23 16 53 51 11  2 56 60  3  8  7  4 22 12  5  6 18 27 17 15 24 25 19 63 50 13 26 14 20 21 46 33 39 37 44 45 41 58 59 64]
newlab = str2num(get(gca,'xticklabel'));
% original labels
orilab = unique(dataCodasETPG(:,3));
% rejected 9 groups (<25 codas)
rejlab = [7 11 14 16 17 24 27 32 68];
% original labels - rejected groups
orilab(rejlab) = [];
% correspondence
corlab = [sort(orilab) sort(newlab)];
% replacing new by original labels
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)


%%% Relating ETP photoid group names with clan names
% 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(65:end) zeros(size(orilab(65:end),1),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zIETPG,0);
set(gca,'xticklabel',newclan)




%%% 2.2 GAL1314 data, Infinity-norm similarity, photoID groups, b=0.001
simAbsInfGro = daysim(dataCodasG,2,25,0);
[simsoutIG, z0001bootIG, z001bootIG, z01bootIG, z1bootIG,out0001IG,out001IG,out01IG,out1IG] = daysim_boot_mc(dataCodasG,100,2,25,5,0,1);
zIG = linkage_sim(simAbsInfGro(:,3)');
dendrogram_sim(zIG,0);
cophenet(zIG, simAbsInfGro(:,3)')

% Relabeling dendrogram branches
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasG(:,3));
%for 25% rule photoid groups
rejlab = [9];
%for 15% rule photoid groups
%rejlab = [5 9];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)
% in 01_GAL1314_INIT:
grouplabels




%%% 2.3.1 ALL data, Infinity-norm similarity, photoID groups, b=0.001
simAbsInfAll = daysim(dataCodasAllG,2,25,0);
[simsoutIAllG, z0001bootIAllG, z001bootIAllG, z01bootIAllG, z1bootIAllG,out0001IAllG,out001IAllG,out01IAllG,out1IAllG] = daysim_boot_mc(dataCodasAllG,100,2,25,5,0,1);
zIAllG = linkage_sim(simAbsInfAll(:,3)');
dendrogram_sim(zIAllG,0);
cophenet(zIAllG, simAbsInfAll(:,3)')

% changing dendrogram labels
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllG(:,3));
rejlab = [7 11 14 16 17 24 27 32 68 (max(dataCodasETPG(:,3))+9)];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% group names are in group_data col1, clan numbers in col8
% clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(65:end) zeros(size(orilab(65:end),1),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zIAllG,0);
set(gca,'xticklabel',newclan)





%%% 2.3.2 ALL data MINUS Caribbean, Infinity-norm similarity, photoID groups, b=0.001
dataCodasAllC = dataCodasAllG;
carib1 = dataCodasAllC(:,3)==41; % caribbean groups = 41
dataCodasAllC(carib1,:) = []; %remove
carib2 = dataCodasAllC(:,3)==58; % caribbean groups = 58
dataCodasAllC(carib2,:) = []; %remove

simAbsInfAllC = daysim(dataCodasAllC,2,25,0);
%[simsoutIAllG, z0001bootIAllG, z001bootIAllG, z01bootIAllG, z1bootIAllG,out0001IAllG,out001IAllG,out01IAllG,out1IAllG] = daysim_boot_mc(dataCodasAllG,100,2,25,5,0,1);
zIAllC = linkage_sim(simAbsInfAllC(:,3)');
dendrogram_sim(zIAllC,0);
cophenet(zIAllC, simAbsInfAllC(:,3)')

newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllC(:,3));
rejlab = [7 11 14 16 17 24 27 32 67 80];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zIAllC,0);
set(gca,'xticklabel',newclan)






%%% 2.3.3 ALL data MINUS Tonga+Caribbean, Infinity-norm similarity, photoID groups, b=0.001
dataCodasAllM = dataCodasAllG;
tonga = dataCodasAllM(:,3)==47; %tonga groups = 47
dataCodasAllM(tonga,:) = []; %remove
carib1 = dataCodasAllM(:,3)==41; % caribbean groups = 41
dataCodasAllM(carib1,:) = []; %remove
carib2 = dataCodasAllM(:,3)==58; % caribbean groups = 58
dataCodasAllM(carib2,:) = []; %remove

simAbsInfAllM = daysim(dataCodasAllM,2,25,0);
%[simsoutIAllG, z0001bootIAllG, z001bootIAllG, z01bootIAllG, z1bootIAllG,out0001IAllG,out001IAllG,out01IAllG,out1IAllG] = daysim_boot_mc(dataCodasAllG,100,2,25,5,0,1);
zIAllM = linkage_sim(simAbsInfAllM(:,3)');
dendrogram_sim(zIAllM,0);
cophenet(zIAllM, simAbsInfAllM(:,3)')

newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllM(:,3));
rejlab = [7 11 14 16 17 24 27 32 66 79];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zIAllM,0);
set(gca,'xticklabel',newclan)








%% 3. Repertoire Similarity with Continuous metrics: Euclidean Distances

%%% 3.1. ALL data , Euclidean similarity, photoID groups, b=0.001
[simAbsEucAll rejlab] = daysim(dataCodasAllG,1,25,0);
[simsoutEAllG, z0001bootEAllG, z001bootEAllG, z01bootEAllG, z1bootEAllG,out0001EAllG,...
    out001EAllG,out01EAllG,out1EAllG] = daysim_boot_mc(dataCodasAllG,100,1,25,15,0,1);
zEAllG = linkage_sim(simAbsEucAll(:,3)');
dendrogram_sim(zEAllG,0);
cophenet(zEAllG, simAbsEucAll(:,3)')

% changing dendrogram labels
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllG(:,3));
rejlab = [7 11 14 16 17 24 27 32 68 (max(dataCodasETPG(:,3))+9)];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(65:end) zeros(size(orilab(65:end),1),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zEAllG,0);
set(gca,'xticklabel',newclan);


%%% Adding number of codas recorded for each group
    % NUmber of codas used in the dendrogram
    nCodasQuant = [unique(dataCodasAllG(:,3)) histc(dataCodasAllG(:,3), unique(dataCodasAllG(:,3)))];
    % removing rejected days
    nCodasQuant(rejlab,:) = [];
    %reordering according to dendrogram
    nCodasQuant = nCodasQuant(newlab, :);
    %adding to dendrogram
    legend(num2str(nCodasQuant(:,2)'), 'Location', 'South')



    
    
    



%%% 3.2 ALL data MINUS Caribbean , Euclidean similarity, photoID groups, b=0.001
dataCodasAllC = dataCodasAllG;
carib1 = dataCodasAllC(:,3)==41; % caribbean groups = 41
dataCodasAllC(carib1,:) = []; %remove
carib2 = dataCodasAllC(:,3)==58; % caribbean groups = 58
dataCodasAllC(carib2,:) = []; %remove

simAbsInfAllC = daysim(dataCodasAllC,1,25,0);
%[simsoutIAllG, z0001bootIAllG, z001bootIAllG, z01bootIAllG, z1bootIAllG,out0001IAllG,out001IAllG,out01IAllG,out1IAllG] = daysim_boot_mc(dataCodasAllG,100,2,25,5,0,1);
zIAllC = linkage_sim(simAbsInfAllC(:,3)');
dendrogram_sim(zIAllC,0);
cophenet(zIAllC, simAbsInfAllC(:,3)')

newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllC(:,3));
rejlab = [7 11 14 16 17 24 27 32 66 80];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zIAllC,0);
set(gca,'xticklabel',newclan)






%%% 3.3. ALL data MINUS Tonga+Caribbean , Euclidian similarity, photoID groups, b=0.001
dataCodasAllM = dataCodasAllG;
tonga = dataCodasAllM(:,3)==47; %tonga groups = 47
dataCodasAllM(tonga,:) = []; %remove
carib1 = dataCodasAllM(:,3)==41; % caribbean groups = 41
dataCodasAllM(carib1,:) = []; %remove
carib2 = dataCodasAllM(:,3)==58; % caribbean groups = 58
dataCodasAllM(carib2,:) = []; %remove

simAbsInfAllM = daysim(dataCodasAllMs,1,25,0);
%[simsoutIAllG, z0001bootIAllG, z001bootIAllG, z01bootIAllG, z1bootIAllG,out0001IAllG,out001IAllG,out01IAllG,out1IAllG] = daysim_boot_mc(dataCodasAllG,100,2,25,5,0,1);
zIAllMs = linkage_sim(simAbsInfAllMs(:,3)');
dendrogram_sim(zIAllMs,0);
cophenet(zIAllMs, simAbsInfAllMs(:,3)')

newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllM(:,3));
rejlab = [7 11 14 16 17 24 27 32 65 79];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zIAllMs,0);
set(gca,'xticklabel',newclan)






%%% 3.4. ALL data MINUS long codas (>10), Euclidean similarity, photoID groups, b=0.001
[simAbsEucAlls rejlab] = daysim(dataCodasAllGs,1,25,0);
%[simsoutEAllG, z0001bootEAllG, z001bootEAllG, z01bootEAllG, z1bootEAllG,out0001EAllG,...
%    out001EAllG,out01EAllG,out1EAllG] = daysim_boot_mc(dataCodasAllG,10,1,25,15,0,1);
zEAllGs = linkage_sim(simAbsEucAlls(:,3)');
dendrogram_sim(zEAllGs,0);
cophenet(zEAllGs, simAbsEucAlls(:,3)')

% changing dendrogram labels
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllGs(:,3));
rejlab = [7 11 14 16 17 24 27 32 35 68 82 87];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(65:end) zeros(size(orilab(65:end),1),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zEAllGs,0);
set(gca,'xticklabel',newclan)







%%% 3.5 ALL data MINUS Caribbean MINUS longcodas (>10), Euclidean similarity, photoID groups, b=0.001
dataCodasAllC = dataCodasAllGs;
carib1 = dataCodasAllC(:,3)==41; % caribbean groups = 41
dataCodasAllC(carib1,:) = []; %remove
carib2 = dataCodasAllC(:,3)==58; % caribbean groups = 58
dataCodasAllC(carib2,:) = []; %remove

[simAbsInfAllC rej ]= daysim(dataCodasAllC,1,25,0);
%[simsoutIAllG, z0001bootIAllG, z001bootIAllG, z01bootIAllG, z1bootIAllG,out0001IAllG,out001IAllG,out01IAllG,out1IAllG] = daysim_boot_mc(dataCodasAllG,100,2,25,5,0,1);
zIAllC = linkage_sim(simAbsInfAllC(:,3)');
dendrogram_sim(zIAllC,0);
cophenet(zIAllC, simAbsInfAllC(:,3)')

newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllC(:,3));
rejlab = [7 11 14 16 17 24 27 32 35 68 80 85];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zIAllC,0);
set(gca,'xticklabel',newclan)







%%% 3.6. ALL data MINUS Tonga+Caribbean MINUS longcodas (>10), Euclidian similarity, photoID groups, b=0.001
dataCodasAllM = dataCodasAllGs;
tonga = dataCodasAllM(:,3)==47; %tonga groups = 47
dataCodasAllM(tonga,:) = []; %remove
carib1 = dataCodasAllM(:,3)==41; % caribbean groups = 41
dataCodasAllM(carib1,:) = []; %remove
carib2 = dataCodasAllM(:,3)==58; % caribbean groups = 58
dataCodasAllM(carib2,:) = []; %remove

[simAbsInfAllM rej] = daysim(dataCodasAllM,1,25,0);
%[simsoutIAllG, z0001bootIAllG, z001bootIAllG, z01bootIAllG, z1bootIAllG,out0001IAllG,out001IAllG,out01IAllG,out1IAllG] = daysim_boot_mc(dataCodasAllG,100,2,25,5,0,1);
zIAllMs = linkage_sim(simAbsInfAllM(:,3)');
dendrogram_sim(zIAllMs,0);
cophenet(zIAllMs, simAbsInfAllM(:,3)')

newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllM(:,3));
rejlab = [7 11 14 16 17 24 27 32 35 65 79 84];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zIAllMs,0);
set(gca,'xticklabel',newclan)







%%%
%%% 3.7. ALL data MINUS OPTICS Noise , Euclidean similarity, photoID groups, b=0.001

%retrieve original data mxmm
load('dataCodaOpticsAllG_mxmm.csv');
%removing noise coda types (#99)
dataCodaOpticsAllG_mxmmN = rmNoise(dataCodaOpticsAllG_mxmm);
%NB: due to the large removal of codas as noise (~76%), here none isremoved
[simAbsEucNmxmm rejlab2] = daysim(dataCodaOpticsAllG_mxmmN(:,1:46),1,1,0);
zENmxmm = linkage_sim(simAbsEucNmxmm(:,3)');
dendrogram_sim(zENmxmm,0);
cophenet(zENmxmm, simAbsEucNmxmm(:,3)')

%removing the groups rejected in the main analysis
rejlab3 = [7 11 14 16 17 24 27 32 68 1409];
aux3 = simAbsEucNmxmm;
for i=1:size(rejlab3,2)
    aux = find(aux3(:,1)==rejlab3(i));
    aux2= find(aux3(:,2)==rejlab3(i));
    aux3(aux,:) =[];
    aux3(aux2,:)=[];
end

zENmxmm = linkage_sim(aux3(:,3)');
dendrogram_sim(zENmxmm,0);

newlab3 = str2num(get(gca,'xticklabel'));
orilab3 = unique(dataCodaOpticsAllG_mxmmN(:,3));
rejlab3 = [10 13 15 22 29 64];
orilab3(rejlab3) = [];
corlab3 = [sort(orilab3) sort(newlab3)];
label3 = zeros(size(orilab3,1),1);
for g = 1:size(corlab3,1)               
    label3(newlab3==corlab3(g,2)) = corlab3(g,1);
end
set(gca,'xticklabel',label3)

%%% Relating ETP photoid group names with clan names
% 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan3 = [orilab3(63:end) zeros(size(orilab3(63:end),1),1)];
groupclan3 = [groupclan; newgroupclan3];
newclan3 = zeros(size(label3,1),1);
for g = 1:size(groupclan3,1)               
    newclan3(groupclan3(g,1)==label3(:,1)) = groupclan3(g,2);
end
[label3 newclan3]
dendrogram_sim(zENmxmm,0);
set(gca,'xticklabel',newclan3);














%%%%%%%% EXTRA: ETP Data labeled by social units %%%%%%%%
%%% NOT WORKING ALL RIGHT YET %%%
%%% ETP data, Infinity-norm similarity,by social units, b=0.001
% Getting the repertories of units Galapagos
%coda number, social unit
unit_assignments
%picking up the rows from the database with unit assignments
dataCodasETPU = zeros(1,46);
for g = 1:size(unit_assignments,1)
    for h = 1:size(dataCodasETPG,1)
        if unit_assignments(g,1)==dataCodasETPG(h,1)
           aux = dataCodasETPG(h,:);
           dataCodasETPU = [dataCodasETPU; aux];
        end
    end
end
%replacing group labels by unit labels
dataCodasETPU(1,:)=[];
dataCodasETPU(:,3) = unit_assignments(:,2);

%ETP data, Infinity-norm similarity,by social units, b=0.001
simAbsInfETPU = daysim(dataCodasETPU,2,25,0);
zIETPU = linkage_sim(simAbsInfETPU(:,3)'); 
dendrogram_sim(zIETPU,0);
cophenet(zIETPU, simAbsInfETPU(:,3)')

newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasETPU(:,3));
rejlab = [5 7 19 21 23];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)









%% 4. Mantel tests for group repertoires


%To test if recordings from different days of the same group represent different repertoires
%A Mantel permutation test will compare all the measures of similarity among
%daily repertoires of a given group and among repertoires of different
%groups to get a global measure of similarity within groups and between groups: 
%because there could be variations due to sampling. 
%Each day repertoire (pooling all recordings of that day) is considered an independent sample
%to avoid potential autocorrelation of coda production in that day.
%For each group, we need a replicate (more than one day)
%The Mantel test will account for heterogeneity in sampling within/between groups, as 
%it gives a sense of self similarity


%%%% ETP DATASET %%%%

datalabel = [dataGroupETP.group_date_assignments(:,2) dataGroupETP.group_date_assignments(:,1)];
datalabel = sortrows(datalabel);

% Preparing the similarity results by day (for b=0.001) and transforming it into a matrix
%%% ETP data, Euclidean similarity, photoID groups, b=0.001
%get rejected days too
[simAbsInfETPD rejlab2] = daysim(dataCodasETPD,1,25,0);
%Making the similarity matrix from the simils output
matsimAbsEucDayETP = simmatrix(simAbsInfETPD);

% Removing excluded groups from the other analysis: rejected 9 groups (<25 codas)
%rejlab = [7 11 14 16 17 24 27 32 68];
%for i=1:size(rejlab,2)
%    datalabel(find(datalabel(:,1)==rejlab(i)), :) = [];
%end
% Remove excluded days
for i=1:size(rejlab2,1)
    datalabel(find(datalabel(:,2)==rejlab2(i)), :) = [];
end
datalabel = [datalabel (1:size(datalabel,1))'];


% The Binary matrix will look like a bunch of large boxes of 1s around the diagonal 
%To do that, create a big nxn matrix of zeros (n is total number of daily repertoires)
%then fill it with multiple small mxm matrices of 1s by using the m number of repertoires (days) for 
% each group landmarked in the larger nxn matrix by the repertoire number between groups.
%putting small matrices inside of the big one:

n = size(unique(datalabel(:,2)), 1);
big = zeros(n);
%get sizes
[rowsBig, columnsBig] = size(big);

for i=1:n
    %Reps = values of daily repertoires for each group
    Reps = datalabel(find(datalabel(:,1)==i), 3);
    %Rlength= Get length of reps for each group
    Rlength = size(Reps, 1);
    %Build matrix of 1s of size Rlength
    small = ones(Rlength);
    %get sizes
    [rowsSmall, columnsSmall] = size(small);
    % Specify upper left row, column of where we'd like to paste the small matrix.
    row1 = min(Reps); column1 = min(Reps);
    % Determine lower right location.
    row2 = row1 + rowsSmall - 1; column2 = column1 + columnsSmall - 1;
    %paste it.
	big(row1:row2, column1:column2) = small;
end

% mantel
[c pval] = bramila_mantel(matsimAbsEucDayETP, big, 1000, 'pearson')
mantel1(matsimAbsEucDayETP, big, 1000)





%%% ALL DATASET %%%%

%Combining data by DAY
dataCodasAllD = [dataCodasETPD(:,1:45); dataCodasD];

%relabelling Gal1314 groups
dataGroups2 = dataGroups;
%auxYear = datestr(dataGroups(:,1), 'yy');
%dataGroups2(:,2) = [str2num(auxYear)*100 + dataGroups(:,2)];
%%Just adding 64, ie the maximum number of groups in ETP
dataGroups2(:,2) = dataGroups(:,2)+ max(dataCodasETPG(:,3))

%organizing reference data on groups and dates
datalabel = [dataGroupETP.group_date_assignments; dataGroups2];
datalabel = [datalabel(:,2) datalabel(:,1)];
datalabel = sortrows(datalabel);

% Preparing the similarity results by day (for b=0.001) and transforming it into a matrix
%%% ETP data, Euclidean similarity, photoID groups, b=0.001
%get rejected days too
[simAbsEucAllD rejlab3] = daysim(dataCodasAllD,1,25,0);
%Making the similarity matrix from the simils output
matsimAbsEucDayAll = simmatrix(simAbsEucAllD);

% Remove excluded days
for i=1:size(rejlab3,1)
    datalabel(find(datalabel(:,2)==rejlab3(i)), :) = [];
end
datalabel = [datalabel (1:size(datalabel,1))'];


% The Binary matrix will look like a bunch of large boxes of 1s around the diagonal 
%To do that, create a big nxn matrix of zeros (n is total number of daily repertoires)
%then fill it with multiple small mxm matrices of 1s by using the m number of repertoires (days) for 
% each group landmarked in the larger nxn matrix by the repertoire number between groups.
%putting small matrices inside of the big one:

n = size(unique(datalabel(:,2)), 1);
big = zeros(n);
%get sizes
[rowsBig, columnsBig] = size(big);

for i=1:n
    %Reps = values of daily repertoires for each group
    Reps = datalabel(find(datalabel(:,1)==i), 3);
    %Rlength= Get length of reps for each group
    Rlength = size(Reps, 1);
    %Build matrix of 1s of size Rlength
    small = ones(Rlength);
    %get sizes
    [rowsSmall, columnsSmall] = size(small);
    % Specify upper left row, column of where we'd like to paste the small matrix.
    row1 = min(Reps); column1 = min(Reps);
    % Determine lower right location.
    row2 = row1 + rowsSmall - 1; column2 = column1 + columnsSmall - 1;
    %paste it.
	big(row1:row2, column1:column2) = small;
end

% mantel
[c pval] = bramila_mantel(matsimAbsEucDayAll, big, 1000, 'pearson')
mantel1(matsimAbsEucDayAll, big, 1000)








%% 5. Assigning groups to clans and dates to clans


load('dataCodasAllG.csv');

% REPEATING THE BEST ANALYSIS: (3.1.) ALL data , Euclidean similarity, photoID groups, b=0.001
[simAbsEucAll rejlab] = daysim(dataCodasAllG,1,25,0);
%[simsoutEAllG, z0001bootEAllG, z001bootEAllG, z01bootEAllG, z1bootEAllG,out0001EAllG,...
%    out001EAllG,out01EAllG,out1EAllG] = daysim_boot_mc(dataCodasAllG,100,1,25,15,0,1);
zEAllG = linkage_sim(simAbsEucAll(:,3)');
dendrogram_sim(zEAllG,0);
%cophenet(zEAllG, simAbsEucAll(:,3)')

% changing dendrogram labels
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodasAllG(:,3));
rejlab = [7 11 14 16 17 24 27 32 68 (max(dataCodasETPG(:,3))+9)];
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
%set(gca,'xticklabel',label)

%%% Relating ETP photoid group names with clan names
% 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(65:end) zeros(size(orilab(65:end),1),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
%dendrogram_sim(zEAllG,0);
set(gca,'xticklabel',newclan);



%%%% Getting clan labels per group and per day.

   %%Assigning clan labels to GAL1314 data based on dendrogram:
    assignClan = [label newclan]
    % Short (5): 1306, 1415, 1416, 1413, 1305
    assignClan([13,14,15,21,22], 2) = 5;
    % FourPlus (6): 1307, 1408, 1414, 1302, 1303, 1304, 1412, 1411, 1301, 1410
    assignClan([63:70,74,75], 2) = 6;
    assignClan

   %%Getting the GAL1314 dates for the groups (SEE 01_GAL1314_INIT.m!!)
    dataGroups 
    grouplabels = [datestr(dataGroups(:,1)) repmat(' -> ',size(dataGroups,1),1) num2str(dataGroups(:,2))]
    % Renaming groups to match    
    groupName = [str2num(datestr(dataGroups(:,1), 'yy'))*100+ dataGroups(:,2)];
    
    %Organizing for GAL1314: dateNum, groupLabel, clan(1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+')
    newClans = zeros(size(dataGroups,1),1);
    DateGroupClan = [dataGroups(:,1), groupName, newClans];
        for g = 1:size(assignClan,1)               
            newClans(assignClan(g,1)==DateGroupClan(:,2)) = assignClan(g,2);
        end
    DateGroupClan = [dataGroups(:,1), groupName, newClans];
    
    % displaying only (date, group, clan)
    [datestr(DateGroupClan(:,1)), repmat(' -> ',size(DateGroupClan,1),1),...
     num2str(DateGroupClan(:,2)), repmat(' -> ',size(DateGroupClan,1),1), num2str(DateGroupClan(:,3))]

    %%Getting dates for ALL data
    
    % ...
    
    

