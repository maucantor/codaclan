%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014 Acoustic Analysis
% Mauricio Cantor, Dalhousie University, May 2015
%
% CATEGORICAL ANALYSES.
% Eastern Tropical Pacific (ETP) and new Galapagos (GAL1314) data:
% Summary:
% 1. Loadings and data organization: ETP, GAL, ALL
% 2. Categorical metric (OPTICSxi): defining coda types
% 3. Handling OPTICSxi outputs: recreating data sets
% 4. RhythmPlots to double check coda type and create meaningful labels
% 5. Discovery curves to evaluate sample size coda type per photoid group
% 6. Clusplots and PCA to visualize final coda types
% 7. Categorical similarities: Spearman correlation and Binary similarity (Tyler/Shane's code)
% 8. Coda types table per group


%% 1. Loadings and Data Organization

% Set input paths:
%Please set your path to the working directory manually
%%% NOTE: If working in Windows do CTRL+F to replace all "/" with "\"! %%%
addpath('Coda_K-means')
addpath('Coda_multivariate')
addpath('GroupSizesPetersen')
addpath('data')
addpath('MATLAB_Galapagos')
addpath('_Cachalote_coredata')
addpath('data/ETP_dataCodas')

% Set output paths
outputpath = [pwd, '/data'];
outputOptics = [pwd, '/OPTICSxi_Elki/data_input'];
outputOptics2 = [pwd, '/OPTICSxi_Elki/data_output'];

% dock all figures
set(0,'DefaultFigureWindowStyle','docked')


% Data input:

%%%% 1.1. ETP DATASET %%%%

%%a. dataCodasETP.codas: coda standard data; dataCodasETP.codas_cols: column name
%for coda data; dataCodasETP.clusternames: coda type labels from k-means.
%dataCodasETP.clanNames: clan names in relation to their numerical labels cols#)
dataCodasETP = load([pwd, '/data/ETP_dataCodas/allcodas_with_types.mat']);

%%b. dataGroupETP.group_assignments: Group assignments = coda#, recording date, group number
%after using groups.m on ids and group_assignments(:,1:2); made groups from
%all q>=3 ids using nab>0.25(min(na,nb)); dataGroupETP.group_date_assignments: 
%date (numerical format) for each photoid group; dataGroupETP.ids: date
%(numerical format) and IDs of members of each photoID group
dataGroupETP = load([pwd, '/data/ETP_dataCodas/group_assignments.mat']);

%%c. group_data: group id, number of codas, %<9 clicks, first day recorded, lat,
%long, # stars for fig, clan number for seven clans (see dataCodasETP.clanNames)
%1/0 genetic data available.
%To find data for a group enter group_data with row#=dendrogram node#
load([pwd, '/data/ETP_dataCodas/groupData.mat']);

%%d. unit_assignments: ETP ID and unit label
load([pwd, '/data/ETP_dataCodas/unit_assignments.mat']);

% Data organization:

% ETP Data with repertoires labeled by Day
dataCodasETPD = dataCodasETP.codas;

% ETP Data with repertoires labeled by Group, defined by photoID
%(codas recorded in the same day = group; codas recorded on different days were in
%the same group if more than 25% of the individuals were present in both)
dataCodasETPG = dataCodasETP.codas;
dataCodasETPG(:,3) = dataGroupETP.group_assignments(:,3);

%%%%%%%%%%%%%%%% REMOVING Caribbean DATA
%carib1 = dataCodasETPG(:,3)==41; % caribbean groups = 41
%dataCodasETPG(carib1,:) = []; %remove
%carib2 = dataCodasETPG(:,3)==58; % caribbean groups = 58
%dataCodasETPG(carib2,:) = []; %remove

%%%%%%%%%%%%%%%% REMOVING LONG CODAS (>= 13 clicks)
%dataCodasAllGs = dataCodasAllG;
%longcodas = dataCodasAllG(:,4)>=13;
%dataCodasAllGs(longcodas,:) = [];



%%%% 1.2. GAL1314 DATASET %%%%

% Input GAL1314 DataCodas with repertoires organized by photoID groups
dataCodasG = load('dataCodasG.csv');



%%%% 1.3. ALL DATASET ETP+GAL %%%%

% ALL Dataset: ETP + GAL1314, labeled by photoID groups
%Relabelling GAL1314 photoID groups to be different than the ETP. These
%will be 'yy##' (yy=year (13,14))
aux = dataCodasG;
auxYear = datestr(aux(:,2), 'yy');
auxLab = [str2num(auxYear)*100+ aux(:,3)];
aux(:,3) = auxLab;
% combining the datasets (note that we removed the ETP 46th col with kmeans
dataCodasAllG = [dataCodasETPG(:,1:45); aux];






%%%% coda sample sizes per year %%%%%
% year vs count of codas
[unique(str2num(datestr(dataCodasETP.codas(:,3)+datenum('31-dec-1899'), 'yyyy'))) ...
 histc(str2num(datestr(dataCodasETP.codas(:,3)+datenum('31-dec-1899'), 'yyyy')),...
 unique(str2num(datestr(dataCodasETP.codas(:,3)+datenum('31-dec-1899'), 'yyyy')))) ; ...
 unique(str2num(datestr(dataCodasG(:,2), 'yyyy'))) ...
 histc(str2num(datestr(dataCodasG(:,2), 'yyyy')),unique(str2num(datestr(dataCodasG(:,2), 'yyyy'))))]

% days vs count of codas for ETP
[unique(str2num(datestr(dataCodasETP.codas(:,3)+datenum('31-dec-1899'), 'yymmdd'))) ...
 histc(str2num(datestr(dataCodasETP.codas(:,3)+datenum('31-dec-1899'), 'yymmdd')),...
 unique(str2num(datestr(dataCodasETP.codas(:,3)+datenum('31-dec-1899'), 'yymmdd'))))]

% year vs groups
x=dataGroupETP.group_assignments(:,2:3);
x(:,1) = str2num(datestr(dataGroupETP.group_assignments(:,2)+datenum('31-dec-1899'), 'yyyy'));
y=dataCodasG(:,2:3);
y(:,1)=str2num(datestr(dataCodasG(:,2), 'ddmmyy'));
y(:,2)=auxLab;
z=[x;y];
w=unique(z, 'rows');
rejgroup = [7 11 14 16 17 24 27 32 68 1409];
for i=1:10; w(w(:,2)==rejgroup(i),:)=[]; end;
w







%% 2. Categorical metric (OPTICSxi): defining coda types

% preparing ETP data for OPTICSxi based on photoid groups. Three schemes:

%%%% 2.1 ALL DATASET (ETP+GAL) %%%%

% Using the same rules to define coda types in early ETP
%and new Galapagos data. Allow new data to form its own clusters or to be
%part of the clusters formed by the previous ETP data.

% Define a string vector with name of columns in raw data that will
%become the label variables for OPTICSxi. 
%NOTE: combining ETP with GAL1314 datasets require some changes: ETP
%contains coda#, rec#, group#, nClicks, kmeans; GAL1314 contais coda#, date,
%group#, branch#. We will drop the 46th col.
colname = {'codaN', 'recORdate', 'group', 'nClicks'};

% Now format data
dataCodasToOptics(dataCodasAllG, 1, colname,'ALLG', [outputOptics '/ALL']);









%% OPTICS

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RUN OPTICS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% 2.1 ALL DATASET (ETP+GAL) %%%%


% Parameters: xi and minpts:
%Find a reasonable xi across coda lengths. xi=0.025 seems to work well.
%Find a reasonable proportion of codas to define minpts, and use it for all coda lengths. 4% seems to work well (minpts = total_number_of_Nclicks_codas * 0.04)
%Vary xi and minpts to 'extreme', but reasonable, values.  Worked well: xi minimum=0.005 and maximum =0.05; minpts minimum=2%, maximum=8% (but restricting to at least 20 codas and at a maximum 175 codas). 
%Define three schemes to evaluate the parameter space:
%       a) mid xi, mid minpts; b) min xi, max minpts; c) max xi, min minpts. 
%       NB: I could run 9 scenarios combining all these possibilities. But 
%       the maximized xi+minpts tend to give just 1 coda type; and minimized 
%       xi+minpts tend to give way too many coda types. It doesn't seem to make much sense

% Defining xi:
% min = 0.005
% mid =0.025
% max = 0.050

% Defining minpts for each coda length. Percentage of codas used
% min = 2% (bottom = 20)
% mid = 4%
% max = 8% (ceiling = 175)

%number of codas per length
codalength = unique(dataCodasAllG(:,4));
codasample = zeros(size(codalength,1),1);
for i=1:size(codalength,1)
   codasample(i) = length(find(dataCodasAllG(:,4)==codalength(i))); 
end

%applying min=2%, mid=4%, max=8%. Restrict ceiling=175, floor=20
mpts = [codasample*.02 codasample*.08]
ceiling = mpts(:,2)>175;
mpts(ceiling,2)=175; 
floor = mpts(:,1)<20;
mpts(floor,1)=20;
format shortg
minpts = [codalength codasample mpts(:,1) codasample*.04 mpts(:,2)]







%% 3. Handling OPTICSxi outputs

% Input data from OPTICSxi defining coda types for each coda length
% Choose all txt files from the OPTICS output folder. Take all
%'cluster_XX.txt' files for a particular coda length and the function will
%organize into a single dataCoda format, with +46th with Optics cluster label (aka
%the coda type)

% Repertoires among PhotoID Groups



%%%% 3.1. ALL DATASET (ETP+GAL) %%%%

% This will loop up to the total of coda lenghts. However, there is just a few data for codas greater than 12-clicks

% 3.1.1. loop up to 12-click codas:
nCodalength = unique(dataCodasAllG(:,4));

% Scheme1: low xi, high minpts
for m=1:find(nCodalength==12)
    disp(' ');
    disp(['Now processing ETP+GAL dataset, low xi high minpts, ' num2str(nCodalength(m)) '-click codas:']);
    disp(' ');
    OpticsToDatacodas('dataOpticsAllG',nCodalength(m), [outputOptics2 '/ALL/lowxi_higmp'],0);
    disp(' ');
end

% Scheme 2: mid xi, mid minpts
for m=1:find(nCodalength==12)
    disp(' ');
    disp(['Now processing ETP+GAL dataset, mid xi mid minpts, ' num2str(nCodalength(m)) '-click codas:']);
    disp(' ');
    OpticsToDatacodas('dataOpticsAllG',nCodalength(m), [outputOptics2 '/ALL/midxi_midmp'],0);
    disp(' ');
end

% Scheme 3: high xi, low minpts
for m=1:find(nCodalength==12)
    disp(' ');
    disp(['Now processing ETP+GAL dataset, high xi low minpts, ' num2str(nCodalength(m)) '-click codas:']);
    disp(' ');
    OpticsToDatacodas('dataOpticsAllG',nCodalength(m), [outputOptics2 '/ALL/higxi_lowmp'],0);
    disp(' ');
end


    
% 3.1.2. Get all the csv files for each coda size (up to 10) and merge into a single final file
% Select all 'dataOptics***' csv files
% low xi, high minpts
dataCodaOpticsAllG_lxhm = combineCsv('dataCodaOpticsAllG_lxhm', [outputOptics2 '/ALL/lowxi_higmp']);
% mid xi, mid minpts
dataCodaOpticsAllG_mxmm = combineCsv('dataCodaOpticsAllG_mxmm', [outputOptics2 '/ALL/midxi_midmp']);
% high xi, low minpts
dataCodaOpticsAllG_hxlm = combineCsv('dataCodaOpticsAllG_hxlm', [outputOptics2 '/ALL/higxi_lowmp']);
 

% 3.1.3. Dealing with >=12-click codas: It will assign a single coda type for each
% coda size, e.g. 131, 141, 151 (13-click coda type 1, 14-click coda type 1...) 
% Picking up the original data for >=13-click codas
longCodas = dataCodasAllG(dataCodasAllG(:,4)>=13, :);
longCodasSizes = nCodalength(nCodalength>=13)';
% assigning coda types
for c=longCodasSizes
    longCodas(longCodas(:,4)==c,46) = str2num(strcat(num2str(c),num2str(1)));
end


% 3.1.4. Append longCodas to dataOptics and sort by coda number:
dataCodaOpticsAllG_lxhm = [dataCodaOpticsAllG_lxhm; longCodas];
dataCodaOpticsAllG_mxmm = [dataCodaOpticsAllG_mxmm; longCodas];
dataCodaOpticsAllG_hxlm = [dataCodaOpticsAllG_hxlm; longCodas];

dataCodaOpticsAllG_lxhm = sortrows(dataCodaOpticsAllG_lxhm,1);
dataCodaOpticsAllG_mxmm = sortrows(dataCodaOpticsAllG_mxmm,1);
dataCodaOpticsAllG_hxlm = sortrows(dataCodaOpticsAllG_hxlm,1);


% 3.1.5. FINAL files with categorical coda types by OPTICs: save and export
dlmwrite([outputpath '/dataCodaOpticsAllG_lxhm.csv'], dataCodaOpticsAllG_lxhm,'delimiter',',','precision','%.6f');
dlmwrite([outputOptics2 '/ALL/lowxi_higmp/dataCodaOpticsAllG_lxhm.csv'], dataCodaOpticsAllG_lxhm,'delimiter',',','precision','%.6f');

dlmwrite([outputpath '/dataCodaOpticsAllG_mxmm.csv'], dataCodaOpticsAllG_mxmm,'delimiter',',','precision','%.6f');
dlmwrite([outputOptics2 '/ALL/midxi_midmp/dataCodaOpticsAllG_mxmm.csv'], dataCodaOpticsAllG_mxmm,'delimiter',',','precision','%.6f');

dlmwrite([outputpath '/dataCodaOpticsAllG_hxlm.csv'], dataCodaOpticsAllG_hxlm,'delimiter',',','precision','%.6f');
dlmwrite([outputOptics2 '/ALL/higxi_lowmp/dataCodaOpticsAllG_hxlm.csv'], dataCodaOpticsAllG_hxlm,'delimiter',',','precision','%.6f');










%% 4. Rhythm plots for coda types defined by OPTICS

% Coda rhythm plots to diagnose if the OPTICSs clusters are biologically meaningful


%Come up with biologically-meaningful labels
%Match with Rendell & Whitehead 2003 (note that most 'noise' was removed!)
% 'R'=regular, 'R1'=short regular, 'R2'=longer than R1, 'R3' longer than R2;
%'+'=extended pause, 'I'=increasing intervals, 'A,B,C': to distinguish similar types 


%%%% 4.1. ALL DATASET (ETP+GAL-CARIB) %%%%

%%4.1.1. OPTICS low xi, high minpts%%

%retrieve original data
load('dataCodaOpticsAllG_lxhm.csv');

% Exploratory: All coda types and noise
CodaRhythmPlots(dataCodaOpticsAllG_lxhm, 29, 'absolute', 0)
CodaRhythmPlots(dataCodaOpticsAllG_lxhm, 29, 'standardized', 0)

%removing noise coda types (#99)
dataCodaOpticsAllG_lxhmN = rmNoise(dataCodaOpticsAllG_lxhm);
%Rhythm plot for up to 12-click codas
CodaRhythmPlots(dataCodaOpticsAllG_lxhmN, 12, 'absolute', 0)

% Creating biologically meaningfull labels based on Rhythm plots
coda12types_lxhmN = {'12R' '11R1' '11R2' '10R1' '10R2' '10R3' '9R1' '9R3' ...
    '9R2' '8R1' '8R2' '7R1' '7R2' '7R3' '6R2' '6R1' '4+1' '5R' '1+2+1' ...
    '4R2' '4R1' '2+1' '3R' '1++2' '1+2'};

%Rhythm plot for up to 12-click codas with labels
CodaRhythmPlots(dataCodaOpticsAllG_lxhmN, 12, 'absolute', fliplr(coda12types_lxhmN))


%Rhythm plot for up to 12-click codas with noise and labels
coda12types_lxhm = {'11N','10N','9N','8N','7N','6N','5N','4N','3N', ...
    '12R' '11R1' '11R2' '10R1' '10R2' '10R3' '9R1' '9R3' ...
    '9R2' '8R1' '8R2' '7R1' '7R2' '7R3' '6R2' '6R1' '4+1' '5R' '1+2+1' ...
    '4R2' '4R1' '2+1' '3R' '1++2' '1+2'};
CodaRhythmPlots(dataCodaOpticsAllG_lxhm, 12, 'absolute', fliplr(coda12types_lxhm))




%%4.1.2. OPTICS mid xi, mid minpts%%

%retrieve original data
load('dataCodaOpticsAllG_mxmm.csv');

% Exploratory: All coda types and noise
CodaRhythmPlots(dataCodaOpticsAllG_mxmm, 29, 'absolute', 0)
CodaRhythmPlots(dataCodaOpticsAllG_mxmm, 29, 'standardized', 0)

%removing noise coda types (#99)
dataCodaOpticsAllG_mxmmN = rmNoise(dataCodaOpticsAllG_mxmm);
%Rhythm plot for up to 12-click codas
CodaRhythmPlots(dataCodaOpticsAllG_mxmmN, 12, 'absolute', 0)

% Creating biologically meaningfull labels based on Rhythm plots
coda12types_mxmmN = {'12R' '11R1' '11R2' '10R1' '10I' '10R2' '9R1' '9R3' ...
    '9R2' '8R1' '8R2' '7R1' '7R2' '4+1+1' '6I' '2+4' '5+1' '6R2' '6R1' '4+1'...
    '5R' '1+2+1' '3+1' '4R' '1+2' '2+1' '3R'};

%Rhythm plot for up to 12-click codas with labels
CodaRhythmPlots(dataCodaOpticsAllG_mxmmN, 12, 'absolute', fliplr(coda12types_mxmmN))


%Rhythm plot for up to 12-click codas with noise and labels
coda12types_mxmm = {'11N','10N','9N','8N','7N','6N','5N','4N','3N', ...
    '12R' '11R1' '11R2' '10R1' '10I' '10R2' '9R1' '9R3' ...
    '9R2' '8R1' '8R2' '7R1' '7R2' '4+1+1' '6I' '2+4' '5+1' '6R2' '6R1' '4+1'...
    '5R' '1+2+1' '3+1' '4R' '1+2' '2+1' '3R'};
CodaRhythmPlots(dataCodaOpticsAllG_mxmm, 12, 'absolute', fliplr(coda12types_mxmm))




%%4.1.3. OPTICS high xi, low minpts%%

%retrieve original data
load('dataCodaOpticsAllG_hxlm.csv');

% Exploratory: All coda types and noise
CodaRhythmPlots(dataCodaOpticsAllG_hxlm, 29, 'absolute', 0)
CodaRhythmPlots(dataCodaOpticsAllG_hxlm, 29, 'standardized', 0)

%removing noise coda types (#99)
dataCodaOpticsAllG_hxlmN = rmNoise(dataCodaOpticsAllG_hxlm);
%Rhythm plot for up to 12-click codas
CodaRhythmPlots(dataCodaOpticsAllG_hxlmN, 12, 'absolute', 0)

% Creating biologically meaningfull labels based on Rhythm plots
coda12types_hxlmN = { '12R' '11R1' '11R2' '10R1' '10I' '10R2' '9I2' '9I1' '9R1' '9R2' ...
    '8I' '8R' '7I' '1+6' '4+1+1' '5+1' '4+1+1' '2+4' '6R' '1+3+1'...
    '4R' '1+2+1' '3+1' '3R2' '1+2C' '1+2B' '1+2A' '2+1' '3R1'};

%Rhythm plot for up to 12-click codas with labels
CodaRhythmPlots(dataCodaOpticsAllG_hxlmN, 12, 'absolute', fliplr(coda12types_hxlmN))


%Rhythm plot for up to 12-click codas with noise and labels
coda12types_hxlm = { '12N','11N','10N','9N','8N','7N','6N','5N','4N','3N', ...
    '12R' '11R1' '11R2' '10R1' '10I' '10R2' '9I2' '9I1' '9R1' '9R2' ...
    '8I' '8R' '7I' '1+6' '4+1+1' '5+1' '4+1+1' '2+4' '6R' '1+3+1'...
    '4R' '1+2+1' '3+1' '3R2' '1+2C' '1+2B' '1+2A' '2+1' '3R1'};
CodaRhythmPlots(dataCodaOpticsAllG_hxlm, 12, 'absolute', fliplr(coda12types_hxlm))






%% 5. Discovery Curves: categorical coda types per photoID group


%%%% 5.1. ALL DATASET (ETP+GAL-CARIB) %%%%

%%5.1.1. OPTICS low xi, high minpts%%

%retrieve original data
load('dataCodaOpticsAllG_lxhm.csv');

%removing noise coda types (#99)
dataCodaOpticsAllG_lxhmN = rmNoise(dataCodaOpticsAllG_lxhm);
%Discovery curve plot for all coda types
codaDiscovery(dataCodaOpticsAllG_lxhmN, 3, 0);


%%5.1.2. OPTICS mid xi, mid minpts%%

%retrieve original data
load('dataCodaOpticsAllG_mxmm.csv');

%removing noise coda types (#99)
dataCodaOpticsAllG_mxmmN = rmNoise(dataCodaOpticsAllG_mxmm);
%Discovery curve plot for all coda types
codaDiscovery(dataCodaOpticsAllG_mxmmN, 3, 0);


%%5.1.3. OPTICS high xi, low minpts%%

%retrieve original data
load('dataCodaOpticsAllG_hxlm.csv');

%removing noise coda types (#99)
dataCodaOpticsAllG_hxlmN = rmNoise(dataCodaOpticsAllG_hxlm);
%Discovery curve plot for all coda types
codaDiscovery(dataCodaOpticsAllG_hxlmN, 3, 0);








%% 6. Clusplots and PCAs for each coda type length

% Take the OPTICS classification into clusplot, to summarize into 2 PCs
% Visualize coda types in 2D, with or without noise



%%%% 6.1. ALL DATASET (ETP+GAL) %%%%

%%6.1.1. OPTICS low xi, high minpts%%

% 6.1.1.2. Clusplots for all coda types, removing noise:

%retrieve original data
load('dataCodaOpticsAllG_lxhm.csv');
%removing noise coda types (#99)
dataCodaOpticsAllG_lxhmN = rmNoise(dataCodaOpticsAllG_lxhm);

%remove the nClicks label from the coda type (e.g. 31 -> 1, 3-click coda type 1 -> type 1). Otherwise clusplot doesn't work
%this works for up to 9 coda types; if more, remove manually
aux = num2str(dataCodaOpticsAllG_lxhmN(:,46));
dataCodaOpticsAllG_lxhmN(:,46) = str2num(aux(:,3:end));

% Clusplot for all coda lengths, withouth noise
clusplot(dataCodaOpticsAllG_lxhmN, 2, 2, 1);
clusplot(dataCodaOpticsAllG_lxhmN, 2, 1, 1);


% 6.1.1.4. Principal Component Analysis (alternative plotting, with noise)

% PCAs on ICIs for each coda length (up to 11-click codas), without noise
codaPCA(dataCodaOpticsAllG_lxhmN, 3, 11)

% PCAs on ICIs for each coda length (up to 11-click codas), with noise (9)
codaPCA(dataCodaOpticsAllG_lxhm, 3, 11)






%%% 6.1.2. OPTICS mid xi, mid minpts%%

% 6.1.2.1. Clusplots for each coda type, from 3 to 9clicks, removing noise:
dataClusplotOptics_mxmmN = {};
for m=3:9
    %read files in
    aux1 = load([pwd '/OPTICSxi_Elki/data_output/ALL/midxi_midmp/dataOpticsAllG_' num2str(m) 'codas.csv']);
    
    %remove noise
    dataClusplotOptics_mxmmN{m} = rmNoise(aux1);
    
    %remove the nClicks label from the coda type (e.g. 31 -> 1, 3-click coda type 1 -> type 1). Otherwise clusplot doesn't work
    %this works for up to 9 coda types; if more, remove manually
    aux = num2str(dataClusplotOptics_mxmmN{m}(:,46));
    dataClusplotOptics_mxmmN{m}(:,46) = str2num(aux(:,2:end));
    
    % clusplot for each coda length
    clusplot(dataClusplotOptics_mxmmN{m}, 2, m, 1);
end


% 6.1.2.2. Clusplots for all coda types, removing noise:

%retrieve original data
%removing codas longer than 9 (because it screw up the plot dimension)
%longCodas = find(dataCodaOpticsAllG_mxmmN(:,4)>9);
%dataCodaOpticsAllG_mxmmN(longCodas,:) = [];

%remove the nClicks label from the coda type (e.g. 31 -> 1, 3-click coda type 1 -> type 1). Otherwise clusplot doesn't work
%this works for up to 9 coda types; if more, remove manually
aux = num2str(dataCodaOpticsAllG_mxmmN(:,46));
dataCodaOpticsAllG_mxmmN(:,46) = str2num(aux(:,3:end));

% Clusplot for all coda lengths, withouth noise
clusplot(dataCodaOpticsAllG_mxmmN, 2, 2, 1)
clusplot(dataCodaOpticsAllG_mxmmN, 2, 1, 1)


% 6.1.2.3. Clusplots for all coda types, with noise:

%removing codas longer than 9 (because it screw up the plot dimension)
%longCodas = find(dataCodaOpticsAllG_mxmmN(:,4)>9);
%dataCodaOpticsAllG_mxmmN(longCodas,:) = [];

%remove the nClicks label from the coda type (e.g. 31 -> 1, 3-click coda type 1 -> type 1). Otherwise clusplot doesn't work
%this works for up to 9 coda types; if more, remove manually
dataCodaOpticsAllG_mxmmNew = dataCodaOpticsAllG_mxmm;
aux = num2str(dataCodaOpticsAllG_mxmmNew(:,46));
dataCodaOpticsAllG_mxmmNew(:,46) = str2num(aux(:,4:end));

% Clusplot for all coda lengths, with noise
clusplot(dataCodaOpticsAllG_mxmmNew, 2, 2, 1)
clusplot(dataCodaOpticsAllG_mxmmNew, 2, 1, 1)




% 6.1.2.4. Principal Component Analysis (alternative plotting, with noise)

%retrieve original data
load('dataCodaOpticsAllG_mxmm.csv');
%removing noise coda types (#99)
dataCodaOpticsAllG_mxmmN = rmNoise(dataCodaOpticsAllG_mxmm);

% PCAs on ICIs for each coda length (up to 11-click codas), without noise
codaPCA(dataCodaOpticsAllG_mxmmN, 3, 12)

% PCAs on ICIs for each coda length (up to 11-click codas), with noise (9)
codaPCA(dataCodaOpticsAllG_mxmm, 3, 12)















% 6.1.2.5. Principal Component Analysis with colour by clans and shape by coda types

% removing noise:
%%Add clan label 0:GAL1314,1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
% Relating  group names with clan names
groupclan = [group_data(:,1) group_data(:,8)];
newgroup = unique(dataCodasAllG(:,3)); newgroup = newgroup(newgroup>100);
newgroupclan = [newgroup zeros(size(newgroup,1),1)];
groupclan = [groupclan; newgroupclan];

clan = zeros(size(dataCodaOpticsAllG_mxmmN,1),1);
for g = 1:size(groupclan,1)               
    clan(groupclan(g,1)==dataCodaOpticsAllG_mxmmN(:,3)) = groupclan(g,2);
end

% PCAs on ICIs for each coda length (up to 12-click codas), without noise,
% with colours defining clans
% Plotting UNROTATED loadings as well
codaPCAclan(dataCodaOpticsAllG_mxmmN, 3, 12, clan,1, 0)


% adding noise:
clan2 = zeros(size(dataCodaOpticsAllG_mxmm,1),1);
for g = 1:size(groupclan,1)               
    clan2(groupclan(g,1)==dataCodaOpticsAllG_mxmm(:,3)) = groupclan(g,2);
end

% PCAs on ICIs for each coda length (up to 12-click codas), with noise, with colours defining clans
codaPCAclan(dataCodaOpticsAllG_mxmm, 3, 12, clan2, 1, 1)



%%%% TO DO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Are the plots of the scores rotated data? They look the same as the first plot you sent?
%mc: good point, only loading plots were rotated. I will figure this out. 
 
%I would suggest not using circles and squares when there are only two. It?s hard to see difference in this low res image ? maybe not a problem in final.
 
%In the ESM you will need to plat the PCA with all codas and label those removed as noise for context.
%mc: I've done that, but with so much noise it looks pretty confusing. Maybe I will plot one figure for each coda length for the ESM.




















%%6.1.3. OPTICS high xi, low minpts%%

% 6.1.3.2. Clusplots for all coda types, removing noise:

%retrieve original data
load('dataCodaOpticsAllG_hxlm.csv');
%removing noise coda types (#99)
dataCodaOpticsAllG_hxlmN = rmNoise(dataCodaOpticsAllG_hxlm);

%remove the nClicks label from the coda type (e.g. 31 -> 1, 3-click coda type 1 -> type 1). Otherwise clusplot doesn't work
%this works for up to 9 coda types; if more, remove manually
aux = num2str(dataCodaOpticsAllG_hxlmN(:,46));
dataCodaOpticsAllG_hxlmN(:,46) = str2num(aux(:,3:end));

% Clusplot for all coda lengths, withouth noise
clusplot(dataCodaOpticsAllG_hxlmN, 2, 2, 1);
clusplot(dataCodaOpticsAllG_hxlmN, 2, 1, 1);


% 6.1.3.4. Principal Component Analysis (alternative plotting, with noise)

% PCAs on ICIs for each coda length (up to 11-click codas), without noise
codaPCA(dataCodaOpticsAllG_hxlmN, 3, 11)

% PCAs on ICIs for each coda length (up to 11-click codas), with noise (9)
codaPCA(dataCodaOpticsAllG_hxlm, 3, 11)









%% 7. Similarity of categorical repertoires


%%%% 7.1. ALL DATASET (ETP+GAL-CARIB) %%%%

%%7.1.1. OPTICS low xi, high minpts%%

%%% 7.1.1.1. ALL data, Spearman Correlation OPTICS coda types, photoID groups

%removing noise
dataCodaOpticsAllG_lxhmN = rmNoise(dataCodaOpticsAllG_lxhm);

%coda threshold = 5
[simSpearAllG_lxhmN rejlab] = daysim(dataCodaOpticsAllG_lxhmN,3,5,0);
zAllG_lxhmN = linkage_sim(simSpearAllG_lxhmN(:,3)');
dendrogram_sim(zAllG_lxhmN,0);
cophenet(zAllG_lxhmN, simSpearAllG_lxhmN(:,3)')

%relabeling dendrogram with original group labels/numbers
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodaOpticsAllG_lxhmN(:,3));
%find indices of rejected days
[~,rej] = ismember(rejlab, orilab);
orilab(rej)=[];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%Relating ETP photoid group names with clan names; relabeling dendrogram
%clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(orilab>100) zeros(length(orilab(orilab>100)),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zAllG_lxhmN,0);
set(gca,'xticklabel',newclan)




%%% 7.1.1.2. ALL data, Binary Similarity OPTICS coda types, photoID groups

% Using binary similarity: it does NOT look at the similarity
%between codas of the same type. Instead, if two codas are the same type
%(as determined previously) then they are given a similarity of 1.
%If they are different coda types, then they are given a similarity of 0.

%retrieve original data and remove noise
dataCodaOpticsAllG_lxhmN = rmNoise(dataCodaOpticsAllG_lxhm);

%coda threshold = 5
[simBinAllG_lxhmN matsimSpearAllG_lxhmN rejlab] = daysimmatrix(dataCodaOpticsAllG_lxhmN,3,10);
zBAllG_lxhmN = linkage_sim(simBinAllG_lxhmN(:,3)');
dendrogram_sim(zBAllG_lxhmN,0);
cophenet(zBAllG_lxhmN, simBinAllG_lxhmN(:,3)')

%relabeling dendrogram with original group labels/numbers
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodaOpticsAllG_lxhmN(:,3));
%find indices of rejected days
[~,rej] = ismember(rejlab, orilab);
orilab(rej)=[];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%Relating ETP photoid group names with clan names; relabeling dendrogram
%clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(orilab>100) zeros(length(orilab(orilab>100)),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zBAllG_lxhmN,0);
set(gca,'xticklabel',newclan)










%%% 7.1.2. OPTICS mid xi, mid minpts%%

%%% 7.1.2.1. ALL data, Spearman Correlation OPTICS coda types, photoID groups

%removing noise
dataCodaOpticsAllG_mxmmN = rmNoise(dataCodaOpticsAllG_mxmm);

%coda threshold = 5
[simSpearAllG_mxmmN rejlab] = daysim(dataCodaOpticsAllG_mxmmN,3,5,0);
zAllG_mxmmN = linkage_sim(simSpearAllG_mxmmN(:,3)');
dendrogram_sim(zAllG_mxmmN,0);
cophenet(zAllG_mxmmN, simSpearAllG_mxmmN(:,3)')

%relabeling dendrogram with original group labels/numbers
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodaOpticsAllG_mxmmN(:,3));
%find indices of rejected days
[~,rej] = ismember(rejlab, orilab);
orilab(rej)=[];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%Relating ETP photoid group names with clan names; relabeling dendrogram
%clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(orilab>100) zeros(length(orilab(orilab>100)),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zAllG_mxmmN,0);
set(gca,'xticklabel',newclan)





%%% 7.1.2.2. ALL data, Binary Similarity OPTICS coda types, photoID groups

% Using binary similarity: it does NOT look at the similarity
%between codas of the same type. Instead, if two codas are the same type
%(as determined previously) then they are given a similarity of 1.
%If they are different coda types, then they are given a similarity of 0.

%retrieve original data and remove noise
dataCodaOpticsAllG_mxmmN = rmNoise(dataCodaOpticsAllG_mxmm);

%coda threshold = 5
[simBinAllG_mxmmN matsimSpearAllG_mxmmN rejlab] = daysimmatrix(dataCodaOpticsAllG_mxmmN,3,10);
zBAllG_mxmmN = linkage_sim(simBinAllG_mxmmN(:,3)');
dendrogram_sim(zBAllG_mxmmN,0);
cophenet(zBAllG_mxmmN, simBinAllG_mxmmN(:,3)')

%relabeling dendrogram with original group labels/numbers
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodaOpticsAllG_mxmmN(:,3));
%find indices of rejected days
[~,rej] = ismember(rejlab, orilab);
orilab(rej)=[];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%Relating ETP photoid group names with clan names; relabeling dendrogram
%clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(orilab>100) zeros(length(orilab(orilab>100)),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zBAllG_mxmmN,0);
set(gca,'xticklabel',newclan)







%%7.1.3. OPTICS high xi, low minpts%%

%%% 7.1.3.1. ALL data, Spearman Correlation OPTICS coda types, photoID groups

%removing noise
dataCodaOpticsAllG_hxlmN = rmNoise(dataCodaOpticsAllG_hxlm);

%coda threshold = 5
[simSpearAllG_hxlmN rejlab] = daysim(dataCodaOpticsAllG_hxlmN,3,5,0);
zAllG_hxlmN = linkage_sim(simSpearAllG_hxlmN(:,3)');
dendrogram_sim(zAllG_hxlmN,0);
cophenet(zAllG_hxlmN, simSpearAllG_hxlmN(:,3)')

%relabeling dendrogram with original group labels/numbers
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodaOpticsAllG_hxlmN(:,3));
%find indices of rejected days
[~,rej] = ismember(rejlab, orilab);
orilab(rej)=[];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%Relating ETP photoid group names with clan names; relabeling dendrogram
%clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(orilab>100) zeros(length(orilab(orilab>100)),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zAllG_hxlmN,0);
set(gca,'xticklabel',newclan)




%%% 7.1.1.2. ALL data, Binary Similarity OPTICS coda types, photoID groups

% Using binary similarity: it does NOT look at the similarity
%between codas of the same type. Instead, if two codas are the same type
%(as determined previously) then they are given a similarity of 1.
%If they are different coda types, then they are given a similarity of 0.

%retrieve original data and remove noise
dataCodaOpticsAllG_hxlmN = rmNoise(dataCodaOpticsAllG_hxlm);

%coda threshold = 5
[simBinAllG_hxlmN matsimSpearAllG_hxlmN rejlab] = daysimmatrix(dataCodaOpticsAllG_hxlmN,3,10);
zBAllG_hxlmN = linkage_sim(simBinAllG_hxlmN(:,3)');
dendrogram_sim(zBAllG_hxlmN,0);
cophenet(zBAllG_hxlmN, simBinAllG_hxlmN(:,3)')

%relabeling dendrogram with original group labels/numbers
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(dataCodaOpticsAllG_hxlmN(:,3));
%find indices of rejected days
[~,rej] = ismember(rejlab, orilab);
orilab(rej)=[];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%Relating ETP photoid group names with clan names; relabeling dendrogram
%clan names are in clanNames: 1=Regular,2='+1',3=W Caribbean,4=Tonga,5=Short,6='4+'
groupclan = [group_data(:,1) group_data(:,8)];
newgroupclan = [orilab(orilab>100) zeros(length(orilab(orilab>100)),1)];
groupclan = [groupclan; newgroupclan];
newclan = zeros(size(label,1),1);
for g = 1:size(groupclan,1)               
    newclan(groupclan(g,1)==label(:,1)) = groupclan(g,2);
end
[label newclan]
dendrogram_sim(zBAllG_hxlmN,0);
set(gca,'xticklabel',newclan)









%% 8. Coda types table per group

% Categorical coda repertoires per photoID group: number of coda types
% (table in Fig2 Rendell & Whitehead 2003)


%%%% 8.1. ALL DATASET (ETP+GAL-CARIB) %%%%

%%8.1.1. OPTICS low xi, high minpts%%

% 8.1.1.1 Creating coda type table

%retrieve original data and remove noise
load('dataCodaOpticsAllG_lxhm.csv');
dataCodaOpticsAllG_lxhmN = rmNoise(dataCodaOpticsAllG_lxhm);

%get contingency table by cross tabulation of coda types (row) x photoid groups (col)
[tablxhm,chi2,p] = crosstab(dataCodaOpticsAllG_lxhmN(:,46), dataCodaOpticsAllG_lxhmN(:,3));


% 8.1.1.2. Plotting heat maps

%Heat map of Relative counts of coda types (% of occurence within group)
tablxhmRelative = relativeTable(tablxhm, 'column');
plot(HeatMap(tablxhmRelative, 'RowLabels', unique(dataCodaOpticsAllG_lxhmN(:,46)), ...
        'ColumnLabels', unique(dataCodaOpticsAllG_lxhmN(:,3)), 'Colormap', 'redbluecmap'));


% 8.1.2.3. Coupling continuous dendrogram with categorical heat map.

%NB: dendrogram had some groups removed (<25codas); heat map had other
%groups removed with the 'noise coda type' removal. We have to match them

%Dendrogram:
%get the dendrogram of interest, their new labels and the rejected repertoires (<25 codas)
%simAbsInfAll = daysim(dataCodasAllG,2,25,0); %%% 2.3.1 ALL data, Infinity-norm similarity, photoID groups, b=0.001
%zIAllG = linkage_sim(simAbsInfAll(:,3)');
dendrogram_sim(zEAllG,0);
orilab = unique(dataCodasAllG(:,3)); %original group labels (89)
newlab = str2num(get(gca,'xticklabel')); %new group labels in dendrogram (79)
rejlab = [7 11 14 16 17 24 27 32 68 (max(dataCodasETPG(:,3))+9)]; %rejected groups' indices (10)

%row/col labels according to the cross tabulation
rcodes = unique(dataCodaOpticsAllG_lxhmN(:,46));
ccodes = unique(dataCodaOpticsAllG_lxhmN(:, 3));

%Organizing the coda type contingency table to match the dendrogram
finalTablelxhm = matchCodaTable(tablxhmRelative,ccodes,orilab,newlab,rejlab,0);

%relabeling already plotted dendrogram
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%plotting organized heat map to match dendrogram
codaTablePlot(finalTablelxhm, 1, rcodes, label, 0, 1, 100)
plot(HeatMap(finalTablelxhm, 'RowLabels', unique(dataCodaOpticsAllG_lxhmN(:,46)),...
    'ColumnLabels', label, 'Colormap', 'redbluecmap'))

%plotting organized heat map to match dendrogram: LIMITED TO 12CLICK CODAS
finalTable2lxhm = finalTablelxhm(1:25, :);
codaTablePlot(finalTable2lxhm, 1, rcodes, label, 0, 1, 100)
aux=unique(dataCodaOpticsAllG_lxhmN(:,46));
plot(HeatMap(finalTable2lxhm, 'RowLabels', aux(1:25) ,'ColumnLabels', label, 'Colormap', 'redbluecmap'))

%adding biologically-meaningful coda labels 
codaTablePlot(finalTable2lxhm, 1, flip(coda12types_lxhm), label, 0,1, 100)
%heat map
aux=unique(dataCodaOpticsAllG_lxhmN(:,46));
plot(HeatMap(finalTable2lxhm, 'RowLabels', flip(coda12types_lxhm),'ColumnLabels', label, 'Colormap','redbluecmap'))


% Plot heatmap with three thresholds <=5%, 5%<=10%, >10% 
Tlxhm = finalTable2lxhm;
Tlxhm(Tlxhm>=0.1)=1;
Tlxhm(Tlxhm<0.1 & Tlxhm>0.05)=0.6;
Tlxhm(Tlxhm<=0.05 & Tlxhm>0.01)=0.4;
Tlxhm(Tlxhm<=0.01 & Tlxhm>0)=0.2;
codaTablePlot(Tlxhm, 1, flip(coda12types_lxhmN), label, 0, 0, 5)









%%% 8.1.2. OPTICS mid xi, mid minpts%%

% 8.1.2.1 Creating coda type table

%retrieve original data and remove noise
load('dataCodaOpticsAllG_mxmm.csv');
dataCodaOpticsAllG_mxmmN = rmNoise(dataCodaOpticsAllG_mxmm);

%get contingency table by cross tabulation of coda types (row) x photoid groups (col)
%chisquare for testing independence of each dimension of TABLE. (That is,
    %that the proportion of items falling in any cell is equal to the
    %product of the proportion in that row, and the proportion in that
    %column, and so on.)
[tabmxmm,chi2,p] = crosstab(dataCodaOpticsAllG_mxmmN(:,46), dataCodaOpticsAllG_mxmmN(:,3));


% 8.1.2.2. Plotting heat maps

%Heat map of absolute counts of coda types per group
HeatMap(tabmxmm, 'RowLabels', unique(dataCodaOpticsAllG_mxmmN(:,46)), 'ColumnLabels', unique(dataCodaOpticsAllG_mxmmN(:,3)), 'Colormap', 'redbluecmap')

%Heat map of Relative counts of coda types (% of occurence within group)
tabmxmmRelative = relativeTable(tabmxmm, 'column');
HeatMap(tabmxmmRelative, 'RowLabels', unique(dataCodaOpticsAllG_mxmmN(:,46)), ...
        'ColumnLabels', unique(dataCodaOpticsAllG_mxmmN(:,3)), 'Colormap', 'redbluecmap');


% 8.1.2.3. Coupling continuous dendrogram with categorical heat map.

%NB: dendrogram had some groups removed (<25codas); heat map had other
%groups removed with the 'noise coda type' removal. WE have to match them

%Dendrogram:
%get the dendrogram of interest, their new labels and the rejected repertoires (<25 codas)
%simAbsEucAll = daysim(dataCodasAllG,2,25,0); %%% 2.3.4 ALL data, Euclidean Distance, photoID groups, b=0.001
%zEAllG = linkage_sim(simAbsEucAll(:,3)');
dendrogram_sim(zEAllG,0);
orilab = unique(dataCodasAllG(:,3)); %original group labels (89)
newlab = str2num(get(gca,'xticklabel')); %new group labels in dendrogram (79)
rejlab = [7 11 14 16 17 24 27 32 68 (max(dataCodasETPG(:,3))+9)]; %rejected groups' indices (10)
        
        % (
        %%%Number of codas recorded for each group
            % NUmber of codas used in the dendrogram
            nCodasQuant = [unique(dataCodasAllG(:,3)) histc(dataCodasAllG(:,3), unique(dataCodasAllG(:,3)))];
            % removing rejected days
            nCodasQuant(rejlab,:) = [];
            %reordering according to dendrogram
            nCodasQuant = nCodasQuant(newlab, :);
            %adding to dendrogram
            %legend(num2str(nCodasQuant(:,2)'), 'Location', 'South')
        % )
    
%row/col labels according to the cross tabulation
rcodes = unique(dataCodaOpticsAllG_mxmmN(:,46));
ccodes = unique(dataCodaOpticsAllG_mxmmN(:, 3));

        % (
        %%%Number of coda TYPES recorded for each group
            % NUmber of coda types 
            nCodasQuali = [unique(dataCodaOpticsAllG_mxmmN(:,3)) histc(dataCodaOpticsAllG_mxmmN(:,3), unique(dataCodaOpticsAllG_mxmmN(:,3)))];
            % removing rejected groups: add to the table to reorder, then extract
            tabmxmmRelative = [tabmxmmRelative;nCodasQuali(:,2)'];
        % )
        
        
%Organizing the coda type contingency table to match the dendrogram
finalTablemxmm = matchCodaTable(tabmxmmRelative,ccodes,orilab,newlab,rejlab,0);

        % (
        %%%extracting Ncodas ordered, removing from the table
            nCodasQuali = finalTablemxmm(end,:)
            finalTablemxmm(end,:) = [];
        % )

%relabeling already plotted dendrogram
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)


%plotting organized heat map to match dendrogram
codaTablePlot(finalTablemxmm, 1, rcodes, label, 0, 1, 100)
%plotting heatmap
plot(HeatMap(finalTablemxmm, 'RowLabels', unique(dataCodaOpticsAllG_mxmmN(:,46)),'ColumnLabels', label, 'Colormap', 'redbluecmap'))


%plotting organized heat map to match dendrogram: LIMITED TO 12CLICK CODAS
finalTable2mxmm = finalTablemxmm(1:27, :);
codaTablePlot(finalTable2mxmm, 1, rcodes, label, 0, 1, 100)
%adding biologically-meaningful coda labels 
codaTablePlot(finalTable2mxmm, 1, flip(coda12types_mxmmN), label, 0,1, 100)
%heat map
aux=unique(dataCodaOpticsAllG_mxmmN(:,46));
plot(HeatMap(finalTable2mxmm, 'RowLabels', flip(coda12types_mxmmN),'ColumnLabels', label, 'Colormap','redbluecmap'))


%Plotting binary
finalTable2mxmm10 = finalTable2mxmm;
finalTable2mxmm10(finalTable2mxmm>0)=1;
codaTablePlot(finalTable2mxmm10, 1, flip(coda12types_mxmmN), label, 0, 0, 1)
%plotting 10% coda usage threshold
finalTable2mxmm10 = finalTable2mxmm;
finalTable2mxmm10(finalTable2mxmm>=0.1)=1;
finalTable2mxmm10(finalTable2mxmm<0.1)=0;
codaTablePlot(finalTable2mxmm10, 1, flip(coda12types_mxmmN), label, 0, 0, 1)



% Plot heatmap with three thresholds <=5%, 5%<=10%, >10% 
Tmxmm = finalTable2mxmm;
Tmxmm(Tmxmm>=0.1)=1;
Tmxmm(Tmxmm<0.1 & Tmxmm>0.05)=0.6;
Tmxmm(Tmxmm<=0.05 & Tmxmm>0.01)=0.4;
Tmxmm(Tmxmm<=0.01 & Tmxmm>0)=0.2;
codaTablePlot(Tmxmm, 1, flip(coda12types_mxmmN), label, 0, 0, 5)

% Plot heatmap with three thresholds <=5%, 5%<=10%, >10% using number of

        % (
        %%% Adding number of samples to the plots
            %Final number of codas
            nCodas = [nCodasQuant nCodasQuali'];
            
            % codas as the x-axis labels
            codaTablePlot(Tmxmm, 1, flip(coda12types_mxmmN), nCodas(:,2), 0, 0, 5)

            %Adding codas per group
            annotation('textbox', 'String', (num2str(nCodas(:,2)')))
            annotation('textbox', 'String', (num2str(nCodas(:,3)')))
            %Adding codas per type
            nTypes=sum(tabmxmm, 2);
            nTypes(1:size(finalTable2mxmm,1));
            annotation('textbox', 'String', (num2str(nTypes(1:size(finalTable2mxmm,1)))))
        % )



        
        
        
        
        

%%8.1.3. OPTICS high xi, low minpts%%

% 8.1.1.1 Creating coda type table

%retrieve original data and remove noise
load('dataCodaOpticsAllG_hxlm.csv');
dataCodaOpticsAllG_hxlmN = rmNoise(dataCodaOpticsAllG_hxlm);

%get contingency table by cross tabulation of coda types (row) x photoid groups (col)
tabhxlm = crosstab(dataCodaOpticsAllG_hxlmN(:,46), dataCodaOpticsAllG_hxlmN(:,3));


% 8.1.1.2. Plotting heat maps

%Heat map of Relative counts of coda types (% of occurence within group)
tabhxlmRelative = relativeTable(tabhxlm, 'column');
plot(HeatMap(tabhxlmRelative, 'RowLabels', unique(dataCodaOpticsAllG_hxlmN(:,46)), ...
        'ColumnLabels', unique(dataCodaOpticsAllG_hxlmN(:,3)), 'Colormap', 'redbluecmap'));


% 8.1.2.3. Coupling continuous dendrogram with categorical heat map.

%NB: dendrogram had some groups removed (<25codas); heat map had other
%groups removed with the 'noise coda type' removal. We have to match them

%Dendrogram:
%get the dendrogram of interest, their new labels and the rejected repertoires (<25 codas)
%simAbsInfAll = daysim(dataCodasAllG,2,25,0); %%% 2.3.1 ALL data, Infinity-norm similarity, photoID groups, b=0.001
%zIAllG = linkage_sim(simAbsInfAll(:,3)');
dendrogram_sim(zEAllG,0);
orilab = unique(dataCodasAllG(:,3)); %original group labels (89)
newlab = str2num(get(gca,'xticklabel')); %new group labels in dendrogram (79)
rejlab = [7 11 14 16 17 24 27 32 68 (max(dataCodasETPG(:,3))+9)]; %rejected groups' indices (10)

%row/col labels according to the cross tabulation
rcodes = unique(dataCodaOpticsAllG_hxlmN(:,46));
ccodes = unique(dataCodaOpticsAllG_hxlmN(:, 3));

%Organizing the coda type contingency table to match the dendrogram
finalTablehxlm = matchCodaTable(tabhxlmRelative,ccodes,orilab,newlab,rejlab,0);

%relabeling already plotted dendrogram
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)

%plotting organized heat map to match dendrogram
codaTablePlot(finalTablehxlm, 1, rcodes, label, 0, 1, 100)
plot(HeatMap(finalTablehxlm, 'RowLabels', unique(dataCodaOpticsAllG_hxlmN(:,46)),...
    'ColumnLabels', label, 'Colormap', 'redbluecmap'))

%plotting organized heat map to match dendrogram: LIMITED TO 12CLICK CODAS
finalTable2hxlm = finalTablehxlm(1:29, :);
codaTablePlot(finalTable2hxlm, 1, rcodes, label, 0, 1, 100)
aux=unique(dataCodaOpticsAllG_hxlmN(:,46));
plot(HeatMap(finalTable2hxlm, 'RowLabels', aux(1:29) ,'ColumnLabels', label, 'Colormap', 'redbluecmap'))

%adding biologically-meaningful coda labels 
codaTablePlot(finalTable2hxlm, 1, flip(coda12types_hxlmN), label, 0,1, 100)
%heat map
aux=unique(dataCodaOpticsAllG_hxlmN(:,46));
plot(HeatMap(finalTable2hxlm, 'RowLabels', flip(coda12types_hxlmN),'ColumnLabels', label, 'Colormap','redbluecmap'))


% Plot heatmap with three thresholds <=5%, 5%<=10%, >10% 
Thxlm = finalTable2hxlm;
Thxlm(Thxlm>=0.1)=1;
Thxlm(Thxlm<0.1 & Thxlm>0.05)=0.6;
Thxlm(Thxlm<=0.05 & Thxlm>0.01)=0.4;
Thxlm(Thxlm<=0.01 & Thxlm>0)=0.2;
codaTablePlot(Thxlm, 1, flip(coda12types_hxlmN), label, 0, 0, 5)