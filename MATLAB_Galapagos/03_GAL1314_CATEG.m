%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014 Acoustic Analysis
% Mauricio Cantor, St Andrews University, Scotland, Jan 2015
%
% REPERTOIRE COMPARISONS:
% Summary:
% 1. Loadings and data organization: ETP, GAL, ALL
% 2. Categorical metric (OPTICSxi): defining coda types
% 3. Handling OPTICSxi outputs: recreating data sets
% 4. RhythmPlots to double check coda type and create meaningful labels
% 5. Discovery curves to evaluate sample size coda type per photoid group
% 6. Clusplots and PCA to visualize final coda types
% 7. Categorical similarities: Spearman correlation and Binary similarity (Tyler/Shane's code)
% 8. Coda types table per group
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1. Loadings

% Set input paths
% Please set your path to the working directory manually
addpath('Coda_K-means')
addpath('Coda_multivariate')
addpath('GroupSizesPetersen')
addpath('data')
addpath('MATLAB_Galapagos')
addpath('_Cachalote_coredata')
addpath('OPTICsxi_Elki')
addpath('OPTICsxi_Elki/data_input')

% Set output paths
outputpath = [pwd, '/data'];
outputOptics = [pwd, '/OPTICSxi_Elki/data_input'];
outputOptics2 = [pwd, '/OPTICSxi_Elki/data_output'];

% input data
load('dataClusplotEG.csv')
load('dataCodasG.csv')



%% 2. Repertoire similarity: Categorical metric (OPTICSxi)

% Summary: 
% Load data used in Clusplot.m
% format for OPTICS
% RUN OPTICS ON ELKI
%

% preparing data for OPTICSxi (NB: results will be the same for EE,ED,EG,IE,ID,IG!
% First, define a string vector with name of columns in raw data that will
%become the label variables for OPTICSxi.

%if using Clusplot data with branches from the boostraped dendrogram
%colname = {'codaN', 'date', 'repLabel', 'nClicks', 'branch'};
%format data
%dataCodasToOptics(dataClusplotEG, 3, colname,'GALG', [outputOptics '/GAL']);

%if using the basic coda data for GAL groups
colname = {'codaN', 'date', 'repLabel', 'nClicks'};
%format data
dataCodasToOptics(dataCodasG, 1, colname,'GALG', [outputOptics '/GAL']);





%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RUN OPTICS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% 3. Handling OPTICSxi outputs

% Summary: 
% Input data from OPTICSxi defining coda types for each coda length
% Choose all txt files from the OPTICS output folder. Take all
% 'cluster_XX.txt' files for a particular coda length and the function will
% organize into a single dataCoda format, with +46th with Optics cluster label (aka
% the coda type)


% Repertoires among Groups

% This will loop up to the total of coda lenghts (usually 3 to 15).
% However, there is just a few data for codas greater than 10clicks. So:
% 1. loop up to 9-click codas:
%for m=3:9
%    OpticsToDatacodas('dataOpticsG',m,outputpath,0);
%end
nCodalength = unique(dataCodasG(:,4));
for m=1:find(nCodalength==10)
    OpticsToDatacodas('dataOpticsG',nCodalength(m),outputpath,0);
end

    
% 2. Get all the csv files for each coda size (up to 9) and merge into a single final file
% Select all 'dataOpticsXX' csv files files in
% ...\data
dataCodaOpticsG = combineCsv('dataCodaOpticsG', outputpath);
 

% 3. Dealing with >=10-click codas: I will assign a single coda type for each
% coda size, e.g. 111, 121, 131, 141, 151 (11-click coda type 1, 12-click coda type 1...) 
% Picking up the original data fro >10-click codas

longCodas = dataClusplotEG(dataClusplotEG(:,4)>=10, :);
longCodasSizes = nCodalength(nCodalength>=10)';
% assigning coda types
for c=lonCodasSizes
    longCodas(longCodas(:,4)==c,46)=str2num(strcat(num2str(c),num2str(1)));
end


% 4. Append longCodas to dataOpticsEG and sort by Date:
dataCodaOpticsG = [dataCodaOpticsG; longCodas];
dataCodaOpticsG = sortrows(dataCodaOpticsG,2);


% 5. Replicate for repertoires by Encounter and Day labels
dataCodaOpticsE = dataCodaOpticsG;
load('dataCodasE.csv');
dataCodaOpticsE(:,3) = dataCodasE(:,3);

dataCodaOpticsD = dataCodaOpticsG;
load('dataCodasD.csv');
dataCodaOpticsD(:,3) = dataCodasD(:,3);    

% FINAL files with categorical coda types: save and export
dlmwrite([outputpath '/dataCodaOpticsE.csv'], dataCodaOpticsE,'delimiter',',','precision','%.6f');
dlmwrite([outputpath '/dataCodaOpticsD.csv'], dataCodaOpticsD,'delimiter',',','precision','%.6f');
dlmwrite([outputpath '/dataCodaOpticsG.csv'], dataCodaOpticsG,'delimiter',',','precision','%.6f');



%% 4. clusplots (PCA) for each coda type

% Take the OPTICS classification into clusplot, to summarize into 2 PCs

% preparing data for clusplots
dataClusplotOptics = {};
for m=3:9
    %read files in
    dataClusplotOptics{m} = load(['dataOpticsG_' num2str(m) 'codas.csv']);
    %remove the nClicks label from the coda type (e.g. 31 -> 1, 3-click
    %coda type 1 -> type 1). Otherwise clusplot doesn't work
    %this works for up to 9 coda types; if more, remove manually
        aux = num2str(dataClusplotOptics{m}(:,46));
        dataClusplotOptics{m}(:,46) = str2num(aux(:,2:end));
    clusplot(dataClusplotOptics{m},2,m,1);
end

%clusplots for each coda size
clusplot(dataOptics{3},2,3,1)
clusplot(dataOptics{4},2,4,1)
clusplot(dataOptics{5},2,5,1)
clusplot(dataOptics{6},2,6,1)
clusplot(dataOptics{7},2,7,1)
clusplot(dataOptics{8},2,8,1)
clusplot(dataOptics{9},2,9,1)




%% 5. Similarities


%similarity of codas/repertories, relies on the clustering algorithm: if it's the same type gets sim=1, if different cluster sim=0.
%daysim code runs the categorical using the method 3 (spearman correl): update in 2007 Schulz 1-0 similarity ('repsim' matlab code: doe sth emultivariate stuff)
%when run daysim with method=3 gotta call the new repsim by Shane to use Schulz2007 method

%Run twice again: rep of groups and repert days within grups+permutation

% run daysim(spearm=3)
% run simmatrix2 = matrix = mantel


% Absolute ICI, OPTICSxi

% 2.2.1. Encounters
dataCodaOpticsE = load('dataCodaOpticsE.csv');
[simsoutOE, z0001bootOE, z001bootOE, z01bootOE, z1bootOE,out0001OE,out001OE,out01OE,out1OE] = daysim_boot_mc(dataCodaOpticsE,100,3,9,5,0,1);
simAbsOpticsE = daysim(dataCodaOpticsE,3,3,0)
zOE = linkage_sim(simAbsOpticsE(:,3)');
dendrogram_sim(zOE,0);
cophenet(zOE, simAbsOpticsE(:,3)')

% 2.2.2. Days
dataCodaOpticsD = load('dataCodaOpticsD.csv');
[simsoutOD, z0001bootOD, z001bootOD, z01bootOD, z1bootOD,out0001OD,out001OD,out01OD,out1OD] = daysim_boot_mc(dataCodaOpticsD,100,3,9,5,0,1);
simAbsOpticsD = daysim(dataCodaOpticsD,3,3,0)
zOD = linkage_sim(simAbsOpticsD(:,3)');
dendrogram_sim(zOD,0);
cophenet(zOD, simAbsOpticsD(:,3)')

% 2.2.3. Groups
dataCodaOpticsG = load('dataCodaOpticsG.csv');
[simsoutOG, z0001bootOG, z001bootOG, z01bootOG, z1bootOG,out0001OG,out001OG,out01OG,out1OG] = daysim_boot_mc(dataCodaOpticsG,100,3,9,5,0,1);
simAbsOpticsG = daysim(dataCodaOpticsG,3,15,0)
zOG = linkage_sim(simAbsOpticsG(:,3)');
dendrogram_sim(zOG,0);
cophenet(zOG, simAbsOpticsG(:,3)')

%relabeling dendrogram
aux = dataCodaOpticsG;
auxYear = datestr(aux(:,2), 'yy');
auxLab = [str2num(auxYear)*100+ aux(:,3)];
aux(:,3) = auxLab;
newlab = str2num(get(gca,'xticklabel'));
orilab = unique(auxLab);
rejlab = 9;
orilab(rejlab) = [];
corlab = [sort(orilab) sort(newlab)];
label = zeros(size(orilab,1),1);
for g = 1:size(corlab,1)               
    label(newlab==corlab(g,2)) = corlab(g,1);
end
set(gca,'xticklabel',label)



%% 6. CLusplots for categorical repertoires

...
    
% 2.3.3. OPTICSxi, Encounter, cluster=branch with max replicates
...
% 2.3.3. OPTICSxi, Day, cluster=branch with max replicates
...
    
% 2.3.3. OPTICSxi, Group, cluster=branch with max replicates
% extracting the dendrogram branch membership from bootstrapped results
% Picking up the mostly supported branch level (level=0)
[dataClusplotOG membOG breplicOG] = extrMembDendBoot(dataCodaOpticsG,out0001OG,0);
% Exporting data
dlmwrite([outputpath '\dataOpticsClusplotG.csv'], dataClusplotOE,'delimiter',',','precision','%.6f')
% Clusplots with absolute ICIc up to 10-click codas
clusplot(dataClusplotOG,2,1,1)




