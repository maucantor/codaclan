%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014 Acoustic Analysis
% Mauricio Cantor, Dalhousie University, May 2015
%
% CATEGORICAL ANALYSES.
% Eastern Tropical Pacific (ETP):
% Summary:
% 1. Loadings and data organization: ETP, GAL, ALL
% 2. Categorical metric (OPTICSxi): defining coda types
% 3. Handling OPTICSxi outputs: recreating data sets
% 4. RhythmPlots to double check coda type and create meaningful labels
% 5. Discovery curves to evaluate sample size coda type per photoid group
% 6. Clusplots and PCA to visualize final coda types
% 7. Categorical similarities: Spearman correlation and Binary similarity (Tyler/Shane's code)
% 8. Coda types table per group


%% 1. Loadings and Data Organization

% Set input paths:
%Please set your path to the working directory manually
%%% NOTE: If working in Windows do CTRL+F to replace all "/" with "\"! %%%
addpath('Coda_K-means')
addpath('Coda_multivariate')
addpath('GroupSizesPetersen')
addpath('data')
addpath('MATLAB_Galapagos')
addpath('_Cachalote_coredata')
addpath('data/ETP_dataCodas')

% Set output paths
outputpath = [pwd, '/data'];
outputOptics = [pwd, '/OPTICSxi_Elki/data_input'];
outputOptics2 = [pwd, '/OPTICSxi_Elki/data_output'];


% Data input:

%%%% 1.1. ETP DATASET %%%%

%%a. dataCodasETP.codas: coda standard data; dataCodasETP.codas_cols: column name
%for coda data; dataCodasETP.clusternames: coda type labels from k-means.
%dataCodasETP.clanNames: clan names in relation to their numerical labels cols#)
dataCodasETP = load([pwd, '/data/ETP_dataCodas/allcodas_with_types.mat']);

%%b. dataGroupETP.group_assignments: Group assignments = coda#, recording date, group number
%after using groups.m on ids and group_assignments(:,1:2); made groups from
%all q>=3 ids using nab>0.25(min(na,nb)); dataGroupETP.group_date_assignments: 
%date (numerical format) for each photoid group; dataGroupETP.ids: date
%(numerical format) and IDs of members of each photoID group
dataGroupETP = load([pwd, '/data/ETP_dataCodas/group_assignments.mat']);

%%c. group_data: group id, number of codas, %<9 clicks, first day recorded, lat,
%long, # stars for fig, clan number for seven clans (see dataCodasETP.clanNames)
%1/0 genetic data available.
%To find data for a group enter group_data with row#=dendrogram node#
load([pwd, '/data/ETP_dataCodas/groupData.mat']);

%%d. unit_assignments: ETP ID and unit label
load([pwd, '/data/ETP_dataCodas/unit_assignments.mat']);

% Data organization:

% ETP Data with repertoires labeled by Day
dataCodasETPD = dataCodasETP.codas;

% ETP Data with repertoires labeled by Group, defined by photoID
%(codas recorded in the same day = group; codas recorded on different days were in
%the same group if more than 25% of the individuals were present in both)
dataCodasETPG = dataCodasETP.codas;
dataCodasETPG(:,3) = dataGroupETP.group_assignments(:,3);

%%%%%%%%%%%%%%%% REMOVING Caribbean DATA
%carib1 = dataCodasETPG(:,3)==41; % caribbean groups = 41
%dataCodasETPG(carib1,:) = []; %remove
%carib2 = dataCodasETPG(:,3)==58; % caribbean groups = 58
%dataCodasETPG(carib2,:) = []; %remove

%%%%%%%%%%%%%%%% REMOVING LONG CODAS (>= 14 clicks)
%longcodas = dataCodasETPG(:,4)>=14;
%dataCodasETPG(longcodas,:) = [];







%% 2. Categorical metric (OPTICSxi): defining coda types

% preparing ETP data for OPTICSxi based on photoid groups. Three schemes:



%%%% 2.2 ETP DATASET %%%%

% Using the early ETP data to define coda types. From this classification,
%we will prepare a Linear Discriminant Function and use it to fit in the
%new Galapagos coda types. Assume the ETP contains all (or most) coda type
%possible

% First, define a string vector with name of columns in raw data that will
%become the label variables for OPTICSxi. 
colname = {'codaN', 'recN', 'group', 'nClicks', 'kMeans'};

% Now format data
dataCodasToOptics(dataCodasETPG, 2, colname,'ETPG', [outputOptics '/ETP']);

%%If using DAYS instead of photoid groups is preferred:
%colname = {'codaN', 'recN', 'date', 'nClicks', 'kmeans'};
%dataCodasToOptics(dataCodasETPD, 2, colname,'ETPD',outputOptics);






%% OPTICS

%%%% 2.1 ETP DATASET %%%%

% Parameters: xi and minpts:
%Find a reasonable xi across coda lengths. xi=0.025 seems to work well.
%Find a reasonable proportion of codas to define minpts, and use it for all coda lengths. 4% seems to work well (minpts = total_number_of_Nclicks_codas * 0.04)
%Vary xi and minpts to 'extreme', but reasonable, values.  Worked well: xi minimum=0.005 and maximum =0.05; minpts minimum=2%, maximum=8% (but restricting to at least 20 codas and at a maximum 175 codas). 
%Define three schemes to evaluate the parameter space:
%       a) mid xi, mid minpts; b) min xi, max minpts; c) max xi, min minpts. 
%       NB: I could run 9 scenarios combining all these possibilities. But 
%       the maximized xi+minpts tend to give just 1 coda type; and minimized 
%       xi+minpts tend to give way too many coda types. It doesn't seem to make much sense

% Defining xi:
% min = 0.005
% mid =0.025
% max = 0.050

% Defining minpts for each coda length. Percentage of codas used
% min = 2% (bottom = 20)
% mid = 4%
% max = 8% (ceiling = 175)

%number of codas per length
codalength = unique(dataCodasETPG(:,4));
codasample = zeros(size(codalength,1),1);
for i=1:size(codalength,1)
   codasample(i) = length(find(dataCodasETPG(:,4)==codalength(i))); 
end

%applying min=2%, mid=4%, max=8%. Restrict ceiling=175, floor=20
mpts = [codasample*.02 codasample*.08]
ceiling = mpts(:,2)>175;
mpts(ceiling,2)=175; 
floor = mpts(:,1)<20;
mpts(floor,1)=20;
format shortg
minpts = [codalength(1:10) codasample(1:10) mpts(1:10,1) codasample(1:10)*.04 mpts(1:10,2)]



%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RUN OPTICS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







%% 3. Handling OPTICSxi outputs

% Input data from OPTICSxi defining coda types for each coda length
% Choose all txt files from the OPTICS output folder. Take all
%'cluster_XX.txt' files for a particular coda length and the function will
%organize into a single dataCoda format, with +46th with Optics cluster label (aka
%the coda type)

% Repertoires among PhotoID Groups

%%%% 3.1 ETP DATASET %%%%

% 3.2.1. loop up to 12-click codas:
nCodalength = unique(dataCodasETPG(:,4));

% Scheme1: low xi, high minpts
for m=1:find(nCodalength==12)
    disp(' ');
    disp(['Now processing: ETP dataset, low xi high minpts, ' num2str(nCodalength(m)) '-click codas:']);
    disp(' ');
    OpticsToDatacodas('dataOpticsETPG',nCodalength(m),[outputOptics2 '/ETP/lowxi_higmp'],0);
    disp(' ');
end

% Scheme 2: mid xi, mid minpts
for m=1:find(nCodalength==12)
    disp(' ');
    disp(['Now processing: ETP dataset, mid xi mid minpts, ' num2str(nCodalength(m)) '-click codas:']);
    disp(' ');
    OpticsToDatacodas('dataOpticsETPG',nCodalength(m), [outputOptics2 '/ETP/midxi_midmp'],0);
    disp(' ');
end

% Scheme 3: high xi, low minpts
for m=1:find(nCodalength==12)
    disp(' ');
    disp(['Now processing: ETP dataset, high xi low minpts, ' num2str(nCodalength(m)) '-click codas:']);
    disp(' ');
    OpticsToDatacodas('dataOpticsETPG',nCodalength(m), [outputOptics2 '/ETP/higxi_lowmp'],0);
    disp(' ');
end
    

% 3.3.2. Get all the csv files for each coda size (up to 12) and merge into a single final file
% Select all 'dataOpticsETP***' csv files files in
% low xi, high minpts
dataCodaOpticsETPG_lxhm = combineCsv('dataCodaOpticsETPG_lxhm', [outputOptics2 '/ETP/lowxi_higmp']);
% mid xi, mid minpts
dataCodaOpticsETPG_mxmm = combineCsv('dataCodaOpticsETPG_mxmm', [outputOptics2 '/ETP/midxi_midmp']);
% high xi, low minpts
dataCodaOpticsETPG_hxlm = combineCsv('dataCodaOpticsETPG_hxlm', [outputOptics2 '/ETP/higxi_lowmp']);


%%%%%%%%%%%% NOTE %%%%%%%%%%%%%%
% in 2. we set up the k-means label column as an input for Optics. So on the
% way back, OpticsToDatacodas.m included kmeans codatypes in the 5th col. Now we
% have to remove it from the final dataCoda files, to avoid confusion with
% the Optics coda types
dataCodaOpticsETPG_lxhm(:,5) = [];
dataCodaOpticsETPG_mxmm(:,5) = [];
dataCodaOpticsETPG_hxlm(:,5) = [];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% 3.3.3. Dealing with >=13-click codas: assign a single coda type for each
longCodas = dataCodasETPG(dataCodasETPG(:,4)>=13, :);
longCodasSizes = nCodalength(nCodalength>=13)';
% assigning coda types
for c=longCodasSizes
    longCodas(longCodas(:,4)==c,46) = str2num(strcat(num2str(c),num2str(1)));
end


% 3.1.4. Append longCodas to dataOptics and sort by coda number:
dataCodaOpticsETPG_lxhm = [dataCodaOpticsETPG_lxhm; longCodas];
dataCodaOpticsETPG_mxmm = [dataCodaOpticsETPG_mxmm; longCodas];
dataCodaOpticsETPG_hxlm = [dataCodaOpticsETPG_hxlm; longCodas];

dataCodaOpticsETPG_lxhm = sortrows(dataCodaOpticsETPG_lxhm,1);
dataCodaOpticsETPG_mxmm = sortrows(dataCodaOpticsETPG_mxmm,1);
dataCodaOpticsETPG_hxlm = sortrows(dataCodaOpticsETPG_hxlm,1);


% 3.1.5. FINAL fils with categorical coda types by OPTICs: save and export
dlmwrite([outputpath '/dataCodaOpticsETPG_lxhm.csv'], dataCodaOpticsETPG_lxhm,'delimiter',',','precision','%.6f');
dlmwrite([outputOptics2 '/ETP/lowxi_higmp/dataCodaOpticsETPG_lxhm.csv'], dataCodaOpticsETPG_lxhm,'delimiter',',','precision','%.6f');

dlmwrite([outputpath '/dataCodaOpticsETPG_mxmm.csv'], dataCodaOpticsETPG_mxmm,'delimiter',',','precision','%.6f');
dlmwrite([outputOptics2 '/ETP/midxi_midmp/dataCodaOpticsETPG_mxmm.csv'], dataCodaOpticsETPG_mxmm,'delimiter',',','precision','%.6f');

dlmwrite([outputpath '/dataCodaOpticsETPG_hxlm.csv'], dataCodaOpticsETPG_hxlm,'delimiter',',','precision','%.6f');
dlmwrite([outputOptics2 '/ETP/higxi_lowmp/dataCodaOpticsETPG_hxlm.csv'], dataCodaOpticsETPG_hxlm,'delimiter',',','precision','%.6f');









%% 4. Rhythm plots for coda types defined by OPTICS

% Coda rhythm plots to diagnose if the OPTICSs clusters are biologically meaningful


%%%% 4.1 ETP DATASET %%%%

% Coda types: the original K-means outputs in Rendell & Whitehead 2003
CodaRhythmPlots(dataCodasETPG, 8, 'absolute')


%%4.2.1. OPTICS mid xi, mid minpts%%

% Exploratory: All coda types and noise
CodaRhythmPlots(dataCodaOpticsETPG_lxhm, 29, 'absolute')
CodaRhythmPlots(dataCodaOpticsETPG_lxhm, 29, 'standardized')

%removing noise coda types (#99)
dataCodaOpticsETPG_lxhmN = rmNoise(dataCodaOpticsETPG_lxhm);
%Rhythm plot for up to 12-click codas
CodaRhythmPlots(dataCodaOpticsETPG_lxhmN, 12, 'absolute')



%%4.2.2. OPTICS low xi, high minpts%%

% Exploratory: All coda types and noise
CodaRhythmPlots(dataCodaOpticsETPG_mxmm, 29, 'absolute')
CodaRhythmPlots(dataCodaOpticsETPG_mxmm, 29, 'standardized')

%removing noise coda types (#99)
dataCodaOpticsETPG_mxmmN = rmNoise(dataCodaOpticsETPG_mxmm);
%Rhythm plot for up to 12-click codas
CodaRhythmPlots(dataCodaOpticsETPG_mxmmN, 12, 'absolute')



%%4.2.3. OPTICS high xi, low minpts%%

% Exploratory: All coda types and noise
CodaRhythmPlots(dataCodaOpticsETPG_hxlm, 29, 'absolute')
CodaRhythmPlots(dataCodaOpticsETPG_hxlm, 29, 'standardized')

%removing noise coda types (#99)
dataCodaOpticsETPG_hxlmN = rmNoise(dataCodaOpticsETPG_hxlm);
%Rhythm plot for up to 12-click codas
CodaRhythmPlots(dataCodaOpticsETPG_hxlmN, 12, 'absolute')






%% 5. Discovery Curves: categorical coda types per photoID group



%%%% 5.2 ETP DATASET %%%%
%%5.2.1. OPTICS mid xi, mid minpts%%
codaDiscovery(dataCodaOpticsETPG_lxhmN, 3, 0);

%%5.2.2. OPTICS low xi, high minpts%%
codaDiscovery(dataCodaOpticsETPG_mxmmN, 3, 0);

%%5.2.3. OPTICS high xi, low minpts%%
codaDiscovery(dataCodaOpticsETPG_hxlmN, 3, 0);






%% 6. Clusplots and/or PCAs for each coda type length

% Take the OPTICS classification into clusplot, to summarize into 2 PCs
% Visualize coda types in 2D, with or without noise

%%%% 6.2 ETP DATASET %%%%

%%6.2.1. OPTICS mid xi, mid minpts%%

% 6.2.1.4. Principal Component Analysis (alternative plotting, with noise)

% PCAs on ICIs for each coda length (up to 11-click codas), without noise
codaPCA(dataCodaOpticsETPG_lxhmN, 3, 11)

% PCAs on ICIs for each coda length (up to 11-click codas), with noise (9)
codaPCA(dataCodaOpticsETPG_lxhm, 3, 11)




%%6.2.1. OPTICS low xi, high minpts%%

% PCAs on ICIs for each coda length (up to 11-click codas), without noise
codaPCA(dataCodaOpticsETPG_mxmmN, 3, 11)

% PCAs on ICIs for each coda length (up to 11-click codas), with noise (9)
codaPCA(dataCodaOpticsETPG_mxmm, 3, 11)




%%6.2.3. OPTICS high xi, low minpts%%

% PCAs on ICIs for each coda length (up to 11-click codas), without noise
codaPCA(dataCodaOpticsETPG_hxlmN, 3, 11)

% PCAs on ICIs for each coda length (up to 11-click codas), with noise (9)
codaPCA(dataCodaOpticsETPG_hxlm, 3, 11)







%% 7. Similarity of categorical repertoires


%%%% 7.2 ETP DATASET %%%%

%%7.2.1. OPTICS low xi, high minpts%%
%%7.2.2. OPTICS mid xi, mid minpts%%
%%7.2.3. OPTICS high xi, low minpts%%





%% 8. Coda types table per group

% Categorical coda repertoires per photoID group: number of coda types
% (table in Fig2 Rendell & Whitehead 2003)

%Come up with biologically-meaningful labels
%Match with Rendell & Whitehead 2003 (note that most 'noise' was removed!)

%%%%%%%% add 'RowLabels' with meaningful coda types given by RhythmPlot %%%%%%%%



%%%% 8.2 ETP DATASET %%%%

% Original K-means coda types

tabKmeans = crosstab(dataCodasETPG(:,46), dataCodasETPG(:,3));
tabKmeansRelat = relativeTable(tabKmeans, 'column');

HeatMap(tabKmeansRelat, 'RowLabels', unique(dataCodasETPG(:,46)), ...
    'ColumnLabels', unique(dataCodasETPG(:,3)), 'Colormap', 'redbluecmap')

aux = unique(dataCodasETPG(:,46));
HeatMap(tabKmeansRelat(1:31,:), 'RowLabels', aux(1:31), ...
    'ColumnLabels', unique(dataCodasETPG(:,3)), 'Colormap', 'redbluecmap')



%%8.2.1. OPTICS mid xi, mid minpts%%
%%8.2.2. OPTICS low xi, high minpts%%
%%8.2.3. OPTICS high xi, low minpts%%