function similsmatrix = simmatrix(simils)
%function similsmatrix = simmatrix(simils)
%
%function takes output similarities (simils) from daysim and makes it into
%a similarity matrix
%
%INPUT:
%'simils': You need to have run the daysim similarity using any of the three
%   methods: 1 - euclidean, 2 - infinity-norm or 3-categorical. This
%   function assumes you are intersted in b=0.001 similarities and only
%   takes the data from column 3 in the simils output.
%
% Shane Gero, modified by Mauricio Cantor to deal with repertoire labels
% that are not sequential

originalLabel = unique(simils(:,1:2));
nreps = size(originalLabel,1);
newLabel = [1:nreps]';
labels = [originalLabel newLabel];

R = simils(:,1);
C = simils(:,2);
V = simils(:,3);%This line takes b=0.001 so must change value depending on interest in b
%V = simils(:,b);% the 'b' could be added as an input parameter
%the b-value in simils.m. b=3 for 0.001, 4 for 0.01, 5 for 0.1, 6 for 1 

labelR = zeros(size(simils,1),1);
labelC = zeros(size(simils,1),1);
for g = 1:size(labels,1)               
    labelR(R(:,1)==labels(g,1)) = labels(g,2);
    labelC(C(:,1)==labels(g,1)) = labels(g,2);
end

Halfmatrix = accumarray([labelR labelC],V);
Halfmatrix = [Halfmatrix;zeros(1,nreps)];
similsmatrix = Halfmatrix+Halfmatrix'+eye(nreps);

%disp('Matrix row and columns are organized by the following labels')
%disp('The first column gives the original, the second column gives its numerical counterpart')
%labels
end

