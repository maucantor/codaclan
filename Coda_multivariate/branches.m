function [obranches] = branches(Z,nbd)   

n = size(Z,1)+1; %number of data nodes
m = size(Z,1);
obranches = [];

for x = 1:m-1
    T = zeros(n,1);
    T = clusternum(Z,T,1,x);
    nodeso = find(T);
    obranches(x,1:size(nodeso,1)) = nodeso';
%    disp(['Branch ' num2str(x) ' contains nodes ' num2str(nodeso')]);
end
obranches = flipud(obranches);
obranches = obranches(1:nbd,:);