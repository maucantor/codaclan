function [sim] = repsim(rep1,rep2,method,standardise,varargin)
%function [sim] = repsim(rep1,rep2,method,standardise, [global_types])
%calculates similarity between two repertoires, rep1 rep2, where repertoires provided in the form of
%coda# nclicks length(s) 1 2 3 4 5 6 up to 40 icis
%function returns average of similarities between rep1 and rep2
%discount factor = comparisons of different lengths = 0
%similarity = b/b+distance where b = 0.001,0.01,0.1 and 1
%method : 1 = euclidean distance with ALL CLICK INTERVALS, 2 =
%infinity-norm distance with all click intervals
%3 = spearman correlations - NB in this case repertoires are in the form:
%coda#, clusterID; also requires last argument to be a vector containging 
% a global numeric list of coda types - 
% comparisons are only valid between similarities calculated using same global list 
%returns the same value in each of the four similarity columns 
%(for compatibility with other routines)
%
% standardise = True(1) for relative ICIs, False(0) for absolute

switch method
    case 3 %if spearman...
        clusters  = varargin{1};  
        nclust = length(clusters);
        %create matrix with repertoires
        for x = 1:nclust % loop thru coda clusters
            reps(1,x) = sum(rep1(:,2)==clusters(x)); % n codas of each type from rep1
            reps(2,x) = sum(rep2(:,2)==clusters(x)); % n codas of each type from rep2
        end
        %reps
        [corr] = rankcorr(reps');%calculate correlations
        sim = repmat(corr(1,2),1,4); %similarities here just the same repeated four times....


    otherwise %if not...
        tmat = [];
        results = [];
        tcodas = [];
        coda = [];
        test1 = [];
        maxes = [];
        sim =[];
        runsum = zeros(1,6);
        nz = 0;
        %get numbers of codas in each repertoire
        numcod1 = length(rep1(:,1));
        numcod2 = length(rep2(:,1));
        for r1 = 1:numcod1 %loop through first repertoire
            %disp(num2str(r1));
            codan = rep1(r1,1); %get coda number,number of clicks
            numcl = rep1(r1,2);
            coda = rep1(r1, 4:(numcl+2)); %get coda
            for x = 3:max(rep2(:,2));%loop through coda lengths in rep2
             
                tcodas = rep2(rep2(:,2)==x,1:x+2);%get codas in rep2 same length as x
                tcodaindex = size(tcodas,1)+1;
                if sum(any(tcodas)) & (x==numcl);%if any codas x long and x=test coda length
                    tmat(:,1) = tcodas(:,1);
                    test1 = tcodas(:,4:3+length(coda));%set up test matrix
                    test1(tcodaindex,:) = coda;%put rep1 coda on the end
                    if standardise
                        lengths = sum(test1,2);%set up matrix to standardise icis
                        lengths = lengths(:,ones(length(coda),1));
                        test1 = test1./lengths;%standardise icis
                        lengths = []; %clear lengths
                    end
                    switch method
                        case 1
                            tmat(:,2) = ldist(test1)'; %get euclidean distances to each coda
                        case 2
                            tmat(:,2) = normdist(test1)'; %get infinity-norm distances to each coda
                    end
                    if any(isnan(tmat))
                        disp(['NaN occurs on coda ' num2str(codan)]);
                    end
                    test1 = [];%clear test1 matrix   
                    b = size(results,1)+1:size(results,1)+size(tmat,1);
                    results(b,1) = (0.001./(0.001+tmat(:,2)));
                    results(b,2) = (0.01./(0.01+tmat(:,2)));
                    results(b,3) = (0.1./(0.1+tmat(:,2)));
                    results(b,4) = (1./(1+tmat(:,2)));
                    tmat = [];
                    nz = nz + size(tcodas,1);%update number of non-zeros
                elseif sum(any(tcodas))%if there are any codas x long and x<>test coda length
                    b = size(results,1)+1:size(results,1)+size(tcodas,1);
                    results(b,1) = 0;
                    results(b,2) = 0;
                    results(b,3) = 0;
                    results(b,4) = 0;
                end
                tcodas = [];
            end
            
            %disp([num2str(r1) num2str(size(results))]);
            runsum(1,1:4) = runsum(1,1:4)+sum(results(:,1:4));%get running sum to avoid huge results matrix
            results = [];%clear results matrix
        end
        sim(1,1:4) = runsum(1,1:4)./(numcod1*numcod2); %calculate means from summed distances
end
