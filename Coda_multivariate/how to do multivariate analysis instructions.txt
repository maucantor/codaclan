Multivariate instructions:

Here are some coda analysis routines, they are all written by me except
for the dendrogram_sim and linkage_sim functions which are versions of the matlab functions dendrogram and linkage from the stats toolbox that I have modified to work with similarity rather than distance data.

These routines calculate similarities between repertoires like so; you
need to give them the coda data, which method of similarity you want
to use and what the minimum repertoire size to be included is:

simils = daysim(codas,met,minsize)
%type help daysim for info on what codas, met and minsize are

simils is then a matrix with
group1 group2 sim(b=0.001) sim(b=0.01) sim(b=0.1) sim(b=1)

except when spearman correlations of coda types are selected in which
case the last four columns are identical.

There is a line in the 'simils' matrix for all possible comparisons between the repertoires defined by the grouping variable in column three of the codas input. You can then produce a dendrogram from those similarities with the syntax:

z = linkage_sim(simils(:,3)'); % for b = 0.001
dendrogram_sim(z); %you may need to tell the function to display all
nodes as it only displays 30 by default

The other function is daysim_boot which bootstraps the similarities:
[simsout, z0001boot, z001boot, z01boot, z1boot] =
daysim_boot(codas,nboots,met,minsize, nbranchdown);

Here the new inputs are nboots (number of bootstrap resamples within
repertoire) and nbranchdown - the number of dendrogram branches
counting from the top that you want the routine to produce bootstrap
support for - if you put in more than there are branches there will be
a crash! The output should be self explanatory but beware with 1000+
bootstraps the analyses can take a lonnnnnngggg tiiiiiimmmme, code
optimisation not being my strongpoint! 