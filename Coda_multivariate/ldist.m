function Y = ldist(X)
%adaptation of pdist to calculate distances between last row of x and all others

[m, n] = size(X);


I(1:m-1,1) = m;
J = (1:1:m-1)';

Y = (X(I,:)-X(J,:))';
I = []; J = []; p = [];  % no need for I J p any more.
Y = sum(Y.^2,1);
Y = sqrt(Y);