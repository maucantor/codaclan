function [simsout, z0001boot, z001boot, z01boot, z1boot, out0001,out001,out01,out1] = daysim_boot(codas,nboots,met,minsize,nbranchdown,standardise,print);

%function [simsout, z0001boot, z001boot, z01boot, z1boot, out0001,out001,out01,out1] = 
%   daysim_boot(codas,nboots,met,minsize,nbranchdown,standardise,print);
%
%function calculates bootstrap similarity matrices between repertoires of different units
%bootstraps are sampled with replacement from within repertoires...
%then automatically converts to linkage 'z' data...
%
% INPUT:
%'codas': coda# recording GROUPING VARIABLE nclicks length(s) 1 2 3 4 5 6 up to 40
%get codas into numeric matrix
%'nboots': number of bootstrap samples
%'met' : 1 = euclidean distance with all click intervals, 2 = infinity-norm distance
%3 = spearman correlations - requires coda type in column 46 and returns the
%same value in each of the four similarity columns (for compatibility with
%other routines)
%'minsize' = minimum number of codas to be included (usually 25)
%'nbranchdown' = number of branches to display bootstrap repetitions...
%'standardise' = True(1) for relative ICIs, False(0) for absolute
%'print': if=1 returns a matrix with branch (c1), times this branch was reproduced (c2)
%and the members of this branch (c3 on)
%
%OUTPUT:
%'simsout' is the original similarity data
%'z****boot' are the bootstrapped linkage data, for each b-parameter
%'out***1' = the output matrix, if 'print'=1
%
% Luke Rendell, updated in Jan 2015 by Mauricio Cantor


%Get only those codas with assigned repertoire
codas = codas(codas(:,3)>0,:);
ncodas = size(codas,1);

%find number of repertoires
days = unique(codas(:,3));
ndays = size(days,1);%get number of repertoires

%get rid of repertoires with less than minsize coda
for un = 1:ndays

    if sum(codas(:,3)==days(un))<minsize
        disp(['Day ' num2str(days(un)) ' rejected, less than ' num2str(minsize) ' codas']);
        codas = codas(codas(:,3)~=days(un),:);
    end
end

disp('Getting original similarities'); 

simsout = daysim(codas,met,minsize,standardise); %% GET SIMILARITIES

bootsamps = [];

for d = 1:ndays; %loop though days
    
    dayindexes = find(codas(:,3)==days(d)); %% find indexes of that day
    ndayindexes = size(dayindexes,1); %% find number of indexes
    
    dayboots = unidrnd(ndayindexes,ndayindexes,nboots); %% get bootstrap samples of that day
    daybootindexes = dayindexes(dayboots); %% convert to indices into codas...
    bootsamps = [bootsamps;daybootindexes]; %% append to bootstrap sample indices matrix
    
end %% now have bootstrap indices

disp('Bootstrapping...');

for b = 1:nboots %now bootstrap
    disp(num2str(b));
    bootsims = daysim(codas(bootsamps(:,b),:), met, minsize, standardise); %% get similarity data for each bootstrap sample...
    z0001boot(:,:,b) = linkage_sim(bootsims(:,3)','average'); %% convert these to linkages...
    z001boot(:,:,b) = linkage_sim(bootsims(:,4)','average');
    z01boot(:,:,b) = linkage_sim(bootsims(:,5)','average');
    z1boot(:,:,b) = linkage_sim(bootsims(:,6)','average');
end

%Now use bootcompz to display number of times each branch repeated for each b....

save last_analysis.mat

zin = linkage_sim(simsout(:,3)','average');
disp(' ');
disp('Results for b = 0.001');
disp(' ');
out0001 = bootcompz_mc(zin,z0001boot,nbranchdown,print);

zin = linkage_sim(simsout(:,4)','average');
disp(' ');
disp('Results for b = 0.01');
disp(' ');
out001 = bootcompz_mc(zin,z001boot,nbranchdown,print);

zin = linkage_sim(simsout(:,5)','average');
disp(' ');
disp('Results for b = 0.1');
disp(' ');
out01 = bootcompz_mc(zin,z01boot,nbranchdown,print);

zin = linkage_sim(simsout(:,6)','average');
disp(' ');
disp('Results for b = 1');
disp(' ');
out1 = bootcompz_mc(zin,z1boot,nbranchdown,print);
