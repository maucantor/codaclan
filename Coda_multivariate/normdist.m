function Y = normdist(X)
%calculates maximum absolute distances between last row of x and all others

[m, n] = size(X);


I(1:m-1,1) = m;
J = (1:1:m-1)';

Y = abs((X(I,:)-X(J,:))'); %get absolute differences
clear I J p; % no need for I J p any more.
Y = max(Y,[],1); %get maximum differences
