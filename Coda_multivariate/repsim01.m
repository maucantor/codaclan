function [sim] = repsim01(rep1,rep2,met)
%[sim] = repsim01(rep1,rep2,met)
%
%Calculates similarity between two coda repertoires.
%This modified version of Luke's routine does NOT look at the similarity
%between codas of the same type. Instead, if two codas are the same type
%(as determined previously) then they are given a similarity of 1.
%If they are different coda types, then they are given a similarity of 0.
%
% INPUT:
%'rep1', 'rep2': coda repertoires
%'met': 1 - euclidean, 2 - infinity-norm or 3-categorical.
%
% Luke Rendell, modified by Shane Gero

        results = [];
        sim =[];

        b=1;
        %get numbers of codas in each repertoire
        numcod1 = length(rep1);
        numcod2 = length(rep2);
        for r1 = 1:numcod1 %loop through first repertoire
            coda1=rep1(r1,2); % get coda from first repertoire of coda types
            
            for r2 = 1:numcod2 % loop through second repertoire
                coda2=rep2(r2,2);
                    if coda1 == coda2 % if they are the SAME type (as determined by VRC)
                        results(b,1)=1;
                        b=b+1;
                    elseif coda1 ~= coda2 %if the are NOT the SAME type
                        results(b,1)=0;
                        b=b+1;
                    end
            end %loop through second repertoire
        end %loop through first repertoire
                      
        runsum=sum(results);            
        results = [];%clear results matrix
        sim(1,1:4) = runsum/(numcod1*numcod2); %calculate means from summed distances
%end
