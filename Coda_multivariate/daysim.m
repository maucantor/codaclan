function [simils rejected] = daysim(codas,met,minsize,standardise)
%function [simils rejected] = daysim(codas,met,minsize,standardise)
%
%function produces columns of similarites between different repertoires
%(groups of codas)
%
%INPUT:
%'codas': standard dataCodas format with repertoires in the 3rd column:
%coda# recording GROUPINGVARIABLE nclicks length(s) 1 2 3 4 5 6 up to 40
%'met':  similarity method 1=euclidian distance of all ICIs, 2=infinity norm
%distance,3 = spearman correlations - requires coda type in column 46 and returns the
%same value in each of the four similarity columns (for compatibility with
%other routines)
%'minsize' = minimum number of codas to be included
%'standardise' = True(1) for relative ICIs, False(0) for absolute
%
%OUTPUT:
%'simils': standard similarity output (col1: rep, col2: rep2, col3-on:similarity
%THIS VERSION PRODUCES A SIMILIARITY MATRIX OF b=0.001 SIMILARITIES ONLY!!!!
%'similsmat': same similarities for b=0.001, organized in matrix format, as
%given by the modified function 'simmatrix.m'
%'reject': column with the labels of the rejected days/repertoires/groups
%
% Luke Rendell, modified by Mauricio Cantor


%Get only those codas with assigned repertoire
codas = codas(codas(:,3)>0,:);

%find number of repertoires
days = unique(codas(:,3));
ndays = size(days,1);%get number of repertoires

%get rid of repertoires with less than minsize coda
rejected={};

for un = 1:ndays
    if sum(codas(:,3)==days(un))<minsize
        disp(['Day ' num2str(days(un)) ' rejected, less than ' num2str(minsize) ' codas']);
        %save rejected day/repertoire/group label
        rejected{un} = days(un);
        codas = codas(codas(:,3)~=days(un),:);
    end
end

%find new number of repertoires
days = unique(codas(:,3));
ndays = size(days,1);%get number of repertoires

if met==3 %if spearman get a global list of coda types so correlations are comparable
    global_types = unique(codas(:,46))
end

[i,j] = find(triu(ones(ndays)).*~eye(ndays));%make indices for comparison matrix

flag25 = 0;
flag50 = 0;
flag75 = 0;
for n = 1:max(size(i));%loop through indices
    switch met
        case 3 %if spearman
            rep1 = [codas(codas(:,3)==days(i(n),1),1) codas(codas(:,3)==days(i(n),1),46)];%get rep1 as day i(n)
            rep2 = [codas(codas(:,3)==days(j(n),1),1) codas(codas(:,3)==days(j(n),1),46)];%get rep2 as day j(n)
        otherwise
            rep1 = [codas(codas(:,3)==days(i(n),1),1) codas(codas(:,3)==days(i(n),1),4:45)];%get rep1 as day i(n)
            rep2 = [codas(codas(:,3)==days(j(n),1),1) codas(codas(:,3)==days(j(n),1),4:45)];%get rep2 as day j(n)
    end
    if n>max(size(i))*0.25 & flag25==0 %% 25% done
        disp('25% done');
        flag25=1;
    end
    if n>max(size(i))*0.50 & flag50==0 %% 25% done
        disp('50% done');
        flag50=1;
    end
    if n>max(size(i))*0.75 & flag75==0 %% 25% done
        disp('75% done');
        flag75=1;
    end
    disp([num2str(n) ' Testing ' num2str(days(i(n))) ' vs ' num2str(days(j(n)))]);
    switch met
        case 3 %if spearman
            simx = repsim(rep1,rep2,met,standardise,global_types);
        otherwise
            simx = repsim(rep1,rep2,met,standardise);
    end
    simils(n,:) = [days(i(n),1) days(j(n),1) simx(1,:)];% fill in similarities
    rep1 = [];
    rep2 = [];
    sub = [];
end

rejected = cell2mat(rejected');

simils = sortrows(simils,1);
