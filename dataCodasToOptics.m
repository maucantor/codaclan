function dataCodasToOptics(codas, datatype, colname, filename, outputpath)
%
%function [dataOptics] = dataCodasToOptics(codas, outputpath)
%Splits dataCodas into csv files formatted for OPTICSxi, one file per coda length
%
% Input parameters:
%
% 'codas' = coda data;
%
% 'datatype' = [1 2 3]; 1: Standard format; 2: Standard ETP format, with k-means
%coda types; 3: Mauricio's adapted format.
% 1: 45 cols with headers:coda#,recording#,date(MS Access),nClicks,total_length,ICI1-40
% 2: 46 cols with headers:coda#,recording#,date(MS Access),nClicks,total_length,ICI1-40,k-means coda type
% 3: 46 cols with headers:coda#,datetime(MATLAB),repertoire_label,nClicks,total_length,ICI1-40,branch_label (exported by the function 'exprMembDendBoot.m')
%
% 'colname' = string vector with name of columns in raw data that will
%become the label variables for OPTICSxi. For instance:
%datatype = 1 --> colname = {'codaN', 'recN', 'date', 'nClicks'}
%datatype = 2 --> colname = {'codaN', 'recN', 'date', 'nClicks', 'kmeans'}
%datatype = 3 --> colname = {'codaN', 'date', 'repLabel', 'nClicks', 'branch'}
%
% 'outputpath': Path for exporting csv files
%
%' filename': string value to distinguish files; e.g. "ETP", "GAL", etc.
%
% Outputs:
%
% One csv file per coda length with ICI of codas and categorical variables:
%ICI1-x, branch (defined by bootstrapping dendrogram), coda#, date (numerical 
%format), repLabel (usually encounter, day or photo-ID group)
%
%
% Mauricio Cantor, Jan 2015; Modified May 2015


    
%deleting total coda length time (col5: common to all formats)
codas(:,5) = [];

%sorting data by number of clicks in codas (col4: common to all formats)
codas = sortrows(codas,4);

%placing label variables at the end, so first cols contain the ICIs
%col1-40 = ICI1-40; col41-45 or 46 will take the other colum labels (given by 'colname')
if datatype == 2 | datatype == 3 
    codas = [codas(:,5:(end-1)) codas(:,1:4) codas(:,45)];
 
    %transforming label variables into strings (required by Optics)
    %column names defined by the used will be the label variables: 'colname'
    col41={};
    col42={};
    col43={};
    col44={};
    col45={};
    for i=1:(size(codas,1))
        col41{i} = strcat(colname{1}, '=', num2str(codas(i,41)));
        col42{i} = strcat(colname{2}, '=', num2str(codas(i,42)));
        col43{i} = strcat(colname{3}, '=', num2str(codas(i,43)));
        col44{i} = strcat(colname{4}, '=', num2str(codas(i,44)));
        col45{i} = strcat(colname{5}, '=', num2str(codas(i,45)));
    end

    %replacing
    codascell=[num2cell(codas(:,1:(end-5))) col41' col42' col43' col44' col45'];

else %datatype=1
    
    codas = [codas(:,5:(end)) codas(:,1:4)];
    
    col41={};
    col42={};
    col43={};
    col44={};
    for i=1:(size(codas,1))
        col41{i} = strcat(colname{1}, '=', num2str(codas(i,41)));
        col42{i} = strcat(colname{2}, '=', num2str(codas(i,42)));
        col43{i} = strcat(colname{3}, '=', num2str(codas(i,43)));
        col44{i} = strcat(colname{4}, '=', num2str(codas(i,44)));
    end

    %replacing
    codascell=[num2cell(codas(:,1:(end-4))) col41' col42' col43' col44'];
 
end
    
    
%split into submatrices with same nClicks (44th col: nClicks)
codaLength = codas(:,44);
uniqueCodaLength = unique(codaLength);
numbCodaLength = histc(codaLength, unique(codaLength));
subCodas = mat2cell(codascell, numbCodaLength, size(codascell,2));

%remove zeroed ICI columns and split rule (44th: nClicks)
final={};
   for j=1:size(subCodas,1)
        aux = subCodas{j};
        %the columns to select: (1:(uniqueCodaLength(j)-1), 41:end-1))
        aux2 = aux(:,[1:(uniqueCodaLength(j)-1) 41:(end)]);
        final{j} = aux2;
        % exporting csv for OPTICS
        cell2csv([outputpath '/dataOptics_' filename '_' num2str(uniqueCodaLength(j)) 'clickcoda.csv'], final{j}, ',');
   end
    
% display message
disp(' ');
disp([num2str(size(subCodas,1)) ' csv files were exported to: ']);
disp(outputpath);


end