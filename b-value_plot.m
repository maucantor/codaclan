
%Plto to evaluate the effect of b-value on the conversion of distance to a
%similarity metric, both using Infinity norm and Euclidean distance.
%
% by Luke Rendell

%code assumes codas are in workspace variable called 'codas' with columns:
%coda# nclicks length(s) 1 2 3 4 5 6 up to 40 icis allDists = []; 

for length = 3:10; %loop through coda lengths
    tcodas = codas(codas(:,2)==length,1:length+2);%get codas in rep2 same length as x
    disp([num2str(length) ' clicks - ' num2str(size(tcodas,1)) ' codas']);
    d = pdist(tcodas(:,4:end),'chebychev'); % get infinity norm distances
    allDists = [allDists d]; %add results to vector 
end

 edges = [0:0.02:2];
 bX = [0:0.001:1];
 h = bar(edges,histc(allDists,edges)./size(allDists,2),'histc');
 set(h, 'FaceColor',[0.9 0.9 0.9])
 hold on

for b=[0.001 0.01 0.1 1]
    s = b./(b+bX);
    plot(bX,s,'k-','Linewidth',2)
end
 
 set(gca,'Xlim',[0 1])
 xlabel('Distance')
 ylabel('Similarity / proportion of distances')