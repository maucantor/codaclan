function savmatn(qqk,unmem,key,fi)
%saves association matrix in VNA (network) form
inds=find(sum(qqk+qqk'))';
filesave=fi;
fid=fopen(filesave,'w');
rnind=length(inds);
fprintf(fid,'*Node data\n');%node data
fprintf(fid,'ID unit key\n');
for j=1:rnind
    fprintf(fid,'%5.0f %3.0f\n',inds(j),key(inds(j)));
end
fprintf(fid,'*Tie data\n');
fprintf(fid,['from to companions\n']);
[I,J]=find(qqk-diag(diag(qqk)));
for i=1:length(I)
    if ~isnan(qqk(I(i),J(i)))
        fprintf(fid,'%5.0f %5.0f 1\n',I(i),J(i));
    end
end
fclose(fid);

