%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014  Analysis
% Mauricio Cantor, Dalhousie University, May 2015
%
% Defining social units using photo-ID data
%
%%%% Get the data from the root folder 
%datapath = [pwd '\data\Females_gal1314.xls']; %only females from Galapagos, 2013-14
%datapath = [pwd '\data\Females_gal.xls']; %only females from Galapagos, all years
%datapath = [pwd '\data\Female.xls']; %all females from ETP, all years

%%%% Set up the right directory for this function
%addpath([pwd '\SocialUnit_analyses']);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function defineunits_gal1314(datapath)

% Original parameter values:
% defines units using the following definitions:
%Key individuals 
%	have been seen at least 4 (keydd) times (days)
%	sightings have at least a 30 (keygap) day gap
%	Quality at least 3 (minQ)
%Constant Companions
%	Have been seen with (same encounter) key individuals at least 3 (ccdd)
%	times OMITTED
%	sightings have at least a 30 (ccgap) day gap between them
%	Quality at least 3 (minQ)
%Units
%   Key members seen together at least (cuc) 4 times, with 30-day (keygap) gaps
%   Others who must be companions of at least two (minkeyunit) key individuals

% Lower parameters for the shorter dataset GAL1314
keydd=2;%min number of times to be seen to be key
keygap=10;%min gap between sightings of key individuals
minQ=3;%minimum Q-value to be included
ccdd=2;%minimum times with key individuals to be companions
ccgap=10;%min gap between sightings for companions
cuc=2;%number of times key individuals seen together for same unit
minkeyunit=2;%Minimum number of keys in unit

[A,B]=xlsread(datapath);% Data input

Q=A(:,5);%Q=A(:,2);
dat=A(:,11);%dat=datenum(A(:,1),'dd/mm/yyyy HH:MM:SS');
good=find(Q>=minQ);
ID=A(good,6);%ID=A(good,3);
%enc=A(good,2);
dat=floor(dat(good));
af=unique(ID); 
af(isnan(af(:,1)),:)=[]; af( ~any(af,2), : ) = []; %remove NaN and 0 IDs

key=zeros(max(af),1);
for i=af'
    datt{i}=dat(find(ID==i));
    %enct{i}=enc(find(ID==i));
    dats=unique(datt{i});
    nint=0;
    if length(dats)>=keydd
        datsd=dats(2:end)-dats(1:(end-1));
        dint=0;
        for k=1:length(datsd)
            dint=dint+datsd(k);
            if dint>=keygap
                nint=nint+1;
                dint=0;
            end
            %disp([k datsd(k) dint nint])
        end
    end
    if nint>=(keydd-1)
        key(i)=1;
        %disp([i length(dats) nint key(i)])
    end
end

iik=find(key)';
disp(['Number of key individuals = ' num2str(length(iik))])
qq=sparse(max(af),max(af));
qqk=sparse(max(af),max(af));
for ii=iik
    disp(['Key individual ' num2str(ii)])
    afx='   companions:';
    for jj=af'
        if ii~=jj
            matq=(datt{ii}*ones(1,length(datt{jj}))==(datt{jj}*ones(1,length(datt{ii})))');%&...
                %(enct{ii}*ones(1,length(enct{jj}))==(enct{jj}*ones(1,length(enct{ii})))');
            [X1,Y1]=find(matq);
            du=datt{ii}(X1);
            dutsd=du(2:end)-du(1:(end-1));
            dintu=0;nintu=0;
            for k=1:length(dutsd)
                dintu=dintu+dutsd(k);
                if dintu>=ccgap
                    nintu=nintu+1;
                    dintu=0;
                end
                %disp([jj k dutsd(k) dintu nintu])
            end
            if nintu>=(cuc-1)%cutoff for companions among keys
                qqk(ii,jj)=1;                
            end
            if nintu>=(ccdd-1)%cutoff for companions with keys
                qq(ii,jj)=1;
                afx=[afx ' ' num2str(jj)];
            end
        end
    end
    disp(afx)
end



disp('  ')
%finds units
unn=0;
usedk=zeros(max(af),1);
unmem=zeros(length(iik),max(af));
for ii=iik
    if ~usedk(ii)
        usedk(ii)=1;
        unn=unn+1;
        unmem(unn,ii)=1;
        inun=zeros(length(af),1);
        inun(ii)=1;
        disp(['Key individual ' num2str(ii)])
        %disp(['   companions:' sprintf(' %4.0f',find(qq(ii,:)))])
        fii=ii;
        while 1
            fuik=[];
            for fi=fii;
                nik=iik(find(qqk(fi,iik)));
                disp(['   key companions of ' num2str(fi) ':' sprintf(' %4.0f',nik)])
                fuik=[fuik iik(find(qqk(fi,iik)&~usedk(iik)'))];
                usedk(nik)=1;
            end
            fii=fuik;
            unmem(unn,fuik)=1;
            if length(fuik)==0;break;end
            
        end
    end
end
disp('   ')
disp(['Units (have at least ', num2str(minkeyunit), ' key members):'])
unx=find(sum(unmem,2)>1);
uname='A';
for k=unx'
    keymem=find(unmem(k,:));
    omem=find(~unmem(k,:));
    nkeymem=omem(find(sum(qq(keymem,omem),1)>=minkeyunit));
    disp(['Unit ' uname '   Size: ' num2str(length(keymem)+length(nkeymem)) ' members (' num2str(length(keymem)) ' key)'])
    disp(['   Key:' sprintf(' %4.0f',keymem)])
    disp(['   Other:' sprintf(' %4.0f',nkeymem)])
    if strcmp(uname,'Z');
        uname=[double(64) '1'];
    end
    if length(uname)==1
        uname=char(double(uname)+1);
    else
        uname=[char(double(uname(1))+1) '1'];
    end
end
savmatn(qqk,unmem,key,'netdat.vna')
savmatn(qq,unmem,key,'netdatc.vna')
disp('   ')
end

function savmatn(qqk,unmem,key,fi)
%saves association matrix in VNA (network) form
inds=find(sum(qqk+qqk'))';
filesave=fi;
fid=fopen(filesave,'w');
rnind=length(inds);
fprintf(fid,'*Node data\n');%node data
fprintf(fid,'ID unit key\n');
for j=1:rnind
    fprintf(fid,'%5.0f %3.0f\n',inds(j),key(inds(j)));
end
fprintf(fid,'*Tie data\n');
fprintf(fid,['from to companions\n']);
[I,J]=find(qqk-diag(diag(qqk)));
for i=1:length(I)
    if ~isnan(qqk(I(i),J(i)))
        fprintf(fid,'%5.0f %5.0f 1\n',I(i),J(i));
    end
end
fclose(fid);
end
 

