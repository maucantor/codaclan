function [prr,tx,pr,cc]=mantel1(t1,t2,nperm)
%Mantel test for proximity matrices, using permutations
%notation roughly follows Schnell et al. Anim Behav. 33:239-253
%removes missing rows and columns from both matrices
nlist=find(~isnan(diag(t1)));
t1=t1(nlist,nlist);
t2=t2(nlist,nlist);
nlist=find(~isnan(diag(t2)));
t1=t1(nlist,nlist);
t2=t2(nlist,nlist);
[l1,l2]=size(t1);
%replaces diagonals by zero
t1=(~diag(ones(1,l1))).*t1;
t2=(~diag(ones(1,l1))).*t2;
t11=diag(NaN*ones(1,l1),0)+t1;
v11=t11(:);
v11=v11(find(~isnan(v11)));
%calculates matrix correlation coefficient
t22=diag(NaN*ones(1,l1),0)+t2;
v22=t22(:);
v22=v22(find(~isnan(v22)));
cor=corrcoef([v11 v22]);
cc=cor(1,2);
fprintf('Matrix correlation =  %6.4f\n',cor(1,2))
%begin Mantel calculation
zx=sum(sum(t1.*t2));
tot=0;
for j=1:nperm
   po=randperm(l1);
   tu=t1(po,po);
   if zx>sum(sum(tu.*t2))
     tot=tot+1;
   end
end
prr=tot/(0.000000001+nperm);
fprintf('Monte Carlo significance =  %7.5f',tot/nperm)
fprintf(' (%g permutations)\n',nperm)
a1x=sum(sum(t1));
a2x=sum(sum(t2));
g1x=a1x^2;
g2x=a2x^2;
b1x=sum(sum(t1.*t1));
b2x=sum(sum(t2.*t2));
d1x=(sum(t1)*sum(t1)');
d2x=(sum(t2)*sum(t2)');
k1x=g1x+2*b1x-4*d1x;
k2x=g2x+2*b2x-4*d2x;
rx=2*b1x*b2x+4*(d1x-b1x)*(d2x-b2x)/(l1-2);
rx=rx+k1x*k2x/((l1-2)*(l1-3))-g1x*g2x/(l1*(l1-1));
tx=sqrt(rx/(l1*(l1-1)));
fprintf('Mantel test statistic = %g\n', zx)
tx=(zx-a1x*a2x/(l1*(l1-1)))/tx;
pr=normcdf(tx,0,1);
fprintf('t-value = %6.4f',tx)
fprintf(' (p = %7.5f)\n', normcdf(tx,0,1))
