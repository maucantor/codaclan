%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Galapagos 2013-2014  Analysis
% Mauricio Cantor, Dalhousie University, May 2015
%
% Estimating group sizes using photo-ID data
%
%%%% Get the data from the root folder 
%a = xlsread([pwd '\data\AllIDdata.xls']); % all photoID data
a = xlsread([pwd '/data/Females_gal1314.xls']); % GAL1314 photoID data

%%%% Set up the right directory for this function
addpath([pwd '/GroupSizesPetersen']);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%reads in iddata and processes it (original by Hal, modified by Mauricio to
%Galapagos 2013-14 data

%%%no males, calves or poor quality 
%%qq=find((a(:,6)>2.5)&((a(:,7)<500)|(a(:,7)>599))&(a(:,7)>10)); %original code
    % MC: no poor quality: removing temporary IDs that are not in the catalogue (here as NaN)
    remove = ~any(isnan(a(:,7)),2); a=a(remove,:);
    % MC: no poor quality Q>=3
    qq=find((a(:,6)>2.5));
nqq=length(qq);%number of records
    id=a(qq,7);%ID
dayd=a(qq,1);%day
yq=datevec(dayd);year=yq(:,1);
%hour=(a(qq,5)-a(qq,1))*24; % MC: original code; but Column 5 is "Frame"
    % MC: Trying to guess how to get the hour data
    hour=(a(qq,2)+ a(qq,3))*24;
    area=a(qq,17); 
sameday=[1 (dayd(2:nqq)~=dayd(1:(nqq-1)))'];
dayn=cumsum(sameday);
stday=find(sameday);
enday=[stday(2:length(stday))-1 nqq];
nday=enday-stday;
poday=((1:nqq)-stday(dayn))./(0.00001+nday(dayn));%position of that sighting within the day's sightings
%idc=zeros(1,1:max(id)); idc(id)=1;idl=find(idc);idc(idl)=1:length(idl); %original code
    %idc=zeros(1,1:max(id)); %original code assumes sequential id labels
    uid=unique(id); uid(isnan(uid(:,1)),:)=[]; idc=zeros(1, length(uid)); % MC: this line doesn't
    % original code: idc(id)=1;idl=find(idc);idc(idl)=1:length(idl);
    idc(id)=1;idl=find(idc);idc(idl)=1:length(idl); % MC: make new labels for IDs to be sequential
sd=spones(sparse(dayn,idc(id),1));ss=full(sum(sd,2));%animals seen all day
disp('  ');
disp('Estimates of group size');
tit=['Morning/afternoon';'Split in half    '];
tit1=['Divided by seen both/total  ';'Divided by estimated cv     '];  
for t=1:2
   disp('   ');
   disp(tit(t,:));
   if t==1;meas=hour/24;else meas=poday;end
   %sdm=spones(sparse(dayn,idc(id),meas<=0.5));sm=full(sum(sdm,2)); %MC:problem with 'sparse()'
      sdm=spones(sparse(dayn,idc(id),(meas<=0.5)+0));sm=full(sum(sdm,2));%seen 1st half
   %sda=spones(sparse(dayn,idc(id),meas>0.5));sa=full(sum(sda,2));%MC:problem with 'sparse()'
      sda=spones(sparse(dayn,idc(id),(meas>0.5)+0));sa=full(sum(sda,2));%seen 2nd half
   nsb=sm+sa-ss;%number seen both
   gse=((sm+1).*(sa+1)./(nsb+1))-1;%group size estimate
   varn=(sm+1).*(sa+1).*(sm-nsb).*(sa-nsb)./(((nsb+1).^2).*(nsb+2));%variance
   cv=sqrt(varn)./gse;
   if (sa.*sm)==0;gse=0;cv=10000;end
   for t1=2:2
      disp('   ');
      disp(tit1(t1,:));
      disp('                Interval          Cumulative         Cumulative--typical')    
      disp('Range        n    mgs    sd     n   mgs    sd  c(sd)   mgs    sd  c(sd)');
      ratch=0.05;
      for rat=0:ratch:1;
         gg=find((cv>=rat)&(cv<(rat+ratch))&sa&sm);
         gh=find((cv<(rat+ratch))&sa&sm);
         typm=sum(gse(gh).^2)/sum(gse(gh));
         typs=sqrt((sum(gse(gh).^3)-typm^2*sum(gse(gh)))/(sum(gse(gh))-1));
         ctyps=sqrt(typs^2-sum(varn(gh).*gse(gh))/sum(gse(gh)));
         if length(gg)>1;disp(sprintf('%4.2f-%4.2f  %3.0f  %6.2f %5.2f  %3.0f  %5.2f %5.2f %5.2f  %5.2f %5.2f %5.2f',rat,(rat+ratch),length(gg),mean(gse(gg)),std(gse(gg)),length(gh),mean(gse(gh)),std(gse(gh)),sqrt(std(gse(gh))^2-mean(varn(gh))),typm,typs,ctyps));end
      end
      disp('                Cumulative         Cumulative--typical')    
      disp('Max(cv) Area   n   mgs    sd  c(sd)   mgs    sd  c(sd)');

      for cvmax=[0.25 0.4]
      for ar=1:4
         gh=find((cv<cvmax)&sa&sm&(ar==area(stday)));
         typm=sum(gse(gh).^2)/sum(gse(gh));
         typs=sqrt((sum(gse(gh).^3)-typm^2*sum(gse(gh)))/(sum(gse(gh))-1));
         ctyps=sqrt(typs^2-sum(varn(gh).*gse(gh))/sum(gse(gh)));
         if length(gh>1);disp(sprintf('%4.2f %4.0f  %3.0f  %6.2f %5.2f  %5.2f  %5.2f %5.2f %5.2f  %5.2f %5.2f %5.2f',cvmax,ar,length(gh),mean(gse(gh)),std(gse(gh)),sqrt(std(gse(gh))^2-mean(varn(gh))),typm,typs,ctyps));end
      end
      end
      for cvmax=[0.25 0.4]
      for yr=85:99
         gh=find((cv<cvmax)&sa&sm&(yr==year(stday))&(1==area(stday)));
         if length(gh>1)
            typm=sum(gse(gh).^2)/sum(gse(gh));
         typs=sqrt((sum(gse(gh).^3)-typm^2*sum(gse(gh)))/(sum(gse(gh))-1));
         ctyps=sqrt(typs^2-sum(varn(gh).*gse(gh))/sum(gse(gh)));
         disp(sprintf('%4.2f %4.0f  %3.0f  %6.2f %5.2f  %5.2f  %5.2f %5.2f %5.2f  %5.2f %5.2f %5.2f',cvmax,yr,length(gh),mean(gse(gh)),std(gse(gh)),sqrt(std(gse(gh))^2-mean(varn(gh))),typm,typs,ctyps));
      end
      end
      end
      subplot(2,1,t);
      %plot(cv,gse,'.');set(gca,'xlim',[0 1]);
      ql=find((cv<0.25)&sa&sm);
      for j=ql';
         disp([datestr(dayd(stday(j))) sprintf('  %1.0f  %3.0f %3.0f %3.0f %3.0f  %5.2f  %5.3f',area(stday(j)),ss(j),sm(j),sa(j),nsb(j),gse(j),cv(j))]);
      end   
   end
end
ql=find((cv<0.25)&sa&sm);
ql4=find((cv>=0.25)&(cv<0.4)&sa&sm);
ra=2.5:5:97.5;hr25=hist(gse(ql),ra);hr4=hist(gse(ql4),ra);bar(ra,[hr25;hr4]',1,'stack');colormap(gray);
xlabel('Estimated group size')
ylabel('Number of groups (days)')
legend('cv<0.25','0.25<cv<0.4')
set(gcf,'color',[1 1 1])
