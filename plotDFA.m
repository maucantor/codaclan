function newType = plotDFA(originalData,originalType,dfa,dfaType,newData)
%function plotDFA(originalData,originalType,dfa,newData)
%
% Produces a plot with new interclick interval (ICI) (or 2 Principal Components of PCA on
% ICI) of a original data set used as training data in a Discriminant
% Analysis, the Linear or Quadratic Discriminant Functions, and the new ICI
% (or PCA) data with new classification
%
% INPUT:
%'originalData': n-by-2 matrix, giving the first 2 Principal Components of PCA on
%the Interclick Intervals (ICI) for all n coda samples; or the raw ICIs for 3-click codas
%
%'originalType': n-by-1 matrix with the coda types for each n codas. It can be
%either numerical or alphanumerical
%
%'dfa': Discriminant Function based on 'originalData'. It must be a object
%'ClassificationDiscriminant'. See help fitcdiscr
%
%'dfaType': 'linear' for linear discriminant function (LDA), 'quadratic'
%for quadratic discriminand function (QDA).
%
%'newData': the new data to be classified into 'originalType' given by the 'dfa'
%object. It has to be a matrix such as 'originalData'.
%
% OUTPUT:
%Scatterplot with original data (either ICI or PCs on ICIs) and the
%original coda types represented by filled circles; new data (crosses) are classified
%in these coda types according to the discriminant function DFA (lines)
%
%'newType':  n-by-1 matrix with the coda types for each n coda in the new
%data, according with the given discriminant function. This can be used to
%compose the 46th column in the standard codaData csv format
%
% Mauricio Cantor, June 2015


figure

% Specify original coda types and colors
ctypes = unique(originalType);
ntypes = size(ctypes, 1);
colors = 'brygkmcbrygkmcbrygkmc';

% Plot original coda data
gscatter(originalData(:,1), originalData(:,2), originalType, colors(1:ntypes), '', 10);
hold on

% Classify new data into coda types based on the discriminant function
newType = predict(dfa, newData);

% Specify matching colors for new data types
nctypes = unique(newType);
ncolors = ismember(ctypes, nctypes)';

% Plotting new data on top of original distribution
gscatter(newData(:,1), newData(:,2), newType, colors(ncolors), '+', 10)

% Plotting legend
legend('Location', 'Best')

% Plot the DFA classification boundaries.
switch dfaType
    case 'linear'
        for i=2:size(ctypes,1)
            %First, retrieve the coefficients for the linear boundary between the ith and jth types 
            K = dfa.Coeffs(i, i-1).Const;
            L = dfa.Coeffs(i, i-1).Linear;

            % Specify the curve K + [x,y]*L  = 0.
            f = @(x1,x2) K + L(1)*x1 + L(2)*x2;

            % Plot the curve f for the range of each raw ICI
            h2 = ezplot(f, [minmax(originalData(:,1)') minmax(originalData(:,2)')]);
            h2.Color = colors(i);
        end
        
    case 'quadratic'
        for i=2:size(ctypes,1)
            %First, retrieve the coefficients for the quadratic boundary between the ith and jth types 
            K = dfa.Coeffs(i, i-1).Const;
            L = dfa.Coeffs(i, i-1).Linear;
            Q = dfa.Coeffs(i, i-1).Quadratic;

            % Specify the curve K + [x1,x2]*L + [x1,x2]*Q*[x1,x2]' = 0.
            f = @(x1,x2) K + L(1)*x1 + L(2)*x2 + Q(1,1)*x1.^2 + (Q(1,2)+Q(2,1))*x1.*x2 + Q(2,2)*x2.^2;

            % Plot the curve f for the range of each raw ICI
            h2 = ezplot(f, [minmax(originalData(:,1)') minmax(originalData(:,2)')]);
            h2.Color = colors(i);
        end
        
end

% Final adjustments of the figure
%get limits of original and new data
%limits = [min([originalData(:,1); newData(:,1)]), max([originalData(:,1); newData(:,1)]), ...
%    min([originalData(:,2); newData(:,2)]), max([originalData(:,2); newData(:,2)])];
%axis(limits)
title('')
xlabel('PC1')
ylabel('PC2')
hold off

end