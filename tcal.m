%for locating text within axes boundaries xin = xlim, yin = ylim
%By Luke Rendell

function [x, y] = tcal(xin,yin);
x = xin(1)+((xin(2)-xin(1))/20);
y = yin(1)+((yin(2)-yin(1))/4);