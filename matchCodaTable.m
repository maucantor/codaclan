function auxtab = matchCodaTable(table,newlabHM,orilab,newlab,rejlab,allrejections)
%auxtab = matchCodaTable(table,newlabHM,orilab,newlab,rejlab, allrejections)
%
% The function matches the repertoires in the output dendrogram from the
% quantitative coda repertoire analysis and the heat map from the
% categorical repertoire analysis. Note that when plotting the dendrogram, 
% some repertoires were removed from the similarity analysis on continuous
% data (ICIs) because they did not have the minimum coda sample
% size (threshold is usually <25codas). When plotting the categorical data from
% OPTICS into a heat map matrix, other repertoires may have been excluded 
% because they contained only 'noise' and no 'true' coda types. The
% function identifies the remaining repertoires in both continuous and
% categorical outputs and re-order the matrix for the heat map to match the
% order in the dendrogram output, so these both graphs can be plotted
% together
%
% INPUT:
%'table': contingency table with coda types in the rows and repertoires in
%the columns. Cell entries are counts or proportion (standardized via relativeTable.m)
%
%'newlabHM': vector with the repertoire labels (usually groups) remaining after the
%classification of codas by OPTICS and the removal of noise. Example:
%%%dataCodaOpticsAllG_mxmmN = rmNoise(dataCodaOpticsAllG_mxmm);
%%%newlabHM = unique(dataCodaOpticsAllG_mxmmN(:, 3));
%
%'orilab': vector with all original repertoire labels (usually groups) in
%the quantitative dataCodas dataset used to build the dendrogram. Example:
%%%orilab = unique(dataCodasAllG(:,3));
%
%'rejlab': vector with the indices of the repertoires rejected in the
%quantitative analysis and dendrogram. Example
%%%[sim rejlab] = daysim(dataCodaOpticsAllG_mxmmN,3,5,0); %OR
%%%rejlab = [7 11 14 16 17 24 27 32 68 (max(dataCodasETPG(:,3))+9)];
%
%'newlab': vectore with the new labels and order for the repertoires after
%the plotting of the dendrogram. Plot the dendrogram and run this:
%%%%newlab = str2num(get(gca,'xticklabel'));
%
%%'allrejections': 1/0. =1 prints the number (indices) of all rejected
%repertoires by the continuous analyses (dendrogram) and by optics (only
%noise)
%
% OUTPUT:
%'auxtab': and organized contingency table with coda types in the rows and 
%repertoires in columns, but the columns contains only the repertoires in
%the dendrogram, and in the same order. Use this table to plot a heat map
%and the output will match the dendrogram
%
% Mauricio Cantor, June 2015 

    % Dendrogram:
    % Get the dendrogram of interest, their new labels and the rejected repertoires (<25 codas)
    %simAbsInfAll = daysim(dataCodasAllG,2,25,0); %%% 2.3.1 ALL data, Infinity-norm similarity, photoID groups, b=0.001
    %zIAllG = linkage_sim(simAbsInfAll(:,3)');
    %dendrogram_sim(zIAllG,0);
    % Original group labels
    %orilab = unique(dataCodasAllG(:,3));
    % New group labels in dendrogram 
    %newlab = str2num(get(gca,'xticklabel'));
    % Rejected groups' indices (10)
    %rejlab = [7 11 14 16 17 24 27 32 68 (max(dataCodasETPG(:,3))+9)]; 

    % Heat Map:
    % Get the crosstable, their labels and the rejected repertoires (only noise)
    %table;
    % New group labels in heatmap
    %newlabHM = unique(dataCodaOpticsAllG_mxmmN(:, 3));
    
    % Rejected groups by Optics
    rejlabHM = ~ismember(orilab, newlabHM);
    orilab(rejlabHM)';
    % Optics rejected groups' indices
    orilabindex = 1:(size(orilab,1));
    rejlabHMindex = orilabindex(rejlabHM);
    % Remaining groups' indices
    remHM = ismember(orilab, newlabHM); 
    remHMindex = orilabindex(remHM);
    % Add empty columns indices
    % New order of columns
    neworder = [remHMindex rejlabHMindex];
    % Empty columns
    emptycol = zeros([size(table,1) size(rejlabHMindex,2)]);
    % Adding empty col at the end
    newtab = [table emptycol];
    % Adding new order indices as a column (flip) to sort the matrix
    auxtab = [newtab; neworder].';
    auxtab = sortrows(auxtab, size(auxtab,2));
    % Remove this column indices and flip back the matrix
    auxtab(:, size(auxtab,2))=[];
    auxtab = auxtab.';
    % Delete the repertoire rejected in the dendrogram
    auxtab(:, rejlab) = [];
    % Reordering columns (groups) of crosstable to match dendrogram output
    auxtab = auxtab(:, newlab);
    
    % All rejections (from dendrogram and optics) combined
    if allrejections==1
        allrej = union(rejlab, rejlabHMindex);
        disp('The index of all rejected columns by dendrogram and optics are :');
        disp(allrej); 
    end
    
    auxtab;
    
end
