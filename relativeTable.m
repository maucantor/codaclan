function relat = relativeTable(table, dimension)
% relativeTable(table)
%
% Standardize a contingency table, or rescale it by row totals or column totals
% In the latter, cell entries will sum up to 1, either by rows or columns
%
% INPUT:
%'table': contingency table, with counts
%'dimension': 'both' to standardize the entire matrix; 'column' to rescale 
%by column; 'row', otherwise
%
% Mauricio Cantor, June 2015

    % Get dimensions
    [rows, columns] = size(table);
    
    switch dimension
        
        case 'both'
            %standardize
            relat = (table - min(min(table))) / ( max(max(table)) - min(min(table)) );

        case 'column'
            % Get sum of columns and replicate vertically.
            denom = repmat(sum(table, 1), [rows, 1]);
            % Do the division: count of each coda type divided per total, for each group
            relat = table ./ denom;
            % Set infinities (where denom == 0) to 0
            relat(denom==0) = 0;

        otherwise
            % Get sum of rows and replicate horizontally.
            denom = repmat(sum(table, 2), [1, columns]);
            % Do the division: count of each coda type divided per total, for each group
            relat = table ./ denom;
            % Set infinities (where denom == 0) to 0
            relat(denom==0) = 0;
        
     end

end